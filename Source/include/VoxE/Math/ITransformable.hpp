/*
 * ITransformable.hpp
 *
 *  Created on: 31.05.2014
 *      Author: euaconlabs
 */

#ifndef ITRANSFORMABLE_HPP_
#define ITRANSFORMABLE_HPP_

#include <VoxE/Math/Matrix4.hpp>
#include <VoxE/Math/Quaternion.hpp>
#include <VoxE/Core/Rect.hpp>
#include <VoxE/Core/Vector.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class ITransformable
	/// This class serves as a basic interface for several applications
	/// of transformable objects. It may be used by deriving from
	/// Transformable or by filling out these methods on one's own.
	//////////////////////////////////////////////////////////////
	class ITransformable
	{
	public:

		ITransformable() {}
		virtual ~ITransformable() {}

		virtual void Translate(const float x, const float y, const float z) = 0;
		virtual void Translate(const Vector3f& trans) = 0;
		virtual void Rotate(const float pitch, const float yaw, const float roll) = 0;
		virtual void Rotate(const Vector3f& rot) = 0;
		virtual void Scale(const float x, const float y, const float z) = 0;
		virtual void Scale(const Vector3f& scale) = 0;

		virtual void SetPosition(const float x, const float y, const float z) = 0;
		virtual void SetPosition(const Vector3f& pos) = 0;
		virtual void SetRotation(const float pitch, const float yaw, const float roll) = 0;
		virtual void SetRotation(const Vector3f& rot) = 0;
		virtual void SetRotation(const Quaternion& rot) = 0;
		virtual void SetScale(const float x, const float y, const float z) = 0;
		virtual void SetScale(const Vector3f& scale) = 0;

		virtual const Vector3f& GetPosition() const = 0;
		virtual const Quaternion& GetRotation() const = 0;
		virtual const Vector3f GetEulerAngles() const = 0;
		virtual const Vector3f& GetScale() const = 0;

		virtual FloatRect TransformRect(const FloatRect& rect) const = 0;

		virtual const Matrix4& GetTransform() const = 0;
	};
}

#endif /* ITRANSFORMABLE_HPP_ */
