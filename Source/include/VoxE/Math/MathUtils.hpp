#ifndef MATHUTILS_HPP
#define MATHUTILS_HPP

#include <VoxE/Core/Vector.hpp>
#include <VoxE/Native/Atomic.hpp>

#include <limits>

namespace VX
{
	namespace Constants
	{
		static const vxF32 kPiFloat = 3.14159265359f;
		static const vxF64 kPiDouble = 3.1415926535897932384626;
	}

	inline vxF32 Radians(const vxF32 deg)
	{
		return (deg * Constants::kPiFloat / 180.0f);
	}

	inline vxF64 Radians(const vxF64 deg)
	{
		return (deg * Constants::kPiDouble / 180.0);
	}

	inline vxF32 Degrees(const vxF32 rad)
	{
		return (rad * 180.0f / Constants::kPiFloat);
	}

	inline vxF64 Degrees(const vxF64 rad)
	{
		return (rad * 180.0 / Constants::kPiDouble);
	}

	template<typename T>
	inline T Epsilon()
	{
		return std::numeric_limits<T>::epsilon();
	}

	template<typename N>
	Vector2<N> VecSin(const Vector2<N>& t)
	{
		return Vector2<N>(sin(t.x), sin(t.y));
	}

	template<typename N>
	Vector3<N> VecSin(const Vector3<N>& t)
	{
		return Vector3<N>(sin(t.x), sin(t.y), sin(t.z));
	}

	template<typename N>
	Vector4<N> VecSin(const Vector4<N>& t)
	{
		return Vector4<N>(sin(t.x), sin(t.y), sin(t.z), sin(t.w));
	}

	template<typename N>
	Vector2<N> VecCos(const Vector2<N>& t)
	{
		return Vector2<N>(cos(t.x), cos(t.y));
	}

	template<typename N>
	Vector3<N> VecCos(const Vector3<N>& t)
	{
		return Vector3<N>(cos(t.x), cos(t.y), cos(t.z));
	}

	template<typename N>
	Vector4<N> VecCos(const Vector4<N>& t)
	{
		return Vector4<N>(cos(t.x), cos(t.y), cos(t.z), cos(t.w));
	}

	template<typename N>
	Vector2<N> VecTan(const Vector2<N>& t)
	{
		return Vector2<N>(tan(t.x), tan(t.y));
	}

	template<typename N>
	Vector3<N> VecTan(const Vector3<N>& t)
	{
		return Vector3<N>(tan(t.x), tan(t.y), tan(t.z));
	}

	template<typename N>
	Vector4<N> VecTan(const Vector4<N>& t)
	{
		return Vector4<N>(tan(t.x), tan(t.y), tan(t.z), tan(t.w));
	}
}

#endif