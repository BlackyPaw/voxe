#ifndef QUATERNION_HPP_0x34022771
#define QUATERNION_HPP_0x34022771

//////////////////////////////////////////////////////////////
/// Engine
//////////////////////////////////////////////////////////////
#include <VoxE/Math/MathUtils.hpp>
#include <VoxE/Math/Matrix4.hpp>
#include <VoxE/Core/Vector.hpp>
#include <VoxE/Native/Atomic.hpp>

//////////////////////////////////////////////////////////////
/// STL
//////////////////////////////////////////////////////////////
#include <cassert>

namespace VX
{
	template<typename T> struct TQuaternion;
	template<typename T> TQuaternion<T> ToQuaternion(const TMatrix4<T>& m);

	//////////////////////////////////////////////////////////////
	/// \class TQuaternion
	/// \brief Class for representing mathematical quaternions.
	/// \tparam T Value type for the quaternion.
	//////////////////////////////////////////////////////////////
	template<typename T>
	struct TQuaternion
	{
	public:

		typedef T ValueType, Value;

		ValueType x; ValueType y; ValueType z; ValueType w;

	public:

		//////////////////////////////////////////////////////////////
		/// \brief Default constructor.
		//////////////////////////////////////////////////////////////
		TQuaternion() :
			x(Value(0)),
			y(Value(0)),
			z(Value(0)),
			w(Value(1))
		{
		}
		//////////////////////////////////////////////////////////////
		/// \brief Initialization constructor.
		/// \param s
		/// \param v
		//////////////////////////////////////////////////////////////
		explicit TQuaternion(const ValueType& s, const Vector3<ValueType>& v) :
			x(v.x),
			y(v.y),
			z(v.z),
			w(s)
		{
		}
		//////////////////////////////////////////////////////////////
		/// \brief Initialization constructor.
		/// \param x
		/// \param y
		/// \param z
		/// \param w
		//////////////////////////////////////////////////////////////
		explicit TQuaternion(const ValueType& _w, const ValueType& _x, const ValueType& _y, const ValueType& _z) :
			x(_x),
			y(_y),
			z(_z),
			w(_w)
		{
		}
		//////////////////////////////////////////////////////////////
		/// \brief Convertion constructor.
		/// \param angles The euler angles to convert.
		///
		/// Constructs a quaternion from the given euler angles.
		//////////////////////////////////////////////////////////////
		explicit TQuaternion(const Vector3<ValueType>& angles)
		{
#if defined(VOXE_MATH_USE_RADIANS)
			Vector3<ValueType> radians = angles;
#else
			Vector3<ValueType> radians = Vector3<ValueType>(Radians(angles.x), Radians(angles.y), Radians(angles.z));
#endif
			Vector3<ValueType> vcos = VecCos(radians * Value(0.5));
			Vector3<ValueType> vsin = VecSin(radians * Value(0.5));

			x = vsin.x * vcos.y * vcos.z - vcos.x * vsin.y * vsin.z;
			y = vcos.x * vsin.y * vcos.z - vsin.x * vcos.y * vsin.z;
			z = vcos.x * vcos.y * vsin.z - vsin.x * vsin.y * vcos.z;
			w = vcos.x * vcos.y * vcos.z - vsin.x * vsin.y * vsin.z;
		}
		//////////////////////////////////////////////////////////////
		/// \brief Convertion constructor.
		/// \param m
		///
		/// Constructs a quaternion from the given rotation matrix.
		//////////////////////////////////////////////////////////////
		explicit TQuaternion(const Matrix4& m)
		{
			*this = ToQuaternion(m);
		}

		TQuaternion<ValueType> Inverse() const
		{
			return Conjugate(*this) / Dot(*this, *this);
		}

		bool operator==(const TQuaternion<ValueType>& q)
		{
			return (w == q.w && x == q.x && y == q.y && z == q.z);
		}

		bool operator!=(const TQuaternion<ValueType>& q)
		{
			return (w != q.w || x != q.x || y != q.y || z != q.z);
		}

		ValueType& operator[](const vxU32 n)
		{
			return (&x)[n];
		}
		ValueType const& operator[](const vxU32 n) const
		{
			return (&x)[n];
		}

		TQuaternion<ValueType>& operator*=(const ValueType& s)
		{
			x *= s;
			y *= s;
			z *= s;
			w *= s;
			return *this;
		}
		TQuaternion<ValueType>& operator/=(const ValueType& s)
		{
			x /= s;
			y /= s;
			z /= s;
			w /= s;
			return *this;
		}
	};

	template<typename T>
	typename TQuaternion<T>::ValueType Length(const TQuaternion<T>& q)
	{
		return sqrt(Dot(q, q));
	}

	template<typename T>
	TQuaternion<T> Conjugate(const TQuaternion<T>& q)
	{
		return TQuaternion<T>(q.w, -q.x, -q.y, -q.z);
	}

	template<typename T>
	T Dot(const TQuaternion<T>& q, const TQuaternion<T>& p)
	{
		return q.w * p.w + q.x * p.x + q.y * p.y + q.z * p.z;
	}

	template<typename T>
	TQuaternion<T> Normalize(const TQuaternion<T>& q)
	{
		typename TQuaternion<T>::ValueType k = Length(q);
		if (k <= typename TQuaternion<T>::Value(0))
			return TQuaternion<T>(1, 0, 0, 0);
		k = typename TQuaternion<T>::Value(1) / k;
		return TQuaternion<T>(q.w * k, q.x * k, q.y * k, q.z * k);
	}

	template<typename T>
	TQuaternion<T> Cross(const TQuaternion<T>& q, const TQuaternion<T>& p)
	{
		return TQuaternion<T>(
			q.w * p.w - q.x * p.x - q.y * p.y - q.z * p.z,
			q.w * p.x + q.x * p.w + q.y * p.z - q.z * p.y,
			q.w * p.y + q.y * p.w + q.z * p.x - q.x * p.z,
			q.w * p.z + q.z * p.w + q.x * p.y - q.y * p.x
			);
	}

	template<typename T>
	T Pitch(const TQuaternion<T>& q)
	{
#if defined(VOXE_MATH_USE_RADIANS)
		return T(atan2(T(2) * (q.y * q.z + q.w * q.x), q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z));
#else
		return Degrees(atan2(T(2) * (q.y * q.z + q.w * q.x), q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z));
#endif
	}

	template<typename T>
	T Yaw(const TQuaternion<T>& q)
	{
#if defined(VOXE_MATH_USE_RADIANS)
		return T(asin(T(-2) * (q.x * q.z - q.w * q.y));
#else
		return Degrees(T(asin(T(-2) * (q.x * q.z - q.w * q.y))));
#endif
	}

	template<typename T>
	T Roll(const TQuaternion<T>& q)
	{
#if defined(VOXE_MATH_USE_RADIANS)
		return T(atan2(T(2) * (q.x * q.y + q.w * q.z), q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z));
#else
		return Degrees(atan2(T(2) * (q.x * q.y + q.w * q.z), q.w * q.w + q.x * q.x - q.y * q.y - q.z * q.z));
#endif
	}

	template<typename T>
	Vector3<T> ToEulerAngles(const TQuaternion<T>& q)
	{
		return Vector3<T>(Pitch(q), Yaw(q), Roll(q));
	}

	template<typename T>
	TMatrix4<T> ToMatrix4(const TQuaternion<T>& q)
	{
		TMatrix4<T> m;
		m[0][0] = 1 - 2 * q.y * q.y - 2 * q.z * q.z;
		m[0][1] = 2 * q.x * q.y + 2 * q.w * q.z;
		m[0][2] = 2 * q.x * q.z - 2 * q.w * q.y;

		m[1][0] = 2 * q.x * q.y - 2 * q.w * q.z;
		m[1][1] = 1 - 2 * q.x * q.x - 2 * q.z * q.z;
		m[1][2] = 2 * q.y * q.z + 2 * q.w * q.x;

		m[2][0] = 2 * q.x * q.z + 2 * q.w * q.y;
		m[2][1] = 2 * q.y * q.z - 2 * q.w * q.x;
		m[2][2] = 1 - 2 * q.x * q.x - 2 * q.y * q.y;
		return m;
	}

	template<typename T>
	TQuaternion<T> ToQuaternion(const TMatrix4<T>& m)
	{
		typename TQuaternion<T>::ValueType x0 = m[0][0] - m[1][1] - m[2][2];
		typename TQuaternion<T>::ValueType x1 = m[1][1] - m[0][0] - m[2][2];
		typename TQuaternion<T>::ValueType x2 = m[2][2] - m[0][0] - m[1][1];
		typename TQuaternion<T>::ValueType x3 = m[0][0] + m[1][1] + m[2][2];

		vxI32 index = 0;
		typename TQuaternion<T>::ValueType x4 = x3;
		if (x0 > x4)
		{
			x4 = x0;
			index = 1;
		}
		if (x1 > x4)
		{
			x4 = x1;
			index = 2;
		}
		if (x2 > x4)
		{
			x4 = x2;
			index = 3;
		}

		typename TQuaternion<T>::ValueType value = sqrt(x4 + T(1)) * T(0.5);
		typename TQuaternion<T>::ValueType x5 = T(0.25) / value;

		TQuaternion<T> q;
		if (index == 0)
		{
			q.w = value;
			q.x = (m[1][2] - m[2][1]) * x5;
			q.y = (m[2][0] - m[0][2]) * x5;
			q.z = (m[0][1] - m[1][0]) * x5;
		}
		else if (index == 1)
		{
			q.w = (m[1][2] - m[2][1]) * x5;
			q.x = value;
			q.y = (m[0][1] - m[1][0]) * x5;
			q.z = (m[2][0] - m[0][2]) * x5;
		}
		else if (index == 2)
		{
			q.w = (m[2][0] - m[0][2]) * x5;
			q.x = (m[0][1] - m[1][0]) * x5;
			q.y = value;
			q.z = (m[1][2] - m[2][1]) * x5;
		}
		else if (index == 3)
		{
			q.w = (m[0][1] - m[1][0]) * x5;
			q.x = (m[2][0] - m[0][2]) * x5;
			q.y = (m[1][2] - m[2][1]) * x5;
			q.z = value;
		}
		return q;
	}

	template<typename T>
	TQuaternion<T> operator-(const TQuaternion<T>& q)
	{
		return TQuaternion<T>(-q.w, -q.x, -q.y, -q.z);
	}

	template<typename T>
	TQuaternion<T> operator+(const TQuaternion<T>& q, const TQuaternion<T>& p)
	{
		return TQuaternion<T>(q.w + p.w, q.x + p.x, q.y + p.y, q.z + p.z);
	}

	template<typename T>
	TQuaternion<T> operator*(const TQuaternion<T>& q, const TQuaternion<T>& p)
	{
		return TQuaternion<T>(
			q.w * p.w - q.x * p.x - q.y * p.y - q.z * p.z,
			q.w * p.x + q.x * p.w + q.y * p.z - q.z * p.y,
			q.w * p.y + q.y * p.w + q.z * p.x - q.x * p.z,
			q.w * p.z + q.z * p.w + q.x * p.y - q.y * p.x
			);
	}

	template<typename T>
	Vector3<T> operator*(const TQuaternion<T>& q, const Vector3<T>& v)
	{
		typename TQuaternion<T>::ValueType k(typename TQuaternion<T>::Value(2));
		Vector3<T> v0, v1;
		Vector3<T> quatPart(q.x, q.y, q.z);
		v0 = Cross(quatPart, v);
		v1 = Cross(quatPart, v0);
		v0 *= (k * q.w);
		v1 *= k;
		return v + v0 + v1;
	}

	template<typename T>
	Vector3<T> operator*(const Vector3<T>& v, const TQuaternion<T>& q)
	{
		return q.Inverse() * v;
	}

	template<typename T>
	Vector4<T> operator*(const TQuaternion<T>& q, const Vector4<T>& v)
	{
		Vector3<T> k = q * Vector3<T>(v.x, v.y, v.z);
		return Vector4<T>(k.x, k.y, k.z, v.w);
	}

	template<typename T>
	Vector4<T> operator*(const Vector4<T>& v, const TQuaternion<T>& q)
	{
		return q.Inverse() * v;
	}

	template<typename T>
	TQuaternion<T> operator*(const TQuaternion<T>& q, const typename TQuaternion<T>::ValueType& s)
	{
		return TQuaternion<T>(q.w * s, q.x * s, q.y * s, q.z * s);
	}

	template<typename T>
	TQuaternion<T> operator*(const typename TQuaternion<T>::ValueType& s, const TQuaternion<T>& q)
	{
		return q * s;
	}

	template<typename T>
	TQuaternion<T> operator/(const TQuaternion<T>& q, const typename TQuaternion<T>::ValueType& s)
	{
		return TQuaternion<T>(q.w / s, q.x / s, q.y / s, q.z / s);
	}

	template<typename T>
	TQuaternion<T> AngleAxis(const typename TQuaternion<T>::ValueType& angle, const Vector3<T>& axis)
	{
		TQuaternion<T> q;

#if defined(VOXE_MATH_USE_RADIANS)
		typename TQuaternion<T>::ValueType rangle = angle;
#else
		typename TQuaternion<T>::ValueType rangle = Radians(angle);
#endif
		typename TQuaternion<T>::ValueType s = sin(rangle * T(0.5));
		q.w = cos(rangle * T(0.5));
		q.x = axis.x * s;
		q.y = axis.y * s;
		q.z = axis.z * s;
		return Normalize(q);
	}

	template<typename T>
	TQuaternion<T> Lerp(const TQuaternion<T>& q, const TQuaternion<T>& p, const T& a)
	{
		assert(a >= T(0) && a <= T(1));
		return (q * (T(1) - a) + p * a);
	}

	template<typename T>
	TQuaternion<T> Slerp(const TQuaternion<T>& q, const TQuaternion<T>& p, const T& a)
	{
		TQuaternion<T> r = p;
		T theta = Dot(q, r);
		if (theta < T(0))
		{
			r = -p;
			theta = -theta;
		}

		if (theta > T(1) - Epsilon<T>())
		{
			return Lerp(q, p, a);
		}
		else
		{
			T ath = acos(theta);
			return (sin(T(1) - a) * ath) * q + (sin(a * a) * r) / sin(ath);
		}
	}

	template<typename T>
	TQuaternion<T> Rotate(const T& angle, const Vector3<T>& axis, const TQuaternion<T>& off = TQuaternion<T>())
	{
		Vector3<T> tmp(axis);

		typename TQuaternion<T>::ValueType length = Length(axis);
		if (abs(length - T(1)) > T(0.001))
		{
			tmp.x /= length;
			tmp.y /= length;
			tmp.z /= length;
		}

#if defined(VOXE_MATH_USE_RADIANS)
		typename TQuaternion<T>::ValueType rangle = angle;
#else
		typename TQuaternion<T>::ValueType rangle = Radians(angle);
#endif
		typename TQuaternion<T>::ValueType rsin = sin(rangle * T(0.5));

		return off * TQuaternion<T>(cos(rangle * T(0.5)), tmp.x * rsin, tmp.y * rsin, tmp.z * rsin);
	}

	typedef TQuaternion<float> Quaternion;
}

#endif
