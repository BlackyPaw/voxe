/*
 * IDrawable.hpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#ifndef IDRAWABLE_HPP_
#define IDRAWABLE_HPP_

#include <VoxE/Graphics/ShaderProgram.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class IDrawable
	/// Interface for creating drawable objects like sprites, meshes, ...
	//////////////////////////////////////////////////////////////
	class IDrawable
	{
	public:

		virtual ~IDrawable() {}

		//////////////////////////////////////////////////////////////
		/// Draws the drawable object.
		//////////////////////////////////////////////////////////////
		virtual void Draw() = 0;
	};
}

#endif /* IDRAWABLE_HPP_ */
