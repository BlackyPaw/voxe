#ifndef POLYGON_MODE_HPP
#define POLYGON_MODE_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum PolygonMode
	/// Describes all supported polygon modes for use when drawing
	/// primitives.
	//////////////////////////////////////////////////////////////
	enum PolygonMode
	{
		POLYGON_MODE_POINTS = 0,
		POLYGON_MODE_LINES,
		POLYGON_MODE_LINELOOP,
		POLYGON_MODE_LINESTRP,
		POLYGON_MODE_TRIANGLES,
		POLYGON_MODE_TRIANGLEFAN,
		POLYGON_MODE_TRIANGLESTRIP,

		POLYGON_MODE_COUNTER
	};

	//////////////////////////////////////////////////////////////
	/// Translates a given polygon mode into its OpenGL GLenum
	/// counterpart.
	/// \param mode The polygon mode to translate.
	//////////////////////////////////////////////////////////////
	vxU32 TranslatePolygonMode(const PolygonMode& mode);
}

#endif // POLYGON_MODE_HPP