#ifndef MESH_HPP
#define MESH_HPP

#include <VoxE/Core/Array.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Mesh
	/// This class holds several submeshes and groups them together
	/// as a whole. The submeshes can be accessed individually
	/// through the provided accesssors.
	//////////////////////////////////////////////////////////////
	class Mesh
	{
	public:

		Mesh();
		~Mesh();

	private:
	};
}

#endif // MESH_HPP