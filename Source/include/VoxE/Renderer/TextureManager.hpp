/*
 * TextureManager.hpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#ifndef TEXTUREMANAGER_HPP_
#define TEXTUREMANAGER_HPP_

#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Graphics/Texture.hpp>

#include <map>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class TextureManager
	/// Manages to which texture units given textures are bound to
	/// and always manages the currently active texture unit. Also
	/// the texture manager prevents double-bindings of textures.
	//////////////////////////////////////////////////////////////
	class TextureManager : public ISingleton<TextureManager>
	{
	public:

		TextureManager();

		//////////////////////////////////////////////////////////////
		/// Binds the given texture to the next free texture unit.
		/// \returns The texture unit to which the texture has been
		///		bound.
		//////////////////////////////////////////////////////////////
		vxU32 BindTexture(Texture* texture);
		//////////////////////////////////////////////////////////////
		/// Gets the currently active texture unit.
		//////////////////////////////////////////////////////////////
		vxU32 GetActiveUnit() const;
		//////////////////////////////////////////////////////////////
		/// Gets the texture bound to a given unit. If no texture is
		/// bound to the given texture unit 0 is returned.
		/// \param unit The texture unit to which the desired texture is bound.
		//////////////////////////////////////////////////////////////
		vxU32 GetTextureHandle(const vxU32 unit) const;
		//////////////////////////////////////////////////////////////
		/// Resets the texture manager's state so that it will overwrite
		/// currently bound textures. This is usually done whenever a
		/// material change takes place.
		//////////////////////////////////////////////////////////////
		void Reset();

	private:

		std::map<vxU32, vxU32> mBoundTextures;
		vxU32 mNextUnit;
	};
}

#endif /* TEXTUREMANAGER_HPP_ */
