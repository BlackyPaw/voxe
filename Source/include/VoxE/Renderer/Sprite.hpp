#ifndef SPRITE_HPP
#define SPRITE_HPP

#include <VoxE/Core/Rect.hpp>
#include <VoxE/Resources/ResourceHandle.hpp>
#include <VoxE/Renderer/IDrawable.hpp>
#include <VoxE/Renderer/Renderer.hpp>

namespace VX
{
	class SpriteTesselator;

	//////////////////////////////////////////////////////////////
	/// \class Sprite
	/// This class is used to draw simple sprites each being a
	/// quad with a texture on it. It is able to be used with
	/// texture atlases since it is also able to only use a subset
	/// of the texture.
	//////////////////////////////////////////////////////////////
	class Sprite : public IDrawable
	{
	public:

		Sprite();
		//////////////////////////////////////////////////////////////
		/// Creates the sprite using the given data.
		/// \param t The texture to use.
		/// \param r The texture rectangle which defines the bounds of
		///		the subset of the texture to display.
		//////////////////////////////////////////////////////////////
		Sprite(const ResourceHandle& t, const IntRect& r);
		virtual ~Sprite();

		//////////////////////////////////////////////////////////////
		/// Sets the shadere to use as default when drawing a sprite
		/// which does not have its own shader set.
		//////////////////////////////////////////////////////////////
		static void SetDefaultShader(const ResourceHandle& s);

		//////////////////////////////////////////////////////////////
		/// Sets the texture to display.
		/// \param t A handle to the sprite's texture resource.
		//////////////////////////////////////////////////////////////
		void SetTexture(const ResourceHandle& t);
		//////////////////////////////////////////////////////////////
		/// Sets the texture as well as its texture rect.
		/// \param t A handle to the sprite's texture resource.
		/// \param r The texture rectangle which defines the bounds of
		///		the subset of the texture to display.
		//////////////////////////////////////////////////////////////
		void SetTexture(const ResourceHandle& t, const IntRect& r);
		//////////////////////////////////////////////////////////////
		/// Sets the shader used for rendering the sprite. If set to
		/// nullptr the default shader for materials will be used. It
		/// may be set via SetDefaultShader(...).
		/// \param s The shader to use.
		/// \see SetDefaultShader()
		//////////////////////////////////////////////////////////////
		void SetShader(const ResourceHandle& s);
		//////////////////////////////////////////////////////////////
		/// Sets the subset of the texture to display.
		/// \param r The rectangle defining the subset of the texture
		///		to display in pixels.
		//////////////////////////////////////////////////////////////
		void SetSubset(const IntRect& r);

		//////////////////////////////////////////////////////////////
		/// Gets the axis-aligned bounding rectangle of the sprite
		/// expressed in its local coordinate space.
		//////////////////////////////////////////////////////////////
		FloatRect GetLocalBounds() const;

		//////////////////////////////////////////////////////////////
		/// Gets the texture the sprite uses.
		//////////////////////////////////////////////////////////////
		const ResourceHandle& GetTexture() const;
		//////////////////////////////////////////////////////////////
		/// Gets the shader the sprite uses.
		//////////////////////////////////////////////////////////////
		const ResourceHandle& GetShader() const;
		//////////////////////////////////////////////////////////////
		/// Gets the subset of the sprite's texture to display.
		//////////////////////////////////////////////////////////////
		const IntRect& GetSubset() const;
		//////////////////////////////////////////////////////////////
		/// Gets the sprite's vertex attributes (untransformed). They
		/// are packed into an array of vxF32s stored like this:
		///	Vertex1: - Position.x
		///			 - Position.y
		///			 - Position.z
		///			 - TexCoord.x
		///			 - TexCoord.y
		/// Vertex2: ...
		/// ...
		/// Vertex4: ...
		//////////////////////////////////////////////////////////////
		const vxF32* GetVertexData() const;

		virtual void Draw();

	private:

		static ResourceHandle kDefaultShader;

		void UpdatePositions();
		void UpdateTexCoords();

		ResourceHandle mTexture;
		IntRect mSubset;
		ResourceHandle mShader;

		vxF32 mVertexData[20];
	};
}

#endif // SPRITE_HPP
