#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Graphics/DataType.hpp>
#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Renderer/IDrawable.hpp>
#include <VoxE/Renderer/Material.hpp>
#include <VoxE/Renderer/PolygonMode.hpp>
#include <VoxE/Renderer/RenderContext.hpp>
#include <VoxE/Renderer/TextureManager.hpp>
#include <VoxE/Scene/Camera.hpp>

namespace VX
{
	class Scene;

	//////////////////////////////////////////////////////////////
	/// \class Renderer
	/// This class performs the basic primitive submission operations
	/// needed when rendering any primitves by making calls into the
	/// underlying graphics driver's API.
	//////////////////////////////////////////////////////////////
	class Renderer : public ISingleton<Renderer>
	{
	public:

		Renderer();
		~Renderer();

		//////////////////////////////////////////////////////////////
		/// Applies the given material thereby passing all provided
		/// shader parameters and setting up the shader for use.
		/// \param mat The material to apply.
		/// \param force If set to true the renderer will reset the
		///		material even if the name's are the same. This is
		///		used for example by sprites where there is no name
		///		available as the materials are generated at runtime.
		//////////////////////////////////////////////////////////////
		void ApplyMaterial(const Material* mat, const vxBool force = false);
		//////////////////////////////////////////////////////////////
		/// Applies the given transform to the shader by passing it
		/// down to the world matrix.
		//////////////////////////////////////////////////////////////
		void ApplyTransform(const Matrix4& t);
		//////////////////////////////////////////////////////////////
		/// Applies the given camera thereby passing down the view and
		/// the projection matrix.
		//////////////////////////////////////////////////////////////
		void ApplyCamera(const Camera& c);
		//////////////////////////////////////////////////////////////
		/// Sets the viewport the graphics API should use for projecting
		/// fragments onto the screen.
		/// \param x The x-coordinate of the viewport in pixels.
		/// \param y The y-coordinate of the viewport in pixels.
		/// \param width The width of the viewport rectangle in pixels.
		/// \param height The height of the viewport rectangle in pixels.
		//////////////////////////////////////////////////////////////
		void SetViewport(const vxI32 x, const vxI32 y, const vxI32 width, const vxI32 height);
		//////////////////////////////////////////////////////////////
		/// Draws an array of un-indexed primitives.
		/// \param mode The polygon mode to use when drawing the
		///		primitives.
		/// \param first The number of the first vertex to draw.
		/// \param count The number of vertices to draw beginning from
		///		the given first vertex.
		//////////////////////////////////////////////////////////////
		void DrawPrimitives(const PolygonMode& mode, const vxI32 first, const vxI32 count);
		//////////////////////////////////////////////////////////////
		/// Draws an array of indexed primitives.
		/// \param mode The polygon mode to use when drawing the
		///		primitives.
		/// \param count The number of elements to draw.
		/// \param type The type of the indices, i.e. how much memory
		///		a single index occuppies. Must be either GL_TYPE_UBYTE,
		///		GL_TYPE_USHORT or GL_TYPE_UINT.
		/// \param indices The pointer to where the indices are stored.
		///		If currently there is a buffer bound to
		///		GL_ELEMENT_ARRAY_BUFFER this pointer will describe an
		///		offset into the buffer.
		//////////////////////////////////////////////////////////////
		void DrawPrimitivesI(const PolygonMode& mode, const vxI32 count, const DataType& type, const void* indices);

	private:

		Material mLastMaterial;
		Scene* mLockedScene;

	};
}

#endif // RENDERER_HPP
