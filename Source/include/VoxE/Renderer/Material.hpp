#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Resources/ResourceHandle.hpp>

#include <list>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Material Variant
	/// Describes a variable type supporting all types used in a
	/// material file in order to describe a material attribute.
	//////////////////////////////////////////////////////////////
	class MaterialVariant
	{
	public:

		enum Type
		{
			TEXTURE,
			FLOAT,
			INT,
			UINT,
			BOOL
		};

		MaterialVariant();
		MaterialVariant(const MaterialVariant& v);
		MaterialVariant(const vxString& t);
		MaterialVariant(const vxF32 f);
		MaterialVariant(const vxI32 i);
		MaterialVariant(const vxU32 u);
		MaterialVariant(const vxBool b);
		~MaterialVariant();

		Type GetType() const;
		vxBool IsTexture() const;
		vxBool IsFloat() const;
		vxBool IsInt() const;
		vxBool IsUInt() const;
		vxBool IsBool() const;

		vxString ToTexture() const;
		vxF32 ToFloat() const;
		vxI32 ToInt() const;
		vxU32 ToUInt() const;
		vxBool ToBool() const;

		void SetTexture(const vxString& t);
		void SetFloat(const vxF32 f);
		void SetInt(const vxI32 i);
		void SetUInt(const vxU32 u);
		void SetBool(const vxBool b);

		MaterialVariant& operator= (const MaterialVariant& v);

	private:

		void Copy(const MaterialVariant& v);
		void Destroy();

		vxU32* mReferences;
		Type mType;
		union
		{
			vxCCharPtr mAsTexture;
			vxF32 mAsFloat;
			vxI32 mAsInt;
			vxU32 mAsUInt;
			vxBool mAsBool;
		};
	};

	//////////////////////////////////////////////////////////////
	/// \struct MaterialParameter
	/// A named parameter together with its value.
	//////////////////////////////////////////////////////////////
	struct MaterialParameter
	{
		vxString mName;
		MaterialVariant mValue;
	};

	//////////////////////////////////////////////////////////////
	/// \class Material
	/// This class represents a material used for rendering. A
	/// material consists of a shader and one or more optional
	/// textures. Currently up to 4 different textures and two
	/// different texture coordinate pairs are supported / allowed.
	//////////////////////////////////////////////////////////////
	struct Material
	{
	public:

		vxString mName;
		ResourceHandle mShader;
		std::list<MaterialParameter> mParameters;
	};
}

#endif // MATERIAL_HPP
