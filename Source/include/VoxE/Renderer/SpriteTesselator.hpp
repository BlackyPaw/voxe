/*
#ifndef SPRITE_TESSELATOR_HPP
#define SPRITE_TESSELATOR_HPP

#include <VoxE/Graphics/Buffer.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Renderer/Renderer.hpp>
#include <VoxE/Renderer/Sprite.hpp>
#include <map>
#include <vector>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class SpriteTesselator
	/// The SpriteTesselator groups together draw calls for
	/// simple 2-dimensional sprites (i.e. Quads consisting of two
	///	triangles). Internally a vertex and index buffer is used
	/// to group together the vertex attributes of multiple
	/// sprites. Please note that this class can only group
	/// together sprites which share the same texture simple due
	/// to the fact that it is not possible to encode GPU states
	/// into a buffer. And even if it was it would be much more
	/// optimized to sort sprites by their respective material
	/// first before rendering.
	//////////////////////////////////////////////////////////////
	class SpriteTesselator
	{
	public:

		//////////////////////////////////////////////////////////////
		/// Creates a tesselator that allows for up to max sprites to
		/// be cached in.
		/// \param max The maximum number of sprites to be cached in
		///		simultaneously.
		//////////////////////////////////////////////////////////////
		SpriteTesselator(const vxU32 max = 50);
		~SpriteTesselator();

		//////////////////////////////////////////////////////////////
		/// Draws a sprite, i.e. adds it to the process of batching
		/// that is done when the tesselator is flushed. If the number
		/// of cached sprites exceeds the maximum number of sprites
		/// allowed the tesselator is flushed.
		/// \param sprite The sprite to draw.
		/// \see Flush()
		//////////////////////////////////////////////////////////////
		void Draw(Sprite& sprite, RenderContext& c, Renderer& r);
		//////////////////////////////////////////////////////////////
		/// Flushes the tesselator thereby drawing all data that has
		/// been cached in so far. All pointer will then be invalidated
		/// so the cached sprite objects may be deleted.
		/// \param c The render context which describes the projection
		///		matrix to use and the shader as well as the texture.
		/// \param r The renderer to use for rendering.
		//////////////////////////////////////////////////////////////
		void Flush(RenderContext& c, Renderer& r);

	private:

		void* Advance(void* p, const vxU32 n);

		Buffer mVertexBuffer;
		Buffer mIndexBuffer;

		void* mVertexMap;
		void* mIndexMap;

		const vxU32 mMaxSprites;
		vxU32 mNumSprites;
	};
}

#endif // SPRITE_TESSELATOR_HPP
*/
