#ifndef WINDOW_SIZE_EVENT_HPP
#define WINDOW_SIZE_EVENT_HPP

#include <VoxE/Core/Event.hpp>
#include <VoxE/Core/Vector.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class WindowSizeEvent
	/// This event is sent whenever the size of the game's window
	/// changes. It contains the window's new size.
	//////////////////////////////////////////////////////////////
	class WindowSizeEvent : public Event
	{
	public:

		WindowSizeEvent(const Vector2ui& size);

		DECLARE_EVENT(0xF8E62CD5);

		//////////////////////////////////////////////////////////////
		/// Gets the size the window has been resized to.
		//////////////////////////////////////////////////////////////
		const Vector2ui& GetSize() const;

	private:

		Vector2ui mSize;
	};
}

#endif // WINDOW_SIZE_EVENT_HPP