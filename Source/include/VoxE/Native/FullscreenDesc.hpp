#ifndef FULLSCREEN_DESC_HPP
#define FULLSCREEN_DESC_HPP

#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Core/Vector.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \struct FullscreenDesc
	/// Simple structure for describing more advanced properties
	/// of a window if it is shown in fullscreen mode.
	//////////////////////////////////////////////////////////////
	struct FullscreenDesc
	{
		vxBool mFullscreen;
		Vector2ui mResolution;
		vxU32 mBitsPerPixel;
	};
}

#endif // FULLSCREEN_DESC_HPP