#ifndef FILE_HPP
#define FILE_HPP

#include <VoxE/Core/String.hpp>
#include <VoxE/Native/Path.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class File
	/// Provides a platform-independent interface for describing
	/// and retrieving a file's properties like its path, or its
	/// size in bytes.
	//////////////////////////////////////////////////////////////
	class File
	{
	public:

		File(const vxString& path);
		File(const Path& path);
		~File();

		//////////////////////////////////////////////////////////////
		/// Gets the file's path. It may be used for further querying
		/// file attributes such as its existance or its fully
		/// qualified path.
		//////////////////////////////////////////////////////////////
		const Path& GetPath() const;
		//////////////////////////////////////////////////////////////
		/// Gets the file's size in bytes.
		//////////////////////////////////////////////////////////////
		vxU32 GetSize() const;

	private:

		void DetectMetadata();

		Path mPath;
		vxU32 mSize;
	};
}

#endif // FILE_HPP