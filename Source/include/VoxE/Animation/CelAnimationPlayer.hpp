#ifndef ANIMATION_PLAYER_HPP
#define ANIMATION_PLAYER_HPP

#include <VoxE/Animation/CelAnimation.hpp>
#include <VoxE/Resources/ResourceHandle.hpp>

namespace VX
{
	class CelAnimationPlayer
	{
	public:

		CelAnimationPlayer(const ResourceHandle& anim);

		void Update(const vxF32 delta);

		void Play();
		void Stop();

		const IntRect& GetCurrentState() const;

	private:

		ResourceHandle mAnimation;
		vxBool mIsPlaying;
		vxF32 mTime;
		IntRect mCurState;
		vxU32 mIndex;
	};
}

#endif // ANIMATION_PLAYER_HPP