#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include <VoxE/Core/Rect.hpp>
#include <vector>

namespace VX
{
	class CelAnimation
	{
	public:

		CelAnimation();
		CelAnimation(const vxF32 speed, const std::vector<IntRect>& states);

		const vxF32 GetSpeed() const;
		const IntRect& GetState(const vxU32 i) const;
		const vxU32 GetNumStates() const;

	private:

		vxF32 mSpeed;
		std::vector<IntRect> mStates;
	};
}

#endif // ANIMATION_HPP