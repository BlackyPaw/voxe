/*
 * PoolAllocator.hpp
 *
 *  Created on: 31.05.2014
 *      Author: euaconlabs
 */

#ifndef POOLALLOCATOR_HPP_
#define POOLALLOCATOR_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class PoolAllocator
	/// The pool allocator provides a way of optimizing memory
	/// allocation whenever equally sized blocks of continuous
	/// memory are needed. In that case the pool allocator is the
	/// perfect choice to make.
	//////////////////////////////////////////////////////////////
	class PoolAllocator
	{
	public:

		PoolAllocator();
		~PoolAllocator();

		//////////////////////////////////////////////////////////////
		/// Initializes the pool allocator and reserves an amount of
		/// memory depending on the given input parameters.
		/// \param size The size of each element in bytes.
		/// \param count The number of elements to reserve memory for.
		/// \returns True on success, False if either memory allocation
		/// 	fails or the allocator has already been created once successfully.
		//////////////////////////////////////////////////////////////
		vxBool Create(const vxU32 size, const vxU32 count);

		//////////////////////////////////////////////////////////////
		/// Allocates one block of free memory and returns a pointer
		/// to it. If all memory the pool allocator reserved is occupied
		/// a nullptr will be returned.
		//////////////////////////////////////////////////////////////
		void* Allocate();
		//////////////////////////////////////////////////////////////
		/// Frees the block of memory pointed to by p.
		//////////////////////////////////////////////////////////////
		void Free(void* p);

	private:

		vxU8* mMemory;
		vxBool* mBlocks;
		vxU32 mSize;
		vxU32 mCount;
	};
}

#endif /* POOLALLOCATOR_HPP_ */
