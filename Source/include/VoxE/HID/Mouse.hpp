#ifndef MOUSE_HPP
#define MOUSE_HPP

#include <VoxE/Core/Event.hpp>
#include <VoxE/Core/Vector.hpp>
#include <VoxE/HID/MouseButtons.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Mouse
	/// This class keeps track of any mouse changes which occur.
	/// It always holds the mouse's current position and whether
	/// any mouse button is currently held down or released.
	//////////////////////////////////////////////////////////////
	class Mouse
	{
	public:

		Mouse();
		~Mouse();

		//////////////////////////////////////////////////////////////
		/// Gets the mouse's current coordinates.
		//////////////////////////////////////////////////////////////
		const Vector2i& GetCoords() const;
		//////////////////////////////////////////////////////////////
		/// Checks whether the given mouse button is currently pressed.
		/// \param button The mouse button to check.
		//////////////////////////////////////////////////////////////
		vxBool IsButtonDown(const MouseButtons& button) const;
		//////////////////////////////////////////////////////////////
		/// Checks whether the given mouse button is currently released.
		/// \param button The mouse button to check.
		//////////////////////////////////////////////////////////////
		vxBool IsButtonUp(const MouseButtons& button) const;

		//////////////////////////////////////////////////////////////
		/// Resets the state of all cached mouse buttons values.
		/// This is done to avoid input leaks for exmaple on a state
		/// change.
		//////////////////////////////////////////////////////////////
		void ResetState();

	private:

		void OnMouseDown(EventPtr& e);
		void OnMouseUp(EventPtr& e);
		void OnMouseMove(EventPtr& e);

		Vector2i mCoords;
		vxBool mButtons[MOUSE_NUM_BUTTONS];
	};
}

#endif // MOUSE_HPP