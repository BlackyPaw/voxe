#ifndef KEY_DOWN_EVENT_HPP
#define KEY_DOWN_EVENT_HPP

#include <VoxE/Core/Event.hpp>
#include <VoxE/HID/KeyboardKeys.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class KeyDownEvent
	/// This event is sent whenever a key on a keyboard is pressed.
	/// It is sent only once when the key gets pressed. Every
	/// KeyDownEvent is matched with a closing KeyUpEvent.
	/// \see KeyUpEvent
	//////////////////////////////////////////////////////////////
	class KeyDownEvent : public Event
	{
	public:

		//////////////////////////////////////////////////////////////
		/// Constructs a KeyDownEvent.
		/// \param key The key which has been pressed.
		//////////////////////////////////////////////////////////////
		KeyDownEvent(const KeyboardKeys& key);

		DECLARE_EVENT(0x64D07DA0)

		//////////////////////////////////////////////////////////////
		/// Gets the key that has been pressed, i.e. the key the
		/// event contains.
		//////////////////////////////////////////////////////////////
		const KeyboardKeys& GetKey() const;

	private:

		const KeyboardKeys mKey;
	};
}

#endif // KEY_DOWN_EVENT_HPP