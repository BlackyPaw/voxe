#ifndef KEYBOARD_HPP
#define KEYBOARD_HPP

#include <VoxE/Core/Event.hpp>
#include <VoxE/HID/KeyboardKeys.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Keyboard
	/// This class allows for easily querying the keyboard's
	/// current state in an asynchronous way. It keeps track of
	/// all occuring keyboard-related event and manages a list of
	/// keystates which may be queried from every without the nedd
	/// to perform a native OS call.
	/// On construction all keys are assumed to be in a released
	/// state so only new KeyDown and KeyUp events will respond
	/// in a state change.
	/// \see KeyDownEvent
	/// \see KeyUpEvent
	//////////////////////////////////////////////////////////////
	class Keyboard
	{
	public:

		Keyboard();
		~Keyboard();

		//////////////////////////////////////////////////////////////
		/// This method reset all internally cached key states.
		/// This results in avoiding input leaks, meaning that for
		/// example on a state change the input is not left in an
		/// invalid state.
		//////////////////////////////////////////////////////////////
		void ResetKeys();

		//////////////////////////////////////////////////////////////
		/// Checks whether the given key is pressed or not.
		/// \param key The key to check.
		//////////////////////////////////////////////////////////////
		vxBool IsPressed(const KeyboardKeys& key) const;
		//////////////////////////////////////////////////////////////
		/// Checks whether the given key is released or not.
		/// \param key The key to check.
		//////////////////////////////////////////////////////////////
		vxBool IsReleased(const KeyboardKeys& key) const;

	private:

		void OnKeyDown(EventPtr& e);
		void OnKeyUp(EventPtr& e);

		vxBool mStates[KEYBOARD_NUM_KEYS];
	};
}

#endif // KEYBOARD_HPP