#ifndef MOUSE_UP_EVENT_HPP
#define MOUSE_UP_EVENT_HPP

#include <VoxE/Core/Event.hpp>
#include <VoxE/Core/Vector.hpp>
#include <VoxE/HID/MouseButtons.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class MouseUpEvent
	/// This event is sent whenever is mouse button is released.
	//////////////////////////////////////////////////////////////
	class MouseUpEvent : public Event
	{
	public:

		MouseUpEvent(const MouseButtons& button, const Vector2i& coords);

		DECLARE_EVENT(0x31D19CE3);

		//////////////////////////////////////////////////////////////
		/// Gets the mouse button which was released.
		//////////////////////////////////////////////////////////////
		const MouseButtons& GetButton() const;
		//////////////////////////////////////////////////////////////
		/// Gets the coordinates where the button was pressed down.
		//////////////////////////////////////////////////////////////
		const Vector2i& GetCoords() const;

	private:

		MouseButtons mButton;
		Vector2i mCoords;
	};
}

#endif // MOUSE_UP_EVENT_HPP