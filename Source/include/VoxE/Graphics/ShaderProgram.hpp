#ifndef SHADER_PROGRAM_HPP
#define SHADER_PROGRAM_HPP

#include <VoxE/Core/Array.hpp>
#include <VoxE/Core/String.hpp>
#include <VoxE/Graphics/DataType.hpp>
#include <VoxE/Graphics/Shader.hpp>
#include <VoxE/Native/Atomic.hpp>

#include <list>
#include <map>

namespace VX
{
	static const vxU32 kInvalidShaderArgIndex = 0xFFFFFFFF;

	enum ShaderSemantic
	{
		// Vertex Attributes:

		SHADER_SEMANTIC_POSITION = 0,
		SHADER_SEMANTIC_NORMAL,
		SHADER_SEMANTIC_COLOR,
		SHADER_SEMANTIC_TANGENT,
		SHADER_SEMANTIC_TEX_COORDS0,
		SHADER_SEMANTIC_TEX_COORDS1,

		// Uniforms:
		
		SHADER_SEMANTIC_PROJECTION_MATRIX,
		SHADER_SEMANTIC_VIEW_MATRIX,
		SHADER_SEMANTIC_WORLD_MATRIX,
		SHADER_SEMANTIC_NORMAL_MATRIX,
		SHADER_SEMANTIC_TEXTURE_MATRIX,

		SHADER_SEMANTIC_COUNTER
	};

	enum ShaderMappingType
	{
		SHADER_MAPPING_TYPE_TEXTURE,
		SHADER_MAPPING_TYPE_FLOAT,
		SHADER_MAPPING_TYPE_INT,
		SHADER_MAPPING_TYPE_UINT,
		SHADER_MAPPING_TYPE_BOOL,

		SHADER_MAPPING_TYPE_INVALID
	};

	struct ShaderMapping
	{
		vxString mName;
		vxString mParam;
		ShaderMappingType mType;
	};

	//////////////////////////////////////////////////////////////
	/// \class ShaderProgram
	/// This class wraps the functionality of an OpenGL shader
	/// program. It is able to link shaders together and to bind
	/// the underlying OpenGL shader program.
	/// This class is self-reference counting and may therefore be
	/// freely passed by reference.
	/// \see Shader
	//////////////////////////////////////////////////////////////
	class ShaderProgram
	{
	public:

		ShaderProgram();
		ShaderProgram(const ShaderProgram& p);
		~ShaderProgram();

		//////////////////////////////////////////////////////////////
		/// This method will add a shader to the shader programs shader
		/// list if it has not been linked yet. If it has been linked
		/// already this method will effectively do nothing. A reference
		/// to this shader will be kept until either RemoveShaders()
		/// is called or the program is <b>successfully</b> linked.
		//////////////////////////////////////////////////////////////
		void AddShader(const Shader& s);
		//////////////////////////////////////////////////////////////
		/// Removes all shader references from the program if there
		/// are any.
		//////////////////////////////////////////////////////////////
		void RemoveShaders();
		//////////////////////////////////////////////////////////////
		/// Tries to link the program. If VOXE_DEBUG is defined and
		/// the compilation fails the method will check if it is
		/// given a pointer to a character buffer it may write the
		/// info log to. If so, it will get the information log containing
		/// some information on why the linkage failed.
		/// \param[out] This pointer will contain the info log if the
		///		specified criterias are met.
		/// \param maxLen The maximum number of characters which may
		///		be written into the character array pointed to by
		///		pLog.
		/// \returns True, if the program could be linked successfully,
		///		or false otherwise.
		//////////////////////////////////////////////////////////////
		vxBool Link(vxChar** pLog = nullptr, const vxU32 maxLen = 0);

		//////////////////////////////////////////////////////////////
		/// This method will only do anything if the program has been
		/// successfully linked yet.
		/// This method links a variable to a semantic thereby allowing
		/// the rendering engine to pass actual vertex / uniform data
		/// to the shader. A semantic basically is a descriptor which
		/// is used by the engine to figure out what data to pass to
		/// a variable.
		/// The semantic may later be resolved to a variable by a call
		/// to ResolveSemantic().
		/// Please note though that only one variable may be assigned
		/// to a semantic if a semantic is "re"-assigned the old value
		/// will be overridden.
		/// \param var The name of the variable to assign.
		/// \param sem The semantic to assign.
		/// \see ResolveSemantic()
		//////////////////////////////////////////////////////////////
		void AssignSemantic(const vxString& var, const ShaderSemantic& sem);
		//////////////////////////////////////////////////////////////
		/// Resolves a semantic to a shader argument index. If the
		/// semantic is not assigned the method will
		/// return kInvalidShaderArgIndex.
		/// \param sem The semantic to resolve.
		//////////////////////////////////////////////////////////////
		vxU32 ResolveSemantic(const ShaderSemantic& sem);
		//////////////////////////////////////////////////////////////
		/// Resolves a shader variable by its name. If the variable is
		/// not found kInvalidShaderArgIndex is returned.
		/// \param name The name of the variable to resolve.
		/// \returns The shader argument index of the resolved variable.
		//////////////////////////////////////////////////////////////
		vxU32 ResolveName(const vxString& name);
		//////////////////////////////////////////////////////////////
		/// Resolves a shader mapping by its name and returns the index
		/// of the shader parameter.
		/// \param name The name of the variable to resolve.
		/// \returns The shader argument index of the resolved variable.
		//////////////////////////////////////////////////////////////
		vxU32 ResolveMapping(const vxString& name);
		//////////////////////////////////////////////////////////////
		/// Adds the given shader mapping to the shader program's list
		/// of mappings.
		/// \param mapping The shader mapping to add.
		//////////////////////////////////////////////////////////////
		void AddMapping(const ShaderMapping& mapping);

		//////////////////////////////////////////////////////////////
		/// Enables a vertex attribute for use given its shader argument
		/// index.
		/// \param argIdx The shader argument index of the attribute
		///		to enable.
		//////////////////////////////////////////////////////////////
		void EnableAttribute(const vxU32 argIdx);
		//////////////////////////////////////////////////////////////
		/// Sets the vertex attribute data of an enabled vertex
		/// attribute using a pointer to the data given the semantic
		/// it is bound to.
		/// \param argIdx The shader argument index of the vertex attribute
		///		whose data is to be set.
		/// \param size The number of components per vertex attribute.
		///		Valid values are 1 to 4.
		/// \param type The data type of each component in the array
		///		pointed to by the given pointer.
		/// \param normalized Specifies, whether fixed-point data values
		///		should be normalized or not.
		/// \param stride The offset between consecutive vertex attributes
		///		in bytes.
		/// \param pointer The pointer to the array containing the vertex
		///		attribute data. Can be either an offset into the currently
		///		bound GL_ARRAY_BUFFER or a pointer to the data directly.
		//////////////////////////////////////////////////////////////
		void AttributeData(const vxU32 argIdx, const vxU32 size, const DataType& type, const vxBool normalized, const vxU32 stride, const void* pointer);
		//////////////////////////////////////////////////////////////
		/// Disables a vertex attribute given its shader argument index.
		/// \param argIdx The shader argument index of the attribute
		///		to enable.
		//////////////////////////////////////////////////////////////
		void DisableAttribute(const vxU32 argIdx);

		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of one single component as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform1(const vxU32 argIdx, const vxU32 count, const vxF32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of one single component as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform1(const vxU32 argIdx, const vxU32 count, const vxI32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of one single component as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform1(const vxU32 argIdx, const vxU32 count, const vxU32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 2 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform2(const vxU32 argIdx, const vxU32 count, const vxF32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 2 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform2(const vxU32 argIdx, const vxU32 count, const vxI32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 2 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform2(const vxU32 argIdx, const vxU32 count, const vxU32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 3 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform3(const vxU32 argIdx, const vxU32 count, const vxF32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 3 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform3(const vxU32 argIdx, const vxU32 count, const vxI32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 3 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform3(const vxU32 argIdx, const vxU32 count, const vxU32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 4 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform4(const vxU32 argIdx, const vxU32 count, const vxF32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 4 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform4(const vxU32 argIdx, const vxU32 count, const vxI32* value);
		//////////////////////////////////////////////////////////////
		/// Sets either the value of a single uniform v or the values
		/// of a whole uniform array. count specifies the number of
		/// variables to change (i.e. 1 for one single uniform variable
		/// or n for an uniform variable array of size n). Each of these
		/// uniform values consits of 4 components as the name
		/// implies.
		/// Supported pointer types are vxF32*, vxI32* and vxU32*.
		/// \param argIdx The shader argument index of the uniform to set
		/// \param count See description.
		/// \param value See description.
		//////////////////////////////////////////////////////////////
		void Uniform4(const vxU32 argIdx, const vxU32 count, const vxU32* value);

		//////////////////////////////////////////////////////////////
		/// Sets either one single uniform 4x4 matrix containing float
		/// elements or an array of them.
		/// \param argIdx The shader argument index of the uniform
		///		variable to set.
		/// \param count The number of uniform variables to set. 1 for
		///		a single one, or any number for an array of that size.
		/// \param transpose If set to true the matrix is assumed to be
		///		given in row-major order. If set to false it is assumed
		///		to be given in column-major order.
		/// \param value The pointer to the actual values to set.
		//////////////////////////////////////////////////////////////
		void UniformMatrix4x4(const vxU32 argIdx, const vxU32 count, const vxBool transpose, const vxF32* value);

		//////////////////////////////////////////////////////////////
		/// Binds the program for use.
		//////////////////////////////////////////////////////////////
		void Bind();

		ShaderProgram& operator= (const ShaderProgram& p);

	private:

		void Copy(const ShaderProgram& p);
		void Destroy();

		vxU32* mReferences;
		vxU32 mHandle;
		Array<Shader> mShaders;
		std::map<vxString, vxU32> mDeclaredVars;
		std::map<ShaderSemantic, vxU32> mSemantics;
		std::map<vxString, ShaderMapping> mMappings;
	};
}

#endif // SHADER_PROGRAM_HPP
