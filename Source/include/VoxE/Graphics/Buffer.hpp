#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <VoxE/Graphics/BufferType.hpp>
#include <VoxE/Graphics/GLConstants.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Buffer
	/// This class provides a convenient interface for acessing
	/// OpenGL buffer objects in a consisten manner. It declares
	/// some functions which are available for all types of
	/// buffers.
	/// This class is self-reference counting so it may be passed
	/// by reference.
	//////////////////////////////////////////////////////////////
	class Buffer
	{
	public:

		Buffer();
		Buffer(const Buffer& buffer);
		~Buffer();

		//////////////////////////////////////////////////////////////
		/// Creates a buffer of the specified buffer type possessing
		/// an immutable memory storage. Immutable means that after
		/// creation the buffer's content are not supposed to be
		/// changed. Therefore glBufferData and similiar operations
		/// won't work. The only possibility is to invalidate the
		/// buffer.
		/// \param type The type of the buffer.
		/// \param size The size of the buffer in bytes.
		/// \param data The pointer pointing to the data to fill the
		///		buffer with. Has to be exactly size bytes in size.
		/// \param flags A bitwise combination of one or more flags
		///		which give hints about how the buffer is intended to
		///		be used. See <a href="http://www.opengl.org/wiki/Buffer_Object#Immutable_access_methods">OpenGL's glBufferStorage documentation</a>
		///		fore more details on this parameter.
		/// \returns True on success; False on failure.
		/// \remark Automatically binds the buffer after creation.
		//////////////////////////////////////////////////////////////
		vxBool CreateImmutable(const BufferType& type, const vxI32 size, const void* data, const vxU32 flags = 0x0000);
		//////////////////////////////////////////////////////////////
		/// Create a buffer of the specified buffer type possessing
		/// a mutable memory storage. This allocates a general purpose
		/// block of memory in OpenGL. Furhtermore a usage hint is
		/// given so that the underlying implementation may perform
		/// some optimizations based on what information it is given.
		/// See <a href="http://www.opengl.org/wiki/Buffer_Object#Mutable_Storage">OpenGL's glBufferData documentation</a>
		/// for a more detailed discussion on that topic.
		/// \param type The type of the buffer.
		/// \param size The size of the buffer in bytes.
		/// \param data The pointer pointing to the data to fill the
		///		buffer with. Has to be exactly size bytes in size.
		/// \param usage Gives a usage hint on how the data inside the
		///		buffer will be used. See the above link for more
		///		information.
		/// \returns True on success; False on failure.
		/// \remark Automatically binds the buffer after creation.
		//////////////////////////////////////////////////////////////
		vxBool CreateMutable(const BufferType& type, const vxI32 size, const void* data, const vxU32 usage = GL_STATIC_DRAW);

		//////////////////////////////////////////////////////////////
		/// Updates a buffer's contents without reallocation it.
		/// \param offset The offset into the buffer from which to
		///		start changing the its contents from.
		/// \param size The number of bytes to change beginning at
		///		offset.
		/// \param data The pointer to the data. Has to be exactly
		///		size bytes in size.
		/// \remark The buffer has to be bound otherwise this operation
		///		will have no effect.
		//////////////////////////////////////////////////////////////
		void SubData(const vxI32 offset, const vxI32 size, const void* data);

		//////////////////////////////////////////////////////////////
		/// Maps the whole buffers content and returns a usable
		/// pointer. As long as the buffer is mapped all accesses will
		/// fail.
		/// \param access A set of flags describing the planned use of
		///		the buffer.
		/// \returns A pointer to the mapped buffer.
		/// \remark See <a href="http://www.opengl.org/wiki/GLAPI/glMapBuffer">OpenGL's documentation on glMapBufferRange</a> for
		///		more information.
		/// \remark The buffer has to be bound otherwise this operation
		///		will have no effect.
		//////////////////////////////////////////////////////////////
		void* Map(const vxU32 access);
		//////////////////////////////////////////////////////////////
		/// Maps the a range of the buffers content and returns a
		/// usable pointer. All accesses on the buffer will fail as
		/// long as it is mapped.
		/// \param offset An offset in bytes to add into the buffer.
		/// \param length The length of the range to map in bytes.
		/// \param access A set of flags describing the planned use of
		///		the buffer.
		/// \returns A pointer to the mapped buffer.
		/// \remark See <a href="http://www.opengl.org/wiki/GLAPI/glMapBufferRange">OpenGL's documentation on glMapBufferRange</a> for
		///		more information.
		/// \remark The buffer has to be bound otherwise this operation
		///		will have no effect.
		//////////////////////////////////////////////////////////////
		void* MapRange(const vxI32 offset, const vxI32 length, const vxU32 access);
		//////////////////////////////////////////////////////////////
		/// Unmaps the buffer thereby invalidating the pointer returned
		/// by either Map or MapRange and making the buffer available
		/// for use again.
		/// \remark The buffer has to be bound otherwise this operation
		///		will have no effect.
		//////////////////////////////////////////////////////////////
		void UnMap();

		//////////////////////////////////////////////////////////////
		/// Gets the type of the buffer this buffer is created as.
		//////////////////////////////////////////////////////////////
		const BufferType& GetBufferType() const;
		//////////////////////////////////////////////////////////////
		/// Binds the buffer to the target of the type it has been
		/// created with.
		//////////////////////////////////////////////////////////////
		void Bind();
		//////////////////////////////////////////////////////////////
		/// Unbinds the  buffer from the target of the type it has
		/// been created with.
		//////////////////////////////////////////////////////////////
		void UnBind();

		Buffer& operator= (const Buffer& b);

	private:

		void Copy(const Buffer& b);
		void Destroy();

		vxU32* mReferences;
		vxU32 mHandle;
		BufferType mType;
		vxU32 mTypeEnum;
	};
}

#endif // BUFFER_HPP