#ifndef SHADER_HPP
#define SHADER_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	enum ShaderType
	{
		SHADER_TYPE_VERTEX = 0,
		SHADER_TYPE_FRAGMENT,
		SHADER_TYPE_GEOMETRY,
		SHADER_TYPE_TESS_EVALUATION,
		SHADER_TYPE_TESS_CONTROL,

		SHADER_TYPE_INVALID
	};

	//////////////////////////////////////////////////////////////
	/// \class Shader
	/// This class manages the lifetime of a single shader object
	/// in OpenGL like any vertex, fragment, geometry or tesselation
	/// shader.
	/// The shader can later be linked into a ShaderProgram which
	/// is managed by a separate class.
	/// It is self-reference counting so it may be passed by reference.
	/// \see ShaderProgram
	//////////////////////////////////////////////////////////////
	class Shader
	{
	public:

		Shader();
		Shader(const Shader& s);
		~Shader();

		//////////////////////////////////////////////////////////////
		/// Creates a shader assuming it is a shader of the given
		/// shader type, given its shader source code as well as the
		/// len of that source code in bytes.
		/// \param type The type of the shader to create.
		/// \param pSource The shader source code.
		/// \param sourceLen The length of the shader source code in
		///		bytes.
		/// \param[out] pLog A pointer to the info log which is retrieved
		///		when VOXE_DEBUG is defined, the compilation fails and
		///		pLog is non-null.
		/// \param maxLen The maximum length of the info log which may
		///		be written into the pLog pointer.
		/// \returns True, if the shader could be compiled without
		///		any failures or false otherwise.
		//////////////////////////////////////////////////////////////
		vxBool Create(const ShaderType& type, const vxChar* pSource, const vxU32 sourceLen, vxChar** pLog = nullptr, const vxU32 maxLen = 0);

		//////////////////////////////////////////////////////////////
		/// Returns the OpenGL handle of the shader or 0 if the
		/// shader has not been created (successfully) yet.
		//////////////////////////////////////////////////////////////
		const vxU32 GetHandle() const;
		//////////////////////////////////////////////////////////////
		/// Returns the type of the shader or SHADER_TYPE_INVALID
		/// if the shader has not been created (successfully) yet.
		//////////////////////////////////////////////////////////////
		const ShaderType GetType() const;

		Shader& operator= (const Shader& s);

	private:

		void Copy(const Shader& s);
		void Destroy();

		vxU32* mReferences;
		vxU32 mHandle;
		ShaderType mType;
	};
}

#endif // SHADER_HPP