#ifndef GL_CONSTANTS_HPP
#define GL_CONSTANTS_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	static const vxU32 GL_STREAM_DRAW = 0x88E0;
	static const vxU32 GL_STREAM_READ = 0x88E1;
	static const vxU32 GL_STREAM_COPY = 0x88E2;
	static const vxU32 GL_DYNAMIC_DRAW = 0x88E8;
	static const vxU32 GL_DYNAMIC_READ = 0x88E9;
	static const vxU32 GL_DYNAMIC_COPY = 0x88EA;
	static const vxU32 GL_STATIC_DRAW = 0x88E4;
	static const vxU32 GL_STATIC_READ = 0x88E5;
	static const vxU32 GL_STATIC_COPY = 0x88E6;

	static const vxU32 GL_MAP_READ_BIT = 0x0001;
	static const vxU32 GL_MAP_WRITE_BIT = 0x0002;
	static const vxU32 GL_MAP_PERSISTENT_BIT = 0x0040;
	static const vxU32 GL_MAP_COHERENT_BIT = 0x0080;
	static const vxU32 GL_MAP_INVALIDATE_RANGE_BIT = 0x0004;
	static const vxU32 GL_MAP_INVALIDATE_BUFFER_BIT = 0x0008;
	static const vxU32 GL_MAP_FLUSH_EXPLICIT_BIT = 0x0010;
	static const vxU32 GL_MAP_UNSYNCHRONIZED_BIT = 0x0020;
	static const vxU32 GL_CLIENT_STORAGE_BIT = 0x0200;
	static const vxU32 GL_DYNAMIC_STORAGE_BIT = 0x0100;

	static const vxU32 GL_ALPHA4 = 0x803B;
	static const vxU32 GL_ALPHA8 = 0x803C;
	static const vxU32 GL_ALPHA12 = 0x803D;
	static const vxU32 GL_ALPHA16 = 0x803;
	static const vxU32 GL_LUMINANCE4 = 0x803F;
	static const vxU32 GL_LUMINANCE8 = 0x8040;
	static const vxU32 GL_LUMINANCE12 = 0x8041;
	static const vxU32 GL_LUMINANCE16 = 0x8042;
	static const vxU32 GL_LUMINANCE4_ALPHA4 = 0x8043;
	static const vxU32 GL_LUMINANCE6_ALPHA2 = 0x8044;
	static const vxU32 GL_LUMINANCE8_ALPHA8 = 0x8045;
	static const vxU32 GL_LUMINANCE12_ALPHA4 = 0x8046;
	static const vxU32 GL_LUMINANCE12_ALPHA12 = 0x8047;
	static const vxU32 GL_LUMINANCE16_ALPHA16 = 0x8048;
	static const vxU32 GL_INTENSITY = 0x8049;
	static const vxU32 GL_INTENSITY4 = 0x804A;
	static const vxU32 GL_INTENSITY8 = 0x804B;
	static const vxU32 GL_INTENSITY12 = 0x804;
	static const vxU32 GL_INTENSITY16 = 0x804D;
	static const vxU32 GL_RGB4 = 0x804F;
	static const vxU32 GL_RGB5 = 0x8050;
	static const vxU32 GL_RGB8 = 0x8051;
	static const vxU32 GL_RGB10 = 0x8052;
	static const vxU32 GL_RGB12 = 0x8053;
	static const vxU32 GL_RGB16 = 0x8054;
	static const vxU32 GL_RGBA2 = 0x8055;
	static const vxU32 GL_RGBA4 = 0x8056;
	static const vxU32 GL_RGB5_A1 = 0x8057;
	static const vxU32 GL_RGBA8 = 0x8058;
	static const vxU32 GL_RGB10_A2 = 0x8059;
	static const vxU32 GL_RGBA12 = 0x805A;
	static const vxU32 GL_RGBA16 = 0x805B;

	static const vxU32 GL_READ_ONLY = 0x88B8;
	static const vxU32 GL_WRITE_ONLY = 0x88B9;
	static const vxU32 GL_READ_WRITE = 0x88BA;
}

#endif // GL_CONSTANTS_HPP