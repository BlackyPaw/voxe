#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	enum TextureFilter
	{
		TEX_FILTER_NEAREST,
		TEX_FILTER_LINEAR
	};

	enum TextureWrap
	{
		TEX_WRAP_CLAMP,
		TEX_WRAP_REPEAT
	};

	//////////////////////////////////////////////////////////////
	/// \class Texture
	/// Wrapper class around OpenGL textures. Every texture created
	/// with this class is 2-dimensional at the moment. Filters
	/// as well as wrap modes may be changed and mipmapping is
	/// WIP.
	/// This class is self-reference counting, i.e. there's no
	/// need to pass an instance around by pointer but mostly
	/// it is done anyways like in the resource cache.
	//////////////////////////////////////////////////////////////
	class Texture
	{
		friend class TextureManager;
	public:

		Texture();
		Texture(const vxU32 width, const vxU32 height, const vxU8* pixels);
		Texture(const Texture& t);
		~Texture();

		//////////////////////////////////////////////////////////////
		/// Creates the texture from an image described by the given
		/// dimensions in pixels and an array containing the actual
		/// pixel data in the 8-bit unsigned integer RGBA format.
		/// \param width The width of the image in pixels.
		/// \param height The height of the image in pixels.
		/// \param pixels The pixel data.
		//////////////////////////////////////////////////////////////
		void Create(const vxU32 width, const vxU32 height, const vxU8* pixels);
		//////////////////////////////////////////////////////////////
		/// Sets the min filter used by OpenGL to access the texture's
		/// texels when the texture is smaller than the original
		/// image.
		/// \param f The filter mode to set. Valid values are Linear
		///		and Nearest.
		/// \remark The texture has to be bound before this operation
		///		is performed. Othersie there will occur texture leaks.
		//////////////////////////////////////////////////////////////
		void SetMinFilter(const TextureFilter& f);
		//////////////////////////////////////////////////////////////
		/// Sets the magnify filter used for accessing the texture's
		/// texels when the texture is larger than the original
		/// image.
		/// \param f The filter mode to set. Valid values are Linear
		///		and Nearest.
		/// \remark The texture has to be bound before this operation
		///		is performed. Othersie there will occur texture leaks.
		//////////////////////////////////////////////////////////////
		void SetMagFilter(const TextureFilter& f);
		//////////////////////////////////////////////////////////////
		/// Sets the texture's wrap mode used when accessing the
		/// texture with texture coordinates which lie outside of the
		/// valid range (0.0-1.0) on the S-axis.
		/// \param w The wrap mode to set.
		/// \remark The texture has to be bound before this operation
		///		is performed. Othersie there will occur texture leaks.
		//////////////////////////////////////////////////////////////
		void SetWrapS(const TextureWrap& w);
		//////////////////////////////////////////////////////////////
		/// Sets the texture's wrap mode used when accessing the
		/// texture with texture coordinates which lie outside of the
		/// valid range (0.0-1.0) on the T-axis.
		/// \param w The wrap mode to set.
		/// \remark The texture has to be bound before this operation
		///		is performed. Othersie there will occur texture leaks.
		//////////////////////////////////////////////////////////////
		void SetWrapT(const TextureWrap& w);
		//////////////////////////////////////////////////////////////
		/// Gets the width of the image the texture was created from
		/// in pixels.
		//////////////////////////////////////////////////////////////
		const vxU32 GetWidth() const;
		//////////////////////////////////////////////////////////////
		/// Gets the height of the image the texture was created from
		/// in pixels.
		//////////////////////////////////////////////////////////////
		const vxU32 GetHeight() const;
		//////////////////////////////////////////////////////////////
		/// Binds the texture to the currently active texture unit.
		//////////////////////////////////////////////////////////////
		void Bind();

		vxBool operator== (const Texture& t);
		vxBool operator!= (const Texture& t);

		Texture& operator= (const Texture& t);

	private:

		vxU32 GetHandle() const;

		void Copy(const Texture& t);
		void Destroy();

		vxU32* mReferences;
		vxU32  mHandle;
		vxU32  mWidth;
		vxU32  mHeight;
	};
}

#endif // TEXTURE_HPP
