#ifndef GL_DATA_TYPES_HPP
#define GL_DATA_TYPES_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	enum DataType
	{
		DATA_TYPE_BYTE = 0,
		DATA_TYPE_UBYTE,
		DATA_TYPE_SHORT,
		DATA_TYPE_USHORT,
		DATA_TYPE_INT,
		DATA_TYPE_UINT,
		DATA_TYPE_FLOAT,
		DATA_TYPE_DOUBLE,

		DATA_TYPE_COUNTER
	};

	//////////////////////////////////////////////////////////////
	/// Translates a given DataType into its OpenGL GLenum
	/// counterpart.
	/// \param type The DataType to translate.
	//////////////////////////////////////////////////////////////
	vxU32 TranslateDataType(const DataType& type);
}

#endif // GL_DATA_TYPES_HPP