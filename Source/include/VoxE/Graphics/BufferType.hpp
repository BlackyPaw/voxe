#ifndef GL_BUFFER_TYPES_HPP
#define GL_BUFFER_TYPES_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum BufferType
	/// Describes different types of buffers within OpenGL which
	/// are definitely supported by the engine.
	//////////////////////////////////////////////////////////////
	enum BufferType
	{
		BUFFER_TYPE_VERTEX = 0,
		BUFFER_TYPE_INDEX,

		BUFFER_TYPE_COUNTER
	};

	//////////////////////////////////////////////////////////////
	/// Translates a given GLBufferType into its OpenGL GLenum
	/// counterpart.
	/// \param type The GLBufferType to translate.
	//////////////////////////////////////////////////////////////
	vxU32 TranslateBufferType(const BufferType& type);
}

#endif // GL_BUFFER_TYPES_HPP