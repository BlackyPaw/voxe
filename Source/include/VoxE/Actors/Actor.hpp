/*
 * Actor.hpp
 *
 *  Created on: 30.05.2014
 *      Author: euaconlabs
 */

#ifndef ACTOR_HPP_
#define ACTOR_HPP_

#include <VoxE/Actors/ActorComponent.hpp>
#include <VoxE/Math/ITransformable.hpp>
#include <VoxE/Scene/SceneNode.hpp>

#include <vector>

namespace VX
{
	class World;
	//////////////////////////////////////////////////////////////
	/// \class Actor
	/// This class represents actors in the VoxE Engine. Every
	/// actor can have zero or more actor components attached to
	/// it, which will be updated or drawn whenever the actor itself
	/// get, respectively.
	/// Actors are never created via
	/// \code
	/// Actor* actor = new Actor(...);
	/// \endcode
	/// Rather they are created using the current world object.
	/// The world class provides convenient functions named NewActor
	/// and DestroyActor which serve for the purpose of creating
	/// and destroying actors. This allows for integrating actors
	/// into the world's actor management structures directly as well
	/// as for implementing specialized memory allocation strategies.
	//////////////////////////////////////////////////////////////
	class Actor : public ITransformable
	{
		friend class World;
	public:

		//////////////////////////////////////////////////////////////
		/// Creates an actor.
		/// The actor is immediately attached to the given scene node.
		/// The scene node is allocated by the world in order to use
		/// specialized memory allocation strategies and to integrate
		/// it into the scene graph.
		/// Apart from the scene node the actor is also given the world
		/// it is spawned into.
		//////////////////////////////////////////////////////////////
		Actor(World* world, SceneNode* node);
		~Actor();

		//////////////////////////////////////////////////////////////
		/// Adds an actor component to the actor's list of components.
		/// Please note that an actor component must not be added to
		/// an actor twice or more times. This can cause memory
		/// violations.
		/// \param com The component to add.
		//////////////////////////////////////////////////////////////
		void AddComponent(ActorComponent* com);
		//////////////////////////////////////////////////////////////
		/// Removes an actor component from the actor's list of
		/// components. The removed actor component will not be deleted
		/// automatically. Therefore its memory must be freed manually.
		/// \param com The component to remove.
		//////////////////////////////////////////////////////////////
		void RemoveComponent(ActorComponent* com);

		//////////////////////////////////////////////////////////////
		/// Updates the actor given the time which has passed since
		/// the last frame update (delta time).
		/// \param delta
		//////////////////////////////////////////////////////////////
		void Update(const vxF32 delta);
		//////////////////////////////////////////////////////////////
		/// Draws the actor thereby calling every component's Draw()
		/// function.
		//////////////////////////////////////////////////////////////
		void Draw();

		//////////////////////////////////////////////////////////////
		// The following functions are merely wrappers around the
		// scene node since it stores the actual transformation.
		//////////////////////////////////////////////////////////////
		void Translate(const float x, const float y, const float z);
		void Translate(const Vector3f& trans);
		void Rotate(const float pitch, const float yaw, const float roll);
		void Rotate(const Vector3f& rot);
		void Scale(const float x, const float y, const float z);
		void Scale(const Vector3f& scale);

		void SetPosition(const float x, const float y, const float z);
		void SetPosition(const Vector3f& pos);
		void SetRotation(const float pitch, const float yaw, const float roll);
		void SetRotation(const Vector3f& rot);
		void SetRotation(const Quaternion& rot);
		void SetScale(const float x, const float y, const float z);
		void SetScale(const Vector3f& scale);

		const Vector3f& GetPosition() const;
		const Quaternion& GetRotation() const;
		const Vector3f GetEulerAngles() const;
		const Vector3f& GetScale() const;

		FloatRect TransformRect(const FloatRect& rect) const;

		const Matrix4& GetTransform() const;

	private:

		/// The world the actor has been spawned into:
		World* mWorld;
		/// The actor's scene node. Every actor is attached to a scene node for represenatation
		/// in the scene graph.
		SceneNode* mSceneNode;
		/// The actor's list of actor components attached to it.
		std::vector<ActorComponent*> mComponents;
	};
}

#endif /* ACTOR_HPP_ */
