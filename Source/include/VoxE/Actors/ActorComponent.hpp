/*
 * ActorComponent.hpp
 *
 *  Created on: 30.05.2014
 *      Author: euaconlabs
 */

#ifndef ACTORCOMPONENT_HPP_
#define ACTORCOMPONENT_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	typedef vxU32 ComponentID;
	static const ComponentID kInvalidComponentID = 0xFFFFFFFF;
	#define DECLARE_ACTOR_COMPONENT(id) \
	static ComponentID GetComponentID() \
	{ \
		return id; \
	}
	//////////////////////////////////////////////////////////////
	/// \class ActorComponent
	/// This class provides a basic interface for creating components
	/// to add to actors. This allows for a data-driven way of
	/// enhancing reactions of actors thereby reducing redundancy to a minimum.
	/// Every actor component should be declared inside its class
	/// declaration via DECLARE_ACTOR_COMPONENT or via a special
	/// static function:
	/// \code
	/// static ComponentID GetComponentID()
	/// {
	/// 	return [any id here];
	/// }
	/// \endcode
	//////////////////////////////////////////////////////////////
	class ActorComponent
	{
	public:

		ActorComponent(const ComponentID& id) : mID(id) {}
		virtual ~ActorComponent() {}

		DECLARE_ACTOR_COMPONENT(kInvalidComponentID)

		//////////////////////////////////////////////////////////////
		/// Gets the actor components ID.
		//////////////////////////////////////////////////////////////
		const ComponentID& GetID() const { return mID; }

		//////////////////////////////////////////////////////////////
		/// Called whenever the parent actor gets updated.
		/// Updates the component.
		/// \param delta The delta time since the last frame.
		//////////////////////////////////////////////////////////////
		virtual void Update(const vxF32 delta) = 0;
		//////////////////////////////////////////////////////////////
		/// Called whenever the parent actor should draw itself.
		//////////////////////////////////////////////////////////////
		virtual void Draw() = 0;

	private:

		ComponentID mID;
	};
}

#endif /* ACTORCOMPONENT_HPP_ */
