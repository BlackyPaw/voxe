/*
 * World.hpp
 *
 *  Created on: 30.05.2014
 *      Author: euaconlabs
 */

#ifndef WORLD_HPP_
#define WORLD_HPP_

#include <VoxE/Actors/Actor.hpp>
#include <VoxE/Memory/PoolAllocator.hpp>
#include <VoxE/Scene/Scene.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class World
	/// The world class is responsible for creating, managing and
	/// destroying actors and scene nodes. It manages all entities
	/// and a scene graph. It also uses specialized memory allocation
	/// strategies whereever it can in order to be as fast as possible.
	///
	/// By using memory allocation strategies the engines faces a
	/// tradeoff: It has to restrict itself to a certain amount
	/// of entities at a maximum. Currently this number is set to
	/// be 2048 entities at max. This number may change in the
	/// future. The same goes for the scene nodes. Only 2048 scene
	/// nodes are allowed. Therefore usually there will be less
	/// than 2048 actors since every actor needs a scene node but
	/// some space is occupied by light nodes, etc.
	//////////////////////////////////////////////////////////////
	class World
	{
	public:

		World();
		virtual ~World();

		//////////////////////////////////////////////////////////////
		/// Updates the whole world given the time which has passed
		/// since the last frame update (delta time).
		/// \param delta
		//////////////////////////////////////////////////////////////
		virtual void Update(const vxF32 delta);
		//////////////////////////////////////////////////////////////
		/// Draws the world using the scene graph. This may avoid
		/// unnecessary draws.
		//////////////////////////////////////////////////////////////
		virtual void Draw();

		//////////////////////////////////////////////////////////////
		/// Allocates a new actor and constructs it. This means
		/// populating the scene graph with it and spawning it into
		/// the world.
		/// If there is any error occurring due to memory limitations or
		/// reaching the preset limitations put onto the engine a nullptr
		/// is returned
		/// \returns The newly allocated actor (or nullptr on an error).
		/// \remark The returned actor has to be freed using DestroyActor()
		/// \see DestroyActor()
		//////////////////////////////////////////////////////////////
		Actor* NewActor();
		//////////////////////////////////////////////////////////////
		/// Destroys an actor allocated using NewActor().
		/// If the actor has not been created using NewActor() this
		/// can lead to serious memory violations.
		/// Apart from freeing the memory occupied by the actor also
		/// the actor's scene node is removed from the scene graph
		/// and being freed and the actor is despawned.
		/// \param actor The actor to destroy.
		//////////////////////////////////////////////////////////////
		void DestroyActor(Actor* actor);

	private:

		Scene mScene;
		std::list<Actor*> mActors;

		PoolAllocator mSceneNodePool;
		PoolAllocator mActorPool;
	};
}

#endif /* WORLD_HPP_ */
