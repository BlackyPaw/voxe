#ifndef SOUND_BUFFER_HPP
#define SOUND_BUFFER_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	class SoundBuffer
	{
	public:

		SoundBuffer();
		SoundBuffer(const SoundBuffer& b);
		~SoundBuffer();

		vxBool Create(const vxU32 channels, const vxU32 bitdepth, const vxU8* pcm, const vxU32 size, const vxU32 frequency);

		vxBool IsValid() const;
		vxU32 GetHandle() const;

		SoundBuffer& operator= (const SoundBuffer& b);

	private:

		void Copy(const SoundBuffer& b);
		void Destroy();

		vxU32* mReferences;
		vxU32 mHandle;
	};
}

#endif // SOUND_BUFFER_HPP