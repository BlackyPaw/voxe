#ifndef SOUND_MANAGER_HPP
#define SOUND_MANAGER_HPP

#include <VoxE/Audio/SoundSource.hpp>
#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Resources/ResourceHandle.hpp>

#include <list>

namespace VX
{
	class SoundManager : public ISingleton<SoundManager>
	{
	public:

		void Initialize();

		void PlayOneShot(const ResourceHandle& sound);

	private:

		static const vxU32 kNumSources = 5;

		SoundSource mSources[kNumSources];
	};
}

#endif // SOUND_MANAGER_HPP