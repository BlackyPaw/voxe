#ifndef AL_CONTEXT_HPP
#define AL_CONTEXT_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	class ALContext
	{
	public:

		ALContext();
		ALContext(const ALContext& c);
		~ALContext();

		vxBool Create();
		void MakeCurrent();
		vxBool IsValid() const;

		ALContext& operator= (const ALContext& c);

	private:

		void Copy(const ALContext& c);
		void Destroy();

		vxU32* mReferences;
		void* mDevice;
		void* mContext;
	};
}

#endif // AL_CONTEXT_HPP