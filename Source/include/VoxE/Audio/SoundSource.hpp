#ifndef SOUND_SOURCE_HPP
#define SOUND_SOURCE_HPP

#include <VoxE/Audio/SoundBuffer.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Resources/ResourceHandle.hpp>

namespace VX
{
	class SoundSource
	{
	public:

		SoundSource();
		SoundSource(const SoundSource& s);
		~SoundSource();

		void SetSound(const ResourceHandle& sound);
		void SetLooping(const vxBool l);
		void SetVolume(const vxF32 v);

		void Play();
		void Pause();
		void Stop();

		vxBool IsPlaying() const;

		SoundSource& operator= (const SoundSource& s);

	private:

		void Create();
		void Copy(const SoundSource& s);
		void Destroy();

		vxU32* mReferences;
		vxU32 mHandle;
	};
}

#endif // SOUND_SOURCE_HPP