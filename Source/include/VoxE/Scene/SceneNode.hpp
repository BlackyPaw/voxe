/*
 * SceneNode.hpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#ifndef SCENENODE_HPP_
#define SCENENODE_HPP_

#include <VoxE/Core/Delegate.hpp>
#include <VoxE/Math/Transformable.hpp>

#include <list>

namespace VX
{
	class Scene;
	typedef Delegate0<Priv::VoidDummy> DrawCallback;

	//////////////////////////////////////////////////////////////
	/// \class SceneNode
	/// The base class for creating custom scene nodes for use in
	/// a scene graph. Every scene node has a transformation, a
	/// parent (except for the root node) and a type which differentiates
	/// it from other types of scene nodes at runtime. Furthermore
	/// every scene node is able to have one or more child nodes.
	//////////////////////////////////////////////////////////////
	class SceneNode : public Transformable
	{
	public:

		SceneNode(Scene* scene);
		//////////////////////////////////////////////////////////////
		/// Destructor.
		/// \remark Destroys all child nodes.
		//////////////////////////////////////////////////////////////
		virtual ~SceneNode();

		//////////////////////////////////////////////////////////////
		/// Adds the given scene node as a child to this one.
		/// Therefore the parent of the given child node is set to
		/// this node.
		/// \param child The child node to add.
		//////////////////////////////////////////////////////////////
		virtual void AddChild(SceneNode* child);
		//////////////////////////////////////////////////////////////
		/// Removes the given scene node from the list of children.
		/// Afterwards the parent of the child node is set to a nullptr.
		/// \param child The child node to remove.
		/// \remark The child node is not automatically destroyed!
		//////////////////////////////////////////////////////////////
		virtual void RemoveChild(SceneNode* child);

		//////////////////////////////////////////////////////////////
		/// Sets the callback which is called whenever the node is
		/// supposed to draw itself.
		//////////////////////////////////////////////////////////////
		virtual void SetDrawCallback(const DrawCallback& callback);
		//////////////////////////////////////////////////////////////
		/// Called when the node is supposed to draw itself.
		//////////////////////////////////////////////////////////////
		virtual void Draw();

	protected:

		Scene* mScene;
		SceneNode* mParent;
		std::list<SceneNode*> mChildren;
		DrawCallback mCallback;
	};
}

#endif /* SCENENODE_HPP_ */
