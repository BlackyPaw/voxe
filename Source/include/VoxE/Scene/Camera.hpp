/*
 * Camera.hpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include <VoxE/Math/Transformable.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Camera
	/// This class manages a projection as well as a view matrix
	/// and also is a transformable object for further convenience.
	/// It is used by the renderer to receive the projection and
	/// view matrix and to easily allow for creating cameras in
	/// 3-dimensional space as well as in 2-dimensional space.
	//////////////////////////////////////////////////////////////
	class Camera : public Transformable
	{
	public:

		Camera();
		~Camera();

		//////////////////////////////////////////////////////////////
		/// Builds an orthographic projection matrix.
		/// \param left
		/// \param right
		/// \param bottom
		/// \param top
		/// \param near
		/// \param far
		//////////////////////////////////////////////////////////////
		void Orthographic(const vxF32 left, const vxF32 right, const vxF32 bottom, const vxF32 top, const vxF32 near, const vxF32 far);
		//////////////////////////////////////////////////////////////
		/// Builds a perspective projection matrix.
		/// \param fovy
		/// \param aspect
		/// \param near
		/// \param far
		//////////////////////////////////////////////////////////////
		void Perspective(const vxF32 fovy, const vxF32 aspect, const vxF32 near, const vxF32 far);

		//////////////////////////////////////////////////////////////
		/// Gets the projection matrix used by the camera.
		//////////////////////////////////////////////////////////////
		const Matrix4& GetProjection() const;

	private:

		Matrix4 mProjection;
	};
}

#endif /* CAMERA_HPP_ */
