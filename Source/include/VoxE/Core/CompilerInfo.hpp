#ifndef COMPILER_INFO_HPP
#define COMPILER_INFO_HPP

//////////////////////////////////////////////////////////////
/// This file makes some definitions in order to determine
/// the compiler used to compile the project. This is
/// necessary for some functionality like the Delegates to
/// work since they depend on various compiler optimizations
/// and hacks.
//////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
/// Visual Studio - VC (++)
//////////////////////////////////////////////////////////////
#if defined(_MSC_VER) && !defined(__MWERKS__) && !defined(__ICL) && !defined(__VECTOR_C) && !defined(__BORDLANDC__)

#define CompilerMSVC

#if (_MSC_VER < 1300)
#define CompilerMSVC6
#pragma warning(disable : 4786)
#endif

#endif

//////////////////////////////////////////////////////////////
/// Additional compiler information
//////////////////////////////////////////////////////////////
#if defined(_MSC_VER) && !defined(__MWERKS__)

#define CompilerUsingMCMFP

#if !defined(__VECTOR_C)
#define CompilerHasInheritanceKeywords
#endif

//////////////////////////////////////////////////////////////
/// Additional information used for delegates
//////////////////////////////////////////////////////////////

// #define DelegatesAllowFunctionHack

#if defined(CompilerMSVC) && (_MSC_VER >= 1310)
#define DelegatesAllowFunctionTypeSyntax
#endif

#if defined(__DMC__) || defined(__GNUC__) || defined(__ICL) || defined(__COMO__)
#define DelegatesAllowFunctionTypeSyntax
#endif

#if defined(__MWERKS__)
#define DelegatesAllowFunctionTypeSyntax
#endif

#if defined(__GNUC__)
#define DelegatesGCCWorkaround
#endif

#endif

#endif