#ifndef STRING_HPP
#define STRING_HPP

#include <VoxE/Core/Array.hpp>
#include <string>

namespace VX
{
	typedef std::string vxString;

	//////////////////////////////////////////////////////////////
	/// \class String
	/// This class provides an implementation of a simple UTF-8
	/// string. It may be used to localize strings since the plain
	/// ANSI encoding does not support any alphabet except for the
	/// English one.
	//////////////////////////////////////////////////////////////
	class String
	{
	public:

		String();
		~String();

		void Append(const String& string);
		void Append(const vxChar c);
		void Append(vxCCharPtr c);

		String Substring(const vxU32 beg, const vxU32 len);

		vxChar& operator[] (const vxU32 n);
		vxChar const& operator[] (const vxU32 n) const;

		vxU32 Size() const;
		vxU32 Length() const;

		vxCCharPtr ToANSI() const;

		vxBool operator== (const String& str) const;
		vxBool operator!= (const String& str) const;
		vxBool operator<  (const String& str) const;
		vxBool operator<= (const String& str) const;
		vxBool operator>  (const String& str) const;
		vxBool operator>= (const String& str) const;

	private:

		vxU32 mSize;
		vxU32 mLength;
		Array<vxChar> mCharacters;
	};
}

#endif // STRING_HPP