#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <VoxE/Core/ArrayIterator.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <cstring>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Array
	/// \tparam T
	/// This class allows for creating and using dynamically
	/// allocated arrays with non-static sizes using either default
	/// allocators or customly specified ones.
	/// \todo
	/// - Add the functionality for custom memory allocators.
	//////////////////////////////////////////////////////////////
	template<typename T>
	class Array
	{
	public:

		static const vxU32 kTypeSize = sizeof(T);
		typedef ArrayIterator<T> Iterator;
		typedef ArrayIterator<const T> ConstIterator;

		Array() :
			mSize(0),
			mCapacity(0),
			mAlloc(nullptr)
		{
		}

		Array(const Array<T>& a)
		{
			Copy(a);
		}

		~Array()
		{
			Destroy();
		}

		//////////////////////////////////////////////////////////////
		/// Pushes the given element to the end of the array thereby
		/// increasing the array's size by one.
		/// \param t
		//////////////////////////////////////////////////////////////
		void Push(const T& t)
		{
			if ((mSize + 1) > mCapacity)
				Resize((mCapacity * 2 >= 1 ? mCapacity * 2 : 1));

			mAlloc[mSize] = t;
			mSize++;
		}

		//////////////////////////////////////////////////////////////
		/// Pops the last element of the array.
		//////////////////////////////////////////////////////////////
		void Pop()
		{
			if (mSize > 0)
				mSize--;
		}

		//////////////////////////////////////////////////////////////
		/// Returns the element at index 0.
		/// \remark Please ensure that the array is at least one element
		/// in size before calling this function otherwise an error may
		/// be generated.
		//////////////////////////////////////////////////////////////
		T& Front()
		{
			return mAlloc[0];
		}

		//////////////////////////////////////////////////////////////
		/// Returns the element at index 0.
		/// \remark Please ensure that the array is at least one element
		/// in size before calling this function otherwise an error may
		/// be generated.
		//////////////////////////////////////////////////////////////
		const T& Front() const
		{
			return mAlloc[0];
		}

		//////////////////////////////////////////////////////////////
		/// Returns the element at the last, valid index.
		/// \remark Please ensure that the array is at least one element
		/// in size before calling this function otherwise an error may
		/// be generated.
		//////////////////////////////////////////////////////////////
		T& Back()
		{
			return mAlloc[mSize - 1];
		}

		//////////////////////////////////////////////////////////////
		/// Returns the element at the last, valid index.
		/// \remark Please ensure that the array is at least one element
		/// in size before calling this function otherwise an error may
		/// be generated.
		//////////////////////////////////////////////////////////////
		const T& Back() const
		{
			return mAlloc[mSize - 1];
		}

		//////////////////////////////////////////////////////////////
		/// Inserts the given element at the given position into the
		/// array.
		/// \param t
		/// \param pos
		//////////////////////////////////////////////////////////////
		void Insert(const T& t, const vxU32 pos)
		{
			if (pos > mSize)
				return;

			if ((mSize + 1) > mCapacity)
				Resize((mCapacity * 2 >= 1 ? mCapacity * 2 : 1));

			memmove(&mAlloc[pos + 1], &mAlloc[pos], (mSize - pos) * kTypeSize);
			mAlloc[pos] = t;
			mSize++;
		}

		//////////////////////////////////////////////////////////////
		/// Inserts the element using the given iterator.
		/// \param t
		/// \param it
		/// \returns The inserted element.
		//////////////////////////////////////////////////////////////
		Iterator Insert(const T& t, Iterator& it)
		{
			// Calculate the position index:
			T* p = &(*it);
			vxU32 idx = (reinterpret_cast<vxU8*>(p) - reinterpret_cast<vxU8*>(mAlloc)) / kTypeSize;
			Insert(t, idx);
			return Iterator(&mAlloc[idx]);
		}

		//////////////////////////////////////////////////////////////
		/// Removes the element at the given position.
		/// \param pos
		//////////////////////////////////////////////////////////////
		void Remove(const vxU32 pos)
		{
			if (pos >= mSize)
				return;

			memmove(&mAlloc[pos], &mAlloc[pos + 1], (mSize - pos) * kTypeSize);
			mSize--;
		}

		//////////////////////////////////////////////////////////////
		/// Removes an element using the given iterator.
		/// \param it
		/// \returns The iterator pointing to the element that directly
		/// followed the removed element or the end iterator.
		//////////////////////////////////////////////////////////////
		Iterator Remove(Iterator& it)
		{
			// Calculate the position index:
			T* p = &(*it);
			vxU32 idx = (reinterpret_cast<vxU8*>(p)-reinterpret_cast<vxU8*>(mAlloc)) / kTypeSize;
			Remove(idx);
			if (idx >= mSize)
				return End();
			return Iterator(&mAlloc[idx]);
		}

		//////////////////////////////////////////////////////////////
		/// Resizes the array to the given size thereby appending
		/// or deleting items respectively. For creation of those
		/// items the default constructor is called.
		//////////////////////////////////////////////////////////////
		void Resize(const vxU32 size)
		{
			if (size == 0)
			{
				Destroy();
				return;
			}

			T* alloc = new T[size];
			vxU32 min = (mCapacity > size ? size : mCapacity);
			memcpy(alloc, mAlloc, min * kTypeSize);
			mAlloc = alloc; // <- mAlloc is destroyed in Destroy().
			mCapacity = size;
		}

		//////////////////////////////////////////////////////////////
		/// Returns the begin iterator of the array.
		//////////////////////////////////////////////////////////////
		Iterator Begin()
		{
			return Iterator(mAlloc);
		}

		//////////////////////////////////////////////////////////////
		/// Returns the end iterator of the array.
		//////////////////////////////////////////////////////////////
		Iterator End()
		{
			return Iterator(&mAlloc[mSize]);
		}

		//////////////////////////////////////////////////////////////
		/// Gets the size of the array.
		//////////////////////////////////////////////////////////////
		vxU32 Size() const
		{
			return mSize;
		}

		//////////////////////////////////////////////////////////////
		/// Gets the capacity of the array
		//////////////////////////////////////////////////////////////
		vxU32 Capacity() const
		{
			return mCapacity;
		}

		//////////////////////////////////////////////////////////////
		/// Clears the array
		//////////////////////////////////////////////////////////////
		void Clear()
		{
			mSize = 0;
		}

		T& operator[] (const vxU32 n)
		{
			return mAlloc[n];
		}

		T const& operator[] (const vxU32 n) const
		{
			return mAlloc[n];
		}

		Array<T>& operator= (const Array<T>& a)
		{
			Copy(a);
			return *this;
		}

	private:

		void Destroy()
		{
			if (mAlloc != nullptr)
			{
				delete[] mAlloc;
			}
			mSize = 0;
			mCapacity = 0;
			mAlloc = nullptr;
		}

		void Copy(const Array<T>& a)
		{
			Destroy();
			if (a.mCapacity == 0)
				return;

			// Copy the memory:
			T* alloc = new T[a.mCapacity];
			memcpy(alloc, a.mAlloc, a.mCapacity * kTypeSize);

			mSize = a.mSize;
			mCapacity = a.mCapacity;
			mAlloc = a.mAlloc;
		}

		vxU32 mSize;
		vxU32 mCapacity;
		T*	  mAlloc;
	};
}

#endif // ARRAY_HPP