#ifndef ID_GENERATOR_HPP
#define ID_GENERATOR_HPP

#include <VoxE/Core/Array.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class IDGenerator
	/// \tparam T
	/// This class generates unique IDs and provides ways to 
	/// acquire as well as release them. Its template parameter T
	/// may be any unsigned integral type just as vxU32, or vxU64.
	//////////////////////////////////////////////////////////////
	template<typename T>
	class IDGenerator
	{
	public:

		static const T kInvalidID = -1;

		IDGenerator() :
			mCurID(T(0)),
			mFreeIDs()
		{
		}
		
		//////////////////////////////////////////////////////////////
		/// Returns the next free ID or kInvalidID, if no valid ID
		/// could be fetched.
		//////////////////////////////////////////////////////////////
		T NextID()
		{
			if (mFreeIDs.Size() > 0)
			{
				T id = mFreeIDs.Back();
				mFreeIDs.Pop();
				return id;
			}

			if (mCurID < kInvalidID)
				return mCurID++;
			return kInvalidID;
		}
		//////////////////////////////////////////////////////////////
		/// Releases the ownership of the given ID and prepares it for
		/// being returned again.
		//////////////////////////////////////////////////////////////
		void ReleaseID(const T& id)
		{
			mFreeIDs.Push(id);
		}

	private:

		T mCurID;
		Array<T> mFreeIDs;

	};
}

#endif // ID_GENERATOR_HPP
