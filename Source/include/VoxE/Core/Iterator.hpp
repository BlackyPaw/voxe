#ifndef ITERATOR_HPP
#define ITERATOR_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Iterator
	/// \param T The type of the data the iterator operates on.
	/// Base class for creating custom iterators used in the
	/// containers.
	//////////////////////////////////////////////////////////////
	template<typename T>
	class Iterator
	{
	public:
	};
}

#endif // ITERATOR_HPP