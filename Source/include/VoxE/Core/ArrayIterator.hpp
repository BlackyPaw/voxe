#ifndef ARRAY_ITERATOR_HPP
#define ARRAY_ITERATOR_HPP

#include <VoxE/Core/Iterator.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class ArrayIterator
	/// \param T The type of the data the iterator operates on.
	/// Iterator for use with continguous memory blocks as they
	/// occur in containers like arrays.
	//////////////////////////////////////////////////////////////
	template<typename T>
	class ArrayIterator : public Iterator<T>
	{
	public:

		ArrayIterator(T* p) :
			mPtr(p)
		{
		}

		ArrayIterator(const ArrayIterator<T>& it) :
			mPtr(it.mPtr)
		{
		}

		ArrayIterator& operator++()
		{
			mPtr++;
			return *this;
		}

		ArrayIterator  operator++(int)
		{
			ArrayIterator tmp(*this);
			mPtr++;
			return tmp;
		}

		ArrayIterator& operator+=(const vxU32 n)
		{
			mPtr += n;
			return *this;
		}

		ArrayIterator& operator-=(const vxU32 n)
		{
			mPtr -= n;
			return *this;
		}

		vxBool operator== (const ArrayIterator& it) const
		{
			return (mPtr == it.mPtr);
		}

		vxBool operator!= (const ArrayIterator& it) const
		{
			return !((*this) == it);
		}

		T& operator* ()
		{
			return (*mPtr);
		}

		T const& operator* () const
		{
			return (*mPtr);
		}

		T* operator-> ()
		{
			return mPtr;
		}

		T const* operator-> () const
		{
			return mPtr;
		}

		ArrayIterator<T>& operator= (const ArrayIterator<T>& it)
		{
			mPtr = it.mPtr;
			return *this;
		}

	private:

		T* mPtr;
	};

	template<typename T>
	ArrayIterator<T> operator+ (ArrayIterator<T> const& it, const vxU32 n)
	{
		ArrayIterator<T> tmp(it);
		tmp += n;
		return tmp;
	}

	template<typename T>
	ArrayIterator<T> operator- (ArrayIterator<T> const& it, const vxU32 n)
	{
		ArrayIterator<T> tmp(it);
		tmp -= n;
		return tmp;
	}
}

#endif // ARRAY_ITERATOR_HPP