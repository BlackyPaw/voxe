#ifndef EVENT_MANAGER_HPP
#define EVENT_MANAGER_HPP

#include <VoxE/Core/Delegate.hpp>
#include <VoxE/Core/Event.hpp>
#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Native/Atomic.hpp>

#include <list>
#include <map>

namespace VX
{
	typedef Delegate1<EventPtr&> EventListener;
	static const vxU32 kInfiniteTimeout = 0xFFFFFFFF;

	typedef std::list<EventListener> EventListenerList;
	typedef std::map<EventID, EventListenerList> EventListenerMap;
	typedef std::list<EventPtr> EventQueue;

	//////////////////////////////////////////////////////////////
	/// \class EventManager
	/// This class manages events which are sent by all kinds of
	/// engine subsystems such as the HID interface or the Game
	/// Object Model and calls all registered listeners in order
	/// to inform every code that depends on a specific type of
	/// event that one occured.
	//////////////////////////////////////////////////////////////
	class EventManager : public ISingleton<EventManager>
	{
	public:

		EventManager();
		~EventManager();

		//////////////////////////////////////////////////////////////
		/// Flushes the event queue. If timeout is specified it will
		/// flush as many events as possible before the time rans out.
		/// \param timeout The timeout in milliseconds.
		//////////////////////////////////////////////////////////////
		void Flush(const vxU32 timeout = kInfiniteTimeout);

		//////////////////////////////////////////////////////////////
		/// Registers the given event listener to the given event �D.
		/// From this point the listener will receive all events which
		/// are of the type described by the given id. Furthermore a
		/// listener may be registered to multiple event ids in order
		/// to receive multiple types of events.
		/// \param id The ID of the event to register the listener to.
		/// \param l The listener to register.
		//////////////////////////////////////////////////////////////
		void RegisterListener(const EventID id, const EventListener& l);
		//////////////////////////////////////////////////////////////
		/// Unregisters the given event listener from the given event
		/// ID. This makes it possible to dynamically attach listeners
		/// to events and / or detach them from these.
		/// \param id The ID of the event to unregister the listener
		///		from.
		/// \param l The listener to unregister.
		//////////////////////////////////////////////////////////////
		void UnRegisterListener(const EventID id, const EventListener& l);

		//////////////////////////////////////////////////////////////
		/// Triggers the given event immediately, i.e. without queing
		/// it and waiting for a flush first but calling all registered
		/// listeners instantly. Please note that this call is blocking.
		/// \param e The event to trigger.
		//////////////////////////////////////////////////////////////
		void TriggerEvent(EventPtr e);
		//////////////////////////////////////////////////////////////
		/// Queues the given event and stores it into a list. It will
		/// be triggered during a Flush() call in the future.
		/// \param e The event to queue.
		//////////////////////////////////////////////////////////////
		void QueueEvent(EventPtr e);
		//////////////////////////////////////////////////////////////
		/// Aborts the given event and removes it from the list o
		/// events. Therefore it will not be triggered by any Flush()
		/// call in the future.
		//////////////////////////////////////////////////////////////
		void AbortEvent(EventPtr e);

	private:

		EventManager(const EventManager& em);
		EventManager& operator= (const EventManager& em);

		//////////////////////////////////////////////////////////////
		/// The number of queues the event manager uses. Used for
		/// double-buffering queues.
		//////////////////////////////////////////////////////////////
		static const vxU32 kNumQueues = 2;

		EventQueue mQueues[kNumQueues];
		EventListenerMap mListeners;
		vxU32 mActiveQueue;
	};
}

#endif // EVENT_MANAGER_HPP
