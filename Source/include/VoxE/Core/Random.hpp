#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Random
	/// This class provides an implementation of a pseudo random
	/// number generator. Internally it uses the Mersenne Twister
	/// which operates on a huge amount of data (~ 2,5 KiB) so
	/// it is not recommended to instantiate the whole generator
	/// just for generating one single random number. Instead
	/// one should first initialize one global random number
	/// generator and use it whereever possible.
	//////////////////////////////////////////////////////////////
	class Random
	{
	public:

		Random();
		~Random();

		//////////////////////////////////////////////////////////////
		/// Sets the seed of the random number generator and initializes
		/// it. The seed consists of 624 pseudo-random numbers which
		/// are used to generate the following ones.
		/// \param seed A pointer to an array consisting of 624 numbers.
		//////////////////////////////////////////////////////////////
		void Seed(const vxU32* seed);
		//////////////////////////////////////////////////////////////
		/// Calculates a random seed using C++'s std::rand(). Please
		/// make sure std::srand() is called before using this method.
		/// It generates 624 random numbers which are then used as the
		/// algorithm's seed.
		//////////////////////////////////////////////////////////////
		void RandomSeed();

		//////////////////////////////////////////////////////////////
		/// Calculates the next 32-bit pseudo-random number.
		//////////////////////////////////////////////////////////////
		vxU32 Next() const;
		//////////////////////////////////////////////////////////////
		/// Calculates the next 64-bit pseudo-random number.
		//////////////////////////////////////////////////////////////
		vxU64 NextU64() const;
		//////////////////////////////////////////////////////////////
		/// Calculates the next 32-bit pseudo-random floating-point
		/// number. It always lies in the range between 0.0 and 1.0.
		//////////////////////////////////////////////////////////////
		vxF32 NextF32() const;
		//////////////////////////////////////////////////////////////
		/// Calculates the next 32-bit pseudo-random floating-point
		/// number. It always lies in the range between 0.0 and 1.0.
		//////////////////////////////////////////////////////////////
		vxF64 NextF64() const;

	private:

		mutable vxU32 mWords[624];
		mutable vxU32 mIndex;
	};
}

#endif //RANDOM_HPP