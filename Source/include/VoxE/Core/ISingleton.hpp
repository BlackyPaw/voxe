#ifndef ISINGLETON_HPP
#define ISINGLETON_HPP

namespace VX
{
    //////////////////////////////////////////////////////////////
    /// \class ISingleton
    /// \tparam T The type to provide as a singleton.
    /// This class provides a convenient way for declaring singleton
    /// classes.
    //////////////////////////////////////////////////////////////
    template<typename T>
	class ISingleton
	{
	public:

		static T* GetSharedInstance()
		{
			static T kSharedInstance;
			return &kSharedInstance;
		}
	};
}

#endif // ISINGLETON_HPP