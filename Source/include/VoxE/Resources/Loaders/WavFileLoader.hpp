#ifndef WAV_FILE_LOADER_HPP
#define WAV_FILE_LOADER_HPP

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	class WavFileLoader : public IResourceLoader
	{
	public:

		virtual vxString GetWildcard() const;
		virtual vxBool LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache);
	};
}

#endif // WAV_FILE_LOADER_HPP