#ifndef STRING_LOADER_HPP
#define STRING_LOADER_HPP

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class StringLoader
	/// Loads a file's content into a vxString instance.
	//////////////////////////////////////////////////////////////
	class StringLoader : public IResourceLoader
	{
	public:

		virtual vxString GetWildcard() const;
		virtual vxBool LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache);
	};
}

#endif // STRING_LOADER_HPP