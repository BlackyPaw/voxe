#ifndef TEXTURE_LOADER_HPP
#define TEXTURE_LOADER_HPP

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class TextureLoader
	/// This loader class is responsible for loading textures into
	/// memory. It supports all kinds of different file formats
	/// like PNG, JPG, BMP, TGA, and DDS.
	//////////////////////////////////////////////////////////////
	class TextureLoader : public IResourceLoader
	{
	public:

		virtual vxString GetWildcard() const;
		virtual vxBool LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache);
	};
}

#endif // TEXTURE_LOADER_HPP