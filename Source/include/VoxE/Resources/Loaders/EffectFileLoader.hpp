#ifndef EFFECT_FILE_LOADER_HPP
#define EFFECT_FILE_LOADER_HPP

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class EffectFileLoader
	/// This loader can load effect files which are actually parsed
	/// by the effect while parser. It then creates a shader
	/// program and registers all semantics declared in the effect
	/// file and finally returns a ShaderProgram.
	/// \see ShaderProgram
	//////////////////////////////////////////////////////////////
	class EffectFileLoader : public IResourceLoader
	{
	public:

		virtual vxString GetWildcard() const;
		virtual vxBool LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache);
	};
}

#endif // EFFECT_FILE_LOADER_HPP