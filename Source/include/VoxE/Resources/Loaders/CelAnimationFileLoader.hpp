#ifndef ANIMATION_FILE_LOADER_HPP
#define ANIMATION_FILE_LOADER_HPP

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	class CelAnimationFileLoader : public IResourceLoader
	{
	public:

		virtual vxString GetWildcard() const;
		virtual vxBool LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache);
	};
}

#endif // ANIMTION_FILE_LOADER_HPP