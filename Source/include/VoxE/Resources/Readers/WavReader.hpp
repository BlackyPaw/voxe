#ifndef WAV_READER_HPP
#define WAV_READER_HPP

#include <VoxE/Native/FileStream.hpp>
#include <VoxE/Audio/SoundBuffer.hpp>

namespace VX
{

	class WavReader
	{
	public:

		WavReader();
		~WavReader();

		vxBool ReadFile(InputStreamPtr s, SoundBuffer** pBuffer);

	private:
	};
}

#endif // WAV_READER_HPP