#ifndef RESOURCE_HANDLE_HPP
#define RESOURCE_HANDLE_HPP

#include <VoxE/Resources/ResourceKey.hpp>

namespace VX
{
	class ResourceCache;

	//////////////////////////////////////////////////////////////
	/// \class ResourceHandle
	/// Simple handle class used to access resources 
	//////////////////////////////////////////////////////////////
	class ResourceHandle
	{
	public:

		ResourceHandle();
		ResourceHandle(const ResourceKey& key, ResourceCache* pResCache);
		ResourceHandle(const ResourceHandle& h);
		~ResourceHandle();

		vxU32 Size();
		template<typename T>
		T* Get()
		{
			return reinterpret_cast<T*>(Dereference());
		}

		operator vxBool() const;
		vxBool operator! () const;
		vxBool IsValid() const;
		const ResourceKey& GetKey() const;

		ResourceHandle& operator= (const ResourceHandle& h);

		vxBool operator==(const ResourceHandle& h) const;
		vxBool operator!=(const ResourceHandle& h) const;

	private:

		void* Dereference();
		void Copy(const ResourceHandle& h);

		ResourceKey mKey;
		ResourceCache* mResCache;
	};
}

#endif // RESOURCE_HANDLE_HPP
