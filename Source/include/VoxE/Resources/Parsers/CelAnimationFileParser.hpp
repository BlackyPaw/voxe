#ifndef ANIMATION_FILE_PARSER_HPP
#define ANIMATION_FILE_PARSER_HPP

#include <VoxE/Animation/CelAnimation.hpp>

namespace VX
{
	class CelAnimationFileParser
	{
	public:

		CelAnimationFileParser();

		vxBool LoadAnimationFile(const vxU8* buffer, const vxU32 bufferLen, CelAnimation** pAnimation);
	};
}

#endif // ANIMATION_FILE_PARSER_HPP