#ifndef EFFECT_FILE_PARSER_HPP
#define EFFECT_FILE_PARSER_HPP

#include <VoxE/Graphics/ShaderProgram.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class EffectFileParser
	/// This class allows for parsing any valid effect file. It
	/// creates a ShaderProgram from the effect file and registers
	/// all semantics.
	//////////////////////////////////////////////////////////////
	class EffectFileParser
	{
	public:

		EffectFileParser();

		//////////////////////////////////////////////////////////////
		/// Loads an effect file from an in-memory buffer.
		/// \param buffer The buffer which contains the effect file's
		///		contents.
		/// \param bufferLen The number of bytes the buffer contains.
		/// \param[out] pProgram The pointer to where the program will
		///		be put to.
		//////////////////////////////////////////////////////////////
		vxBool LoadEffectFile(const vxU8* buffer, const vxU32 bufferLen, ShaderProgram** pProgram);

	private:

		void EatWhitespace();
		vxString GetToken();
		vxBool ParseSemantic();
		vxBool ParseMapping();
		vxBool ParseShaderBlock();
		vxBool ReturnFailure();
		void PrintDebugMsg();
		void SafeDestroy();

		const vxChar* mBuffer;
		vxU32 mBufferLen;
		ShaderProgram* mProgram;
		vxU32 mIdx;
		vxString mToken;
		vxU32 mLine;
		Array<std::pair<ShaderSemantic, vxString>> mSemantics;
		Array<ShaderMapping> mMappings;
	};
}

#endif // EFFECT_FIlE_PARSER_HPP
