#ifndef RESOURCE_KEY_HPP
#define RESOURCE_KEY_HPP

#include <VoxE/Core/String.hpp>

namespace VX
{
	typedef vxU64 ResourceID;
	static const ResourceID kInvalidResourceID = 0xFFFFFFFFFFFFFFFF;

	//////////////////////////////////////////////////////////////
	/// \class ResourceKey
	/// Simple class for keying resources by either hashing their
	/// name or providing an instance number.
	//////////////////////////////////////////////////////////////
	class ResourceKey
	{
	public:

		ResourceKey();
		ResourceKey(const vxString& name);

		void FromName(const vxString& name);

		const vxString& GetName() const;
		const ResourceID ToUniqueID() const;

		vxBool operator<  (const ResourceKey& key) const;
		vxBool operator== (const ResourceKey& key) const;
		vxBool operator!= (const ResourceKey& key) const;

	private:

		vxString mName;
		ResourceID mID;
	};
}

#endif // RESOURCE_KEY_HPP