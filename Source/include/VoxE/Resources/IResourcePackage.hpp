#ifndef IRESOURCE_PACKAGE_HPP
#define IRESOURCE_PACKAGE_HPP

#include <VoxE/Core/String.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Native/Stream.hpp>
#include <VoxE/Resources/ResourceKey.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class IResourcePackage
	/// This class allows for easily creating an interface for use
	/// with packing and loading resources stored into different
	/// archive types by providing a common interface.
	//////////////////////////////////////////////////////////////
	class IResourcePackage
	{
	public:

		IResourcePackage() {}
		virtual ~IResourcePackage() {}

		//////////////////////////////////////////////////////////////
		/// Loads the resource package from the given file.
		/// \param file The file to load.
		/// \returns True on success; False on failure.
		//////////////////////////////////////////////////////////////
		virtual vxBool LoadFile(const vxString& file) = 0;
		//////////////////////////////////////////////////////////////
		/// Opens a stream for reading the file contents of the
		/// resource matching the given resource key.
		/// \param key The key of the resource to open a stream to.
		/// \param[out] pSize This pointer will hold the number of
		///		bytes available from the input stream after the
		///		function returned.
		/// \returns An input stream to the resource's file.
		//////////////////////////////////////////////////////////////
		virtual InputStreamPtr GetStream(const ResourceKey& key, vxU32* pSize) = 0;
	};
}

#endif // IRESOURCE_PACKAGE_HPP