#ifndef IRESOURCE_LOADER_HPP
#define IRESOURCE_LOADER_HPP

#include <VoxE/Core/String.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Resources/ResourceKey.hpp>

namespace VX
{
	class ResourceHandle;
	class ResourceCache;

	//////////////////////////////////////////////////////////////
	/// \class IResourceLoader
	/// This class is an interface which allows for creating
	/// custom resource loaders in order to perform special tasks
	/// such as decompression or post-processing of the raw
	/// resource data.
	//////////////////////////////////////////////////////////////
	class IResourceLoader
	{
	public:

		virtual ~IResourceLoader() {}
		//////////////////////////////////////////////////////////////
		/// Gets a wildcard pattern that allows for the resource cache
		/// to check if a resource's file is meant to be loaded by
		/// this resource loader. Every pattern has to follow
		/// exactly this form: "*.ext", where ext May be any valid
		/// file extension such as png for example. Even multiple
		/// extensions may be supported by using | like this:
		/// "*.png|*.jpg|*.bmp".
		//////////////////////////////////////////////////////////////
		virtual vxString GetWildcard() const = 0;
		//////////////////////////////////////////////////////////////
		/// Loads the resource from the given resource key.
		/// \param key The resource key of the resource to load.
		/// \param[out] pData This pointer will point to the actually
		///		allocated resource data after the function returned.
		/// \param[out] pSize This pointer will hold the size of the
		///		allocated resource in bytes after the function returned.
		/// \param pResCache The resource cache to allocate memory from.
		/// \returns True on success, i.e. if the resource could be loaded
		///		successfully; False otherwise.
		//////////////////////////////////////////////////////////////
		virtual vxBool LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache) = 0;
	};
}

#endif // IRESOURCE_LOADER_HPP