#ifndef SINGLE_FILE_PACKAGE_HPP
#define SINGLE_FILE_PACKAGE_HPP

#include <VoxE/Resources/IResourcePackage.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class SingleFilePackage
	/// This class implements the IResourcePackage interface and
	/// allows for loading resources from their very own files
	/// without being packed into one large archive. Instead it
	/// opens simple file streams to all loose files.
	//////////////////////////////////////////////////////////////
	class SingleFilePackage : public IResourcePackage
	{
	public:

		SingleFilePackage();
		~SingleFilePackage();

		virtual vxBool LoadFile(const vxString& file) { return true; }
		virtual InputStreamPtr GetStream(const ResourceKey& key, vxU32* pSize);
	};
}

#endif // SINGLE_FILE_PACKAGE_HPP