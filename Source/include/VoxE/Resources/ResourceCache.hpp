#ifndef RESOURCE_CACHE_HPP
#define RESOURCE_CACHE_HPP

#include <VoxE/Core/IDGenerator.hpp>
#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Resources/IResourceLoader.hpp>
#include <VoxE/Resources/IResourcePackage.hpp>
#include <VoxE/Resources/ResourceHandle.hpp>

#include <list>
#include <map>

namespace VX
{
	static const vxU32 kInfiniteCacheSize = 0xFFFFFFFF;

	struct ResCacheData
	{
		void* mDataPtr;
		vxU32 mSize;
	};

	//////////////////////////////////////////////////////////////
	/// \class ResourceCache
	/// Simple cache class that allows for caching resources as
	/// well as gathering debugginginformation such as the amount
	/// of allocated memory and so on.
	//////////////////////////////////////////////////////////////
	class ResourceCache : public ISingleton<ResourceCache>
	{
		friend class ResourceHandle;
	public:
		//////////////////////////////////////////////////////////////
		/// Creates a resource cache with the given size in bytes.
		/// \param size The size of the cache in bytes.
		//////////////////////////////////////////////////////////////
		ResourceCache(const vxU32 size = kInfiniteCacheSize);
		~ResourceCache();

		//////////////////////////////////////////////////////////////
		/// Initializes the cache by registering all pre-defined
		/// resource-loaders.
		//////////////////////////////////////////////////////////////
		vxBool Initialize();
		//////////////////////////////////////////////////////////////
		/// Registers the given resource loader.
		/// \param pLoader The loader to register.
		//////////////////////////////////////////////////////////////
		void RegisterLoader(IResourceLoader* pLoader);

		//////////////////////////////////////////////////////////////
		/// Gets a handle to the specified resource or loads it into
		/// the cache if a cache miss occurs.
		/// \param key The (resource) key of the resource to load.
		/// \returns A handle to the resource.
		//////////////////////////////////////////////////////////////
		ResourceHandle GetHandle(const ResourceKey& key);

		//////////////////////////////////////////////////////////////
		/// Allocates the given number of bytes from the cache's
		/// very own memory allocator. This is used by all resource
		/// loaders for example.
		/// \param size The number of bytes to allocate.
		//////////////////////////////////////////////////////////////
		void* Allocate(const vxU32 size);
		//////////////////////////////////////////////////////////////
		/// Allocates the given number of instances as an array of
		/// the given type.
		/// \tparam T The type to allocate as an array.
		/// \param e The number of elements in the array. May be obmitted
		///		or set to 1 to allocate one single instance.
		//////////////////////////////////////////////////////////////
		template<typename T>
		T* Allocate(const vxU32 e = 1)
		{
			T* buffer = reinterpret_cast<T*>(Allocate(sizeof(T) * e));
			for (vxU32 i = 0; i < e; ++i)
				new (&buffer[i]) T();
			return buffer;
		}
		//////////////////////////////////////////////////////////////
		/// Frees the memory the given pointer is pointing to. In order
		/// to ensure that the cache will always be able to keep track
		/// of the amount of memory allocated the size of the memory
		/// block in bytes has to be given, too.
		/// \param p The pointer which points to the memory block to free.
		/// \param size The size of the memory block to free in bytes.
		//////////////////////////////////////////////////////////////
		void Free(void* p, const vxU32 size);
		//////////////////////////////////////////////////////////////
		/// Gets the resource package used by the resource cache to
		/// access a resource's file data.
		//////////////////////////////////////////////////////////////
		IResourcePackage* GetResourcePackage();

	private:

		//////////////////////////////////////////////////////////////
		// Methods
		//////////////////////////////////////////////////////////////
		ResourceCache(const ResourceCache& cache);
		ResourceCache& operator= (const ResourceCache& cache);

		ResCacheData* LoadResource(const ResourceKey& key);
		ResCacheData* GetCacheData(const ResourceKey& key);
		void Promote(const ResourceKey& key);
		vxBool MakeRoom(const vxU32 size);
		void FreeOneResource();
		void Destroy();

		//////////////////////////////////////////////////////////////
		// Members
		//////////////////////////////////////////////////////////////
		const vxU32 mCacheSize;
		vxU32 mAllocated;

		std::list<IResourceLoader*> mLoaders;
		std::list<ResourceKey> mLRU;
		std::map<ResourceKey, ResCacheData> mCacheData;

		IResourcePackage* mResourcePackage;
	};
}

#endif // RESOURCE_CACHE_HPP
