/*
 * PoolAllocator.cpp
 *
 *  Created on: 31.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Memory/PoolAllocator.hpp>

#include <cstdint>

namespace VX
{
	PoolAllocator::PoolAllocator() :
		mMemory(nullptr),
		mBlocks(nullptr),
		mSize(0),
		mCount(0)
	{
	}

	PoolAllocator::~PoolAllocator()
	{
		// Free memory occupied by reserved space:
		if(mMemory != nullptr)
		{
			delete[] mMemory;
		}
		// Free memory occupied by internal data:
		if(mBlocks != nullptr)
		{
			delete[] mBlocks;
		}
	}

	vxBool PoolAllocator::Create(const vxU32 size, const vxU32 count)
	{
		if(mMemory != nullptr)
			return false;

		vxU8* memory = nullptr;
		vxBool* blocks = nullptr;
		try
		{
			memory = new vxU8[size * count];
			blocks = new vxBool[count];
		}
		catch(...)
		{
			return false;
		}

		mMemory = memory;
		mBlocks = blocks;
		mSize = size;
		mCount = count;

		return true;
	}

	void* PoolAllocator::Allocate()
	{
		vxU32 block = 0xFFFFFFFF;
		for(vxU32 i = 0; i < mCount; ++i)
		{
			if(!mBlocks[i])
			{
				block = i;
				mBlocks[i] = true;
				break;
			}
		}

		if(block == 0xFFFFFFFF)
			return nullptr;

		return &mMemory[block * mSize];
	}

	void PoolAllocator::Free(void* p)
	{
		// Calculate the block number:
		vxU32 block = static_cast<vxU32>(reinterpret_cast<vxU64>(p) / static_cast<vxU64>(mSize));
		if(block > mCount)
			return;

		mBlocks[block] = false;
	}
}
