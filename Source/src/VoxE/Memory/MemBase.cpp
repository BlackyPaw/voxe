#include <VoxE/Memory/MemBase.hpp>
#include <cstdlib>

#if defined(VOXE_DEBUG) && defined(VOXE_PROFILE_MEMORY_USAGE)

VX::vxU64 kAllocMem = 0;

VX::vxU64 GetAllocMem()
{
	return kAllocMem;
}

void* operator new (std::size_t size) throw(std::bad_alloc)
{
	size += sizeof(VX::vxU32);
	void* p = malloc(size);
	(*reinterpret_cast<VX::vxU32*>(p)) = size;
	kAllocMem += size;
	return (reinterpret_cast<VX::vxU32*>(p) + 1);
}

void operator delete (void* p) throw()
{
	if (p != nullptr)
	{
		VX::vxU32* s = reinterpret_cast<VX::vxU32*>(p)-1;
		VX::vxU32 size = (*s);
		kAllocMem -= size;
		free(s);
	}
}

void* operator new[] (std::size_t size) throw(std::bad_alloc)
{
	size += sizeof(VX::vxU32);
	void* p = malloc(size);
	(*reinterpret_cast<VX::vxU32*>(p)) = size;
	kAllocMem += size;
	return (reinterpret_cast<VX::vxU32*>(p)+1);
}

void operator delete[] (void* p) throw()
{
	if (p != nullptr)
	{
		VX::vxU32* s = reinterpret_cast<VX::vxU32*>(p)-1;
		VX::vxU32 size = (*s);
		kAllocMem -= size;
		free(s);
	}
}

#else

VX::vxU64 GetAllocMem()
{
	return -1;
}

void* operator new (std::size_t size) throw(std::bad_alloc)
{
	void* p = malloc(size);
	return p;
}

void operator delete (void* p) throw()
{
	free(p);
}

void* operator new[](std::size_t size) throw(std::bad_alloc)
{
	void* p = malloc(size);
	return p;
}

void operator delete[](void* p) throw()
{
	free(p);
}

#endif
