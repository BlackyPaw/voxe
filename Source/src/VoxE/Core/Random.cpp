#include <VoxE/Core/Random.hpp>
#include <cstring>
#include <random>

namespace VX
{
	Random::Random() :
		mIndex(625)
	{
	}

	Random::~Random()
	{
	}

	void Random::Seed(const vxU32* seed)
	{
		memcpy(mWords, seed, 624 * sizeof(vxU32));
		mIndex = 625;
	}

	void Random::RandomSeed()
	{
		vxU32 seed[624];
		for (vxU32 i = 0; i < 624; ++i)
			seed[i] = static_cast<vxU32>(std::rand());
		Seed(seed);
	}

	vxU32 Random::Next() const
	{
		static const vxU32 kHI = 0x80000000;
		static const vxU32 kLO = 0x7FFFFFFF;
		static const vxU32 kA[2] = { 0x0, 0x9908B0DF };

		// Calculate new words if necessary:
		if (mIndex >= 624)
		{
			vxU32 i;
			vxU32 h;

			for (i = 0; i < 227; ++i)
			{
				h = (mWords[i] & kHI) | (mWords[i + 1] & kLO);
				mWords[i] = mWords[i + 397] ^ (h >> 1) ^ kA[h & 1];
			}
			for ( ; i < 623; ++i)
			{
				h = (mWords[i] & kHI) | (mWords[i + 1] & kLO);
				mWords[i] = mWords[i + (397 - 624)] ^ (h >> 1) ^ kA[h & 1];
			}

			h = (mWords[623] & kHI) | (mWords[0] & kLO);
			mWords[623] = mWords[396] ^ (h >> 1) ^ kA[h & 1];
			mIndex = 0;
		}

		vxU32 res = mWords[mIndex++];
		res ^= (res >> 11);
		res ^= (res <<  7) ^ 0x9D2C5680;
		res ^= (res << 15) ^ 0xEFC60000;
		res ^= (res >> 18);

		return res;
	}

	vxU64 Random::NextU64() const
	{
		vxU32 low  = Next();
		vxU32 high = Next();
		vxU64 res = low | (static_cast<vxU64>(high) << 32);
		return res;
	}

	vxF32 Random::NextF32() const
	{
		union u32tof32 { vxF32 f; vxU32 i; };
		u32tof32 u;
		u.i = Next() & (0x007FFFFF | 0x40000000);
		return (u.f * 0.5f - 1.0f);
	}

	vxF64 Random::NextF64() const
	{
		union u64tof64 { vxF64 d; vxU64 i; };
		u64tof64 u;
		u.i = NextU64() & (0x000FFFFFFFFFFFFF | 0x4000000000000000);
		return (u.d * 0.5f - 1.0f);
	}
}
