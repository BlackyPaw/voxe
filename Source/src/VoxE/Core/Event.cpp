#include <VoxE/Core/Event.hpp>

namespace VX
{
	Event::Event(const vxU32 id) :
		mID(id)
	{
	}

	Event::~Event()
	{
	}

	const EventID Event::GetID() const
	{
		return mID;
	}
}