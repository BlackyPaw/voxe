#include <VoxE/Core/EventManager.hpp>
#include <VoxE/Native/Time.hpp>

namespace VX
{
	EventManager::EventManager() :
		mActiveQueue(0)
	{
	}

	EventManager::~EventManager()
	{
	}

	void EventManager::Flush(const vxU32 timeout)
	{
		// Do a round-robin queue swap:
		vxU32 cur = mActiveQueue;
		mActiveQueue++;
		mActiveQueue = mActiveQueue % kNumQueues;
		EventQueue& queue = mQueues[cur];

		Time begin = Time::Now();

		for (auto it = queue.begin(); it != queue.end() && (timeout == kInfiniteTimeout || (Time::Now() - begin).Milliseconds() < timeout); ++it)
		{
			// Now trigger the event:
			TriggerEvent(*it);
		}

		// Copy all unprocessed events from the old queue into the new one:
		queue.clear();
		mQueues[mActiveQueue].swap(queue);
	}

	void EventManager::RegisterListener(const EventID id, const EventListener& l)
	{
		EventListenerList& list = mListeners[id];
		for (auto it = list.begin(); it != list.end(); ++it)
		{
			if (*it == l)
				return;
		}
		list.push_back(l);
	}

	void EventManager::UnRegisterListener(const EventID id, const EventListener& l)
	{
		auto find = mListeners.find(id);
		if (find == mListeners.end())
			return;

		EventListenerList& list = find->second;
		for (auto it = list.begin(); it != list.end(); ++it)
		{
			if (*it == l)
			{
				list.erase(it);
				return;
			}
		}
	}

	void EventManager::TriggerEvent(EventPtr e)
	{
		auto find = mListeners.find(e->GetID());
		if (find == mListeners.end())
			return;

		EventListenerList& list = find->second;
		for (auto it = list.begin(); it != list.end(); ++it)
		{
			(*it).Invoke(e);
		}
	}

	void EventManager::QueueEvent(EventPtr e)
	{
		mQueues[mActiveQueue].push_back(e);
	}

	void EventManager::AbortEvent(EventPtr e)
	{
		for (auto it = mQueues[mActiveQueue].begin(); it != mQueues[mActiveQueue].end(); ++it)
		{
			if (*it == e)
			{
				mQueues[mActiveQueue].erase(it);
				return;
			}
		}
	}
}
