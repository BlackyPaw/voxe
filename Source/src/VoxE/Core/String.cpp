#include <VoxE/Core/String.hpp>

namespace VX
{
	String::String() :
		mSize(0),
		mLength(0)
	{
	}

	String::~String()
	{
	}

	void String::Append(const String& str)
	{
		for (vxU32 i = 0; i < str.Size(); ++i)
		{
			mCharacters.Push(str[i]);
			mSize++;
		}
		mLength += str.Length();
	}

	void String::Append(const vxChar c)
	{
		mCharacters.Push(c);
		mSize++;
		mLength++;
	}

	void String::Append(vxCCharPtr c)
	{
		for (vxU32 i = 0; c[i] != '\0'; ++i)
		{
			mCharacters.Push(c[i]);
			mSize++;
			mLength++;
		}
	}

	String String::Substring(const vxU32 beg, const vxU32 len)
	{
		vxU32 size = len; // <- Calculate actual size of UTF-8 codepoint HERE (!)
		Array<vxChar> a;
		a.Resize(size);
		memcpy(&a[0], &mCharacters[beg], len * sizeof(vxChar));

		String str;
		str.mCharacters = a;
		str.mSize = len;

		return str;
	}

	vxChar& String::operator[] (const vxU32 n)
	{
		return mCharacters[n];
	}

	vxChar const& String::operator[] (const vxU32 n) const
	{
		return mCharacters[n];
	}

	vxU32 String::Size() const
	{
		return mSize;
	}

	vxU32 String::Length() const
	{
		return mLength;
	}

	vxCCharPtr String::ToANSI() const
	{
		return &mCharacters[0];
	}

	vxBool String::operator== (const String& str) const
	{
		if (mSize != str.mSize || mLength != str.mLength)
			return false;
		return (memcmp(&mCharacters[0], &str.mCharacters[0], mSize * sizeof(vxChar)) == 0);
	}

	vxBool String::operator!= (const String& str) const
	{
		return !((*this) == str);
	}

	vxBool String::operator<  (const String& str) const
	{
		if (mSize < str.mSize || mLength < str.mLength)
			return true;
		return (memcmp(&mCharacters[0], &str.mCharacters[0], mSize * sizeof(vxChar)) < 0);
	}

	vxBool String::operator<= (const String& str) const
	{
		return ((*this) == str || (*this) < str);
	}

	vxBool String::operator>  (const String& str) const
	{
		if (mSize > str.mSize || mLength > str.mLength)
			return true;
		return (memcmp(&mCharacters[0], &str.mCharacters[0], mSize * sizeof(vxChar)) > 0);
	}

	vxBool String::operator>= (const String& str) const
	{
		return ((*this) == str || (*this) > str);
	}
}