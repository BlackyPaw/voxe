#include <VoxE/Math/Transformable.hpp>

namespace VX
{
	Transformable::Transformable() :
		mPosition(0.0f, 0.0f, 0.0f),
		mScale(1.0f, 1.0f, 1.0f),
		mTransform(1.0f),
		mUpdate(false)
	{
	}

	void Transformable::Translate(const float x, const float y, const float z)
	{
		Translate(Vector3f(x, y, z));
	}
	void Transformable::Translate(const Vector3f& trans)
	{
		mPosition += trans;
		mUpdate = true;
	}
	void Transformable::Rotate(const float pitch, const float yaw, const float roll)
	{
		Rotate(Vector3f(pitch, yaw, roll));
	}
	void Transformable::Rotate(const Vector3f& rot)
	{
		mRotation = mRotation * Quaternion(Vector3f(Radians(rot.x), Radians(rot.y), Radians(rot.z)));
		mUpdate = true;
	}
	void Transformable::Scale(const float x, const float y, const float z)
	{
		Scale(Vector3f(x, y, z));
	}
	void Transformable::Scale(const Vector3f& scale)
	{
		mScale *= scale;
		mUpdate = true;
	}

	void Transformable::SetPosition(const float x, const float y, const float z)
	{
		SetPosition(Vector3f(x, y, z));
	}
	void Transformable::SetPosition(const Vector3f& pos)
	{
		mPosition = pos;
		mUpdate = true;
	}
	void Transformable::SetRotation(const float pitch, const float yaw, const float roll)
	{
		SetRotation(Vector3f(pitch, yaw, roll));
	}
	void Transformable::SetRotation(const Vector3f& rot)
	{
		mRotation = Quaternion(rot);
		mUpdate = true;
	}
	void Transformable::SetRotation(const Quaternion& rot)
	{
		mRotation = rot;
		mUpdate = true;
	}
	void Transformable::SetScale(const float x, const float y, const float z)
	{
		Scale(Vector3f(x, y, z));
	}
	void Transformable::SetScale(const Vector3f& scale)
	{
		mScale = scale;
		mUpdate = true;
	}

	const Vector3f& Transformable::GetPosition() const
	{
		return mPosition;
	}
	const Quaternion& Transformable::GetRotation() const
	{
		return mRotation;
	}
	const Vector3f Transformable::GetEulerAngles() const
	{
		return ToEulerAngles(mRotation);
	}
	const Vector3f& Transformable::GetScale() const
	{
		return mScale;
	}

	FloatRect Transformable::TransformRect(const FloatRect& rect) const
	{
		const Matrix4& m = GetTransform();
		Vector2f v[4] =
		{
			Vector2f(rect.mLeft, rect.mTop),
			Vector2f(rect.mLeft + rect.mWidth, rect.mTop),
			Vector2f(rect.mLeft + rect.mWidth, rect.mTop + rect.mHeight),
			Vector2f(rect.mLeft, rect.mTop + rect.mHeight)
		};

		for (vxU32 i = 0; i < 4; ++i)
		{
			Vector4f v_i = m * Vector4f(v[i].x, v[i].y, 0.0f, 1.0f);
			v[i] = Vector2f(v_i.x, v_i.y);
		}

		float r_min = v[0].x;
		float s_min = v[0].y;
		float r_max = v[0].x;
		float s_max = v[0].y;

		for (vxU32 i = 0; i < 4; ++i)
		{
			if (v[i].x < r_min) r_min = v[i].x;
			else if (v[i].x > r_max) r_max = v[i].x;
			if (v[i].y < s_min) s_min = v[i].y;
			else if (v[i].y > s_max) s_max = v[i].y;
		}

		return FloatRect(r_min, s_min, r_max - r_min, s_max - s_min);
	}

	const Matrix4& Transformable::GetTransform() const
	{
		if (mUpdate)
		{
			mTransform = VX::Translate(mPosition) * VX::ToMatrix4(mRotation) * VX::Scale(mScale);
			mUpdate = false;
		}
		return mTransform;
	}
}