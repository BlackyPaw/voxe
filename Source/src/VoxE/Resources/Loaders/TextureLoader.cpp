#include <VoxE/Graphics/Texture.hpp>
#include <VoxE/Resources/Loaders/TextureLoader.hpp>
#include <VoxE/Resources/ResourceCache.hpp>

#include <SOIL/SOIL.h>

namespace VX
{
	vxString TextureLoader::GetWildcard() const
	{
		return "*.png|*.jpg|*.tga|*.bmp|*.dds";
	}

	vxBool TextureLoader::LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache)
	{
		vxU32 size;
		InputStreamPtr stream = pResCache->GetResourcePackage()->GetStream(key, &size);
		if (stream == nullptr)
			return false;

		vxU8* file_buffer = new vxU8[size];
		if (stream->Read(size, file_buffer) == 0)
			return false;

		vxU32 width = 0, height = 0;
		vxU8* pixels = SOIL_load_image_from_memory(file_buffer, size, reinterpret_cast<int*>(&width), reinterpret_cast<int*>(&height), nullptr, SOIL_LOAD_RGBA);
		if (pixels == nullptr)
			return false;

		delete[] file_buffer;

		Texture* texture = pResCache->Allocate<Texture>();
		if (texture == nullptr)
			return false;
		texture->Create(width, height, pixels);

		SOIL_free_image_data(pixels);

		(*pData) = texture;
		(*pSize) = sizeof(Texture);

		return true;
	}
}