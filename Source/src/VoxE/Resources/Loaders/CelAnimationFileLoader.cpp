#include <VoxE/Resources/ResourceCache.hpp>
#include <VoxE/Resources/Loaders/CelAnimationFileLoader.hpp>
#include <VoxE/Resources/Parsers/CelAnimationFileParser.hpp>

namespace VX
{
	vxString CelAnimationFileLoader::GetWildcard() const
	{
		return "*.cel";
	}

	vxBool CelAnimationFileLoader::LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache)
	{
		vxU32 size = 0;
		InputStreamPtr stream = pResCache->GetResourcePackage()->GetStream(key, &size);
		if (stream == nullptr)
			return false;

		vxU8* buffer = new vxU8[size];
		vxU32 n = stream->Read(size, buffer);
		if (n == 0)
			return false;

		stream->Close();

		CelAnimation* pAnim = nullptr;
		CelAnimationFileParser parser;
		if (!parser.LoadAnimationFile(buffer, size, &pAnim))
			return false;

		delete[] buffer;

		CelAnimation* pResult = pResCache->Allocate<CelAnimation>();
		pResult = new (pResult) CelAnimation(*pAnim);
		delete pAnim;

		(*pData) = pResult;
		(*pSize) = sizeof(CelAnimation);

		return true;
	}
}