#include <VoxE/Resources/ResourceCache.hpp>
#include <VoxE/Resources/Loaders/EffectFileLoader.hpp>
#include <VoxE/Resources/Parsers/EffectFileParser.hpp>

namespace VX
{
	vxString EffectFileLoader::GetWildcard() const
	{
		return "*.efx|*.fx";
	}

	vxBool EffectFileLoader::LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache)
	{
		vxU32 size;
		InputStreamPtr stream = pResCache->GetResourcePackage()->GetStream(key, &size);
		if (stream == nullptr)
			return false;

		vxU8* buffer = new vxU8[size];
		vxU32 n = stream->Read(size, buffer);
		if (n == 0)
			return false;

		stream->Close();

		ShaderProgram* pProgram = nullptr;
		EffectFileParser parser;
		if (!parser.LoadEffectFile(buffer, size, &pProgram))
			return false;

		delete[] buffer;

		ShaderProgram* pResult = pResCache->Allocate<ShaderProgram>();
		memmove(pResult, pProgram, sizeof(ShaderProgram));

		(*pData) = pResult;
		(*pSize) = sizeof(ShaderProgram);

		return true;
	}
}