#include <VoxE/Resources/ResourceCache.hpp>
#include <VoxE/Resources/Loaders/StringLoader.hpp>

namespace VX
{
	vxString StringLoader::GetWildcard() const
	{
		return vxString("*.lim|*.str");
	}

	vxBool StringLoader::LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache)
	{
		vxU32 size;
		InputStreamPtr stream = pResCache->GetResourcePackage()->GetStream(key, &size);
		if (stream == nullptr)
			return false;

		vxChar* buffer = new vxChar[size];
		vxU32 n = stream->Read(size, buffer);
		if (n == 0)
			return false;

		stream->Close();

		void* p = pResCache->Allocate(sizeof(vxString));
		if (p == nullptr)
			return false;
		vxString* pString = new (p) vxString();
		pString->append(buffer);
		vxString substr = pString->substr(0, n);
		pString = new (p)vxString(substr);

 		(*pData) = pString;
		(*pSize) = sizeof(vxString);

		delete[] buffer;

		return true;
	}
}