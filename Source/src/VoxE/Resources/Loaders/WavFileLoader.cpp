#include <VoxE/Resources/ResourceCache.hpp>
#include <VoxE/Resources/Loaders/WavFileLoader.hpp>
#include <VoxE/Resources/Readers/WavReader.hpp>

namespace VX
{
	vxString WavFileLoader::GetWildcard() const
	{
		return "*.wav";
	}

	vxBool WavFileLoader::LoadResource(const ResourceKey& key, void** pData, vxU32* pSize, ResourceCache* pResCache)
	{
		vxU32 size;
		InputStreamPtr stream = pResCache->GetResourcePackage()->GetStream(key, &size);
		if (stream == nullptr)
			return false;

		SoundBuffer* pBuffer = nullptr;
		WavReader reader;
		if (!reader.ReadFile(stream, &pBuffer))
			return false;

		if (!pBuffer->IsValid())
			return false;

		SoundBuffer* pResult = pResCache->Allocate<SoundBuffer>();
		pResult = new (pResult)SoundBuffer(*pBuffer);
		delete pBuffer;

		(*pData) = pResult;
		(*pSize) = sizeof(SoundBuffer);

		return true;
	}
}