#include <VoxE/Resources/Readers/WavReader.hpp>

namespace VX
{
	enum WavChunk
	{
		WAV_CHUNK_RIFFHEADER = 0X46464952,
		WAV_CHUNK_FORMAT = 0X020746D66,
		WAV_CHUNK_DATA = 0X61746164,
	};

	enum WavFormat
	{
		WAV_FORMAT_PULSECODEMODULATION = 0X01,
		WAV_FORMAT_IEEEFLOATINGPOINT = 0X03,
		WAV_FORMAT_ALAW = 0X06,
		WAV_FORMAT_MULAW = 0X07,
		WAV_FORMAT_IMAADPCM = 0X11,
		WAV_FORMAT_YAMAHAITUG723ADPCM = 0X16,
		WAV_FORMAT_GSM610 = 0X31,
		WAV_FORMAT_ITUG721ADPCM = 0X40,
		WAV_FORMAT_MPEG = 0X50,
		WAV_FORMAT_EXTENSIBLE = 0XFFFE
	};

	WavReader::WavReader()
	{
	}

	WavReader::~WavReader()
	{
	}

	vxBool WavReader::ReadFile(InputStreamPtr s, SoundBuffer** pBuffer)
	{
		vxI32 chunkid;
		vxBool isData = false;

		vxU32 fullsize = 0;
		vxU32 riffformat = 0;

		vxU32 formatsize = 0;
		vxU16 format = 0;
		vxU16 channels = 0;
		vxU32 samplerate = 0;
		vxU32 bpp = 0;
		vxU16 blockalign = 0;
		vxU16 bitdepth = 0;

		vxU32 datasize = 0;

		while (!isData)
		{
			chunkid = s->ReadLE32();
			switch (chunkid)
			{
				case WAV_CHUNK_FORMAT:
					formatsize = s->ReadLE32();
					format = s->ReadLE16();
					channels = s->ReadLE16();
					samplerate = s->ReadLE32();
					bpp = s->ReadLE32();
					blockalign = s->ReadLE16();
					bitdepth = s->ReadLE16();
					if (formatsize == 18)
					{
						vxU16 extra = s->ReadLE16();
						s->Seek(STREAM_SEEK_CUR, extra);
					}
					break;
				case WAV_CHUNK_RIFFHEADER:
					fullsize = s->ReadLE32();
					riffformat = s->ReadLE32();
					break;
				case WAV_CHUNK_DATA:
					datasize = s->ReadLE32();
					isData = true;
					break;
				default:
					vxU32 n = s->ReadLE32();
					s->Seek(STREAM_SEEK_CUR, n);
					break;
			}
		}

		if (datasize == 0)
			return false;

		vxU8* data = new vxU8[datasize];
		s->Read(datasize, data);

		SoundBuffer* buffer = new SoundBuffer();
		buffer->Create(channels, bitdepth, data, datasize, samplerate);

		delete[] data;

		(*pBuffer) = buffer;

		return true;
	}
}