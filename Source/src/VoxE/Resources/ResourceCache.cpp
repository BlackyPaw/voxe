#include <VoxE/Resources/ResourceCache.hpp>
#include <VoxE/Resources/SingleFilePackage.hpp>

#include <VoxE/Resources/Loaders/CelAnimationFileLoader.hpp>
#include <VoxE/Resources/Loaders/EffectFileLoader.hpp>
#include <VoxE/Resources/Loaders/StringLoader.hpp>
#include <VoxE/Resources/Loaders/TextureLoader.hpp>
#include <VoxE/Resources/Loaders/WavFileLoader.hpp>

namespace VX
{
	ResourceCache::ResourceCache(const vxU32 size) :
		mCacheSize(size),
		mAllocated(0),
		mResourcePackage(nullptr)
	{
	}

	ResourceCache::~ResourceCache()
	{
		Destroy();
	}

	vxBool ResourceCache::Initialize()
	{
		mResourcePackage = new SingleFilePackage();
		// Register all resource loaders here:
		// -TODO:
		//	* Create some resource loaders and add them here.
		RegisterLoader(new CelAnimationFileLoader());
		RegisterLoader(new EffectFileLoader());
		RegisterLoader(new StringLoader());
		RegisterLoader(new TextureLoader());
		RegisterLoader(new WavFileLoader());

		return true;
	}

	void ResourceCache::RegisterLoader(IResourceLoader* pLoader)
	{
		mLoaders.push_front(pLoader);
	}

	ResourceHandle ResourceCache::GetHandle(const ResourceKey& key)
	{
		auto find = mCacheData.find(key);
		if (find == mCacheData.end())
			LoadResource(key);
		return ResourceHandle(key, this);
	}

	void* ResourceCache::Allocate(const vxU32 size)
	{
		if (!MakeRoom(size))
			return nullptr;

		// Use custom memory allocator here, once it is available:
		mAllocated += size;
		return (new vxU8[size]);
	}

	void ResourceCache::Free(void* p, const vxU32 size)
	{
		delete[] p;
		mAllocated -= size;
	}

	IResourcePackage* ResourceCache::GetResourcePackage()
	{
		return mResourcePackage;
	}

	ResCacheData* ResourceCache::LoadResource(const ResourceKey& key)
	{
		// First of all get the file extension of the resource:
		std::string file_ext = "";
		vxU32 last_dot = key.GetName().rfind('.');
		file_ext = key.GetName().substr(last_dot + 1);

		IResourceLoader* pLoader = nullptr;

		for (auto it = mLoaders.begin(); it != mLoaders.end(); ++it)
		{
			std::string wildcard = (*it)->GetWildcard();
			for (vxU32 i = 0; i < wildcard.size(); ++i)
			{
				// Get the sub-wildcard:
				std::string extension = "";
				if (wildcard[i] == '*')
				{
					++i;
					if (wildcard[i] == '.')
					{
						++i;
						while (i < wildcard.size() && wildcard[i] != '|')
							extension += wildcard[i++];
					}
				}

				// If the wildcards match, we've found our loader:
				if (extension == file_ext)
				{
					pLoader = *it;
					break;
				}
			}
			if (pLoader != nullptr)
				break;
		}

		if (pLoader == nullptr)
			return nullptr;

		ResCacheData data;
		if (!pLoader->LoadResource(key, &data.mDataPtr, &data.mSize, this))
			return nullptr;

		mLRU.push_front(key);
		mCacheData.insert(std::make_pair(key, data));
		auto find = mCacheData.find(key);
		return &find->second;
	}

	ResCacheData* ResourceCache::GetCacheData(const ResourceKey& key)
	{
		auto find = mCacheData.find(key);
		if (find == mCacheData.end())
			return LoadResource(key);
		Promote(key);
		return &find->second;
	}

	void ResourceCache::Promote(const ResourceKey& key)
	{
		for (auto it = mLRU.begin(); it != mLRU.end(); ++it)
		{
			if (*it == key)
			{
				mLRU.erase(it);
				mLRU.push_front(key);
				return;
			}
		}
	}

	vxBool ResourceCache::MakeRoom(const vxU32 size)
	{
		if (mCacheSize == kInfiniteCacheSize)
			return true;

		if (size > mCacheSize)
			return false;

		while ((mCacheSize - mAllocated) < size)
			FreeOneResource();

		return true;
	}

	void ResourceCache::FreeOneResource()
	{
		ResourceKey key = mLRU.back();
		mLRU.pop_back();

		auto find = mCacheData.find(key);
		Free(find->second.mDataPtr, find->second.mSize);
		if (find != mCacheData.end())
			mCacheData.erase(find);
	}

	void ResourceCache::Destroy()
	{
		for (auto it = mLoaders.begin(); it != mLoaders.end(); ++it)
		{
			delete *it;
		}
		mLoaders.clear();

		while (!mCacheData.empty())
			FreeOneResource();

		delete mResourcePackage;
	}
}