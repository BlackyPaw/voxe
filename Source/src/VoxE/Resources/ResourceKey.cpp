#include <VoxE/Resources/ResourceKey.hpp>

namespace VX
{
	ResourceKey::ResourceKey() :
		mName(""),
		mID(0)
	{
	}

	ResourceKey::ResourceKey(const vxString& name)
	{
		FromName(name);
	}

	void ResourceKey::FromName(const vxString& name)
	{
		mName = name;

		// Calculate the resource key's unique ID:
		static const vxU64 FNV_Prime = 0x00000100000001B3;
		vxU64 hash = 0xCBF29CE484222325;

		for (vxU32 i = 0; i < name.size(); ++i)
		{
			hash ^= name[i];
			hash *= FNV_Prime;
		}

		mID = hash;
	}

	const vxString& ResourceKey::GetName() const
	{
		return mName;
	}

	const ResourceID ResourceKey::ToUniqueID() const
	{
		return mID;
	}

	vxBool ResourceKey::operator<  (const ResourceKey& key) const
	{
		return (mName < key.mName);
	}

	vxBool ResourceKey::operator== (const ResourceKey& key) const
	{
		return (mName == key.mName);
	}

	vxBool ResourceKey::operator!= (const ResourceKey& key) const
	{
		return !((*this) == key);
	}
}