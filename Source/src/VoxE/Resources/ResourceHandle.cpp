#include <VoxE/Resources/ResourceHandle.hpp>
#include <VoxE/Resources/ResourceCache.hpp>

namespace VX
{
	ResourceHandle::ResourceHandle() :
		mKey(),
		mResCache(nullptr)
	{
	}

	ResourceHandle::ResourceHandle(const ResourceKey& key, ResourceCache* pResCache) :
		mKey(key),
		mResCache(pResCache)
	{
	}

	ResourceHandle::ResourceHandle(const ResourceHandle& h)
	{
		Copy(h);
	}

	ResourceHandle::~ResourceHandle()
	{
	}

	vxU32 ResourceHandle::Size()
	{
		return mResCache->GetCacheData(mKey)->mSize;
	}

	ResourceHandle::operator vxBool() const
	{
		return IsValid();
	}

	vxBool ResourceHandle::operator! () const
	{
		return !IsValid();
	}

	vxBool ResourceHandle::IsValid() const
	{
		return (mResCache != nullptr);
	}

	const ResourceKey& ResourceHandle::GetKey() const
	{
		return mKey;
	}

	ResourceHandle& ResourceHandle::operator= (const ResourceHandle& h)
	{
		Copy(h);
		return *this;
	}

	vxBool ResourceHandle::operator== (const ResourceHandle& h) const
	{
		return (mKey == h.mKey && mResCache == h.mResCache);
	}

	vxBool ResourceHandle::operator!= (const ResourceHandle& h) const
	{
		return !((*this) == h);
	}

	void* ResourceHandle::Dereference()
	{
		if(!IsValid())
			return nullptr;
		ResCacheData* pData = mResCache->GetCacheData(mKey);
		if (pData != nullptr)
			return pData->mDataPtr;
		return nullptr;
	}

	void ResourceHandle::Copy(const ResourceHandle& h)
	{
		mKey = h.mKey;
		mResCache = h.mResCache;
	}
}
