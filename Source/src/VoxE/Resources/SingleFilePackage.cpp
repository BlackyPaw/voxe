#include <VoxE/Resources/SingleFilePackage.hpp>
#include <VoxE/Native/FileStream.hpp>

namespace VX
{
	SingleFilePackage::SingleFilePackage()
	{
	}

	SingleFilePackage::~SingleFilePackage()
	{
	}

	InputStreamPtr SingleFilePackage::GetStream(const ResourceKey& key, vxU32* pSize)
	{
		File f(key.GetName());
		(*pSize) = f.GetSize();
		return InputStreamPtr(new FileInputStream(f));
	}
}