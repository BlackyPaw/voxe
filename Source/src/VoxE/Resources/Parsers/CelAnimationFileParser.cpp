#include <VoxE/Resources/Parsers/CelAnimationFileParser.hpp>
#include <tinyxml2/tinyxml2.h>

namespace VX
{
	CelAnimationFileParser::CelAnimationFileParser()
	{
	}

	vxBool CelAnimationFileParser::LoadAnimationFile(const vxU8* buffer, const vxU32 bufferLen, CelAnimation** pAnim)
	{
		using namespace tinyxml2;

		XMLDocument doc;
		if (doc.Parse(reinterpret_cast<const char*>(buffer), bufferLen) != XML_NO_ERROR)
			return false;

		XMLElement* pAnimation = doc.FirstChildElement("Animation");

		vxF32 speed = pAnimation->FloatAttribute("speed");
		std::vector<IntRect> states;
		for (XMLElement* pState = pAnimation->FirstChildElement("State"); pState != nullptr; pState = pState->NextSiblingElement())
		{
			IntRect state;
			state.mLeft = pState->IntAttribute("left");
			state.mTop = pState->IntAttribute("top");
			state.mWidth = pState->IntAttribute("width");
			state.mHeight = pState->IntAttribute("height");
			states.push_back(state);
		}

		CelAnimation* pRes = new CelAnimation(speed, states);
		(*pAnim) = pRes;

		return true;
	}
}