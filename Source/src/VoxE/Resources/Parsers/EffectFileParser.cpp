#include <VoxE/Resources/Parsers/EffectFileParser.hpp>
#include <VoxE/Native/DebugPrint.hpp>

namespace VX
{
	static vxU32 kMaxLogSize = 4095;
	static vxChar* kLogBuffer = new vxChar[kMaxLogSize+1];

	EffectFileParser::EffectFileParser()
	{
	}

	vxBool EffectFileParser::LoadEffectFile(const vxU8* buffer, const vxU32 bufferLen, ShaderProgram** pProgram)
	{
		if (buffer == nullptr || bufferLen == 0 || pProgram == nullptr)
			return false;

		const vxChar* string = reinterpret_cast<const vxChar*>(buffer);
		if (strncmp(string, "EFFECT", 6) != 0)
			return false;

		mIdx = 6;
		mLine = 1;
		mBuffer = string;
		mBufferLen = bufferLen;
		mProgram = new ShaderProgram();

		// - Eat all whitespace:
		EatWhitespace();
		// Now we definitely expect a '{'
		if (mBuffer[mIdx] != '{')
			return ReturnFailure();
		mIdx++; // Skip the '{'

		// After this line either a declaration of a semantic follows
		// or a shader code follows:
		mToken = GetToken();
		while (mToken.length() != 0)
		{
			if (mToken == "in")
			{
				if (!ParseSemantic())
					return ReturnFailure();
			}
			else if(mToken == "map")
			{
				if(!ParseMapping())
					return ReturnFailure();
			}
			else
			{
				if (!ParseShaderBlock())
					return ReturnFailure();
			}

			EatWhitespace();
			mToken = GetToken();
		}

		// Now we definitely can expect a '}' to be here, so check for it:
		if (mBuffer[mIdx] != '}')
		{
			DebugPrintF("[EffectFileParser] %i: Unexpected character '%c'. Expected a '}'.\n", mLine, mBuffer[mIdx]);
			return false;
		}
		mIdx++; // Skip the '}'.

		if (!mProgram->Link(&kLogBuffer, kMaxLogSize))
		{
			kLogBuffer[kMaxLogSize] = '\0';
			if (kLogBuffer[0] == '\0')
				DebugPrintF("[EffectFileParser] Failed to link shader program! No log information available.\n");
			else
				DebugPrintF("[EffectFileParser] Failed to link shader program! Info log: %s\n", kLogBuffer);
			return false;
		}

		for (auto it = mSemantics.Begin(); it != mSemantics.End(); ++it)
		{
			mProgram->AssignSemantic(it->second, it->first);
		}
		for(auto it = mMappings.Begin(); it != mMappings.End(); ++it)
		{
			mProgram->AddMapping((*it));
		}
		(*pProgram) = mProgram;

		return true;
	}

	void EffectFileParser::EatWhitespace()
	{
		while (true)
		{
			if (mIdx + 2 < mBufferLen && mBuffer[mIdx] == '/' && mBuffer[mIdx+1] == '/')
			{
				mIdx += 2;
				while (mBuffer[mIdx] != '\n')
					mIdx++;
			}
			else if (mIdx + 2 < mBufferLen && mBuffer[mIdx] == '/' && mBuffer[mIdx + 1] == '*')
			{
				mIdx += 2;
				while (true)
				{
					if (mIdx + 2 < mBufferLen && mBuffer[mIdx] == '*')
					{
						if (mBuffer[mIdx + 1] == '/')
						{
							mIdx += 2;
							break;
						}
					}
					if (mBuffer[mIdx] == '\n')
						mLine++;

					mIdx++;
				}
			}
			if (mBuffer[mIdx] == '\n')
				mLine++;

			if (!isspace(mBuffer[mIdx]))
				break;
			mIdx++;
		}
	}

	vxString EffectFileParser::GetToken()
	{
		EatWhitespace();
		vxString token = "";
		while (isalnum(mBuffer[mIdx]) || mBuffer[mIdx] == '_')
			token += mBuffer[mIdx++];
		return token;
	}

	vxBool EffectFileParser::ParseSemantic()
	{
		mToken = GetToken();
		if (mToken.length() == 0)
			return false;
		vxString name = mToken;
		EatWhitespace();
		if (mBuffer[mIdx] != ':')
		{
			DebugPrintF("[EffectFileParser] %i: Unexpected character '%c' in semantic declaration. Expected a colon ':'.\n", mLine, mBuffer[mIdx]);
			return false;
		}
		mIdx++; // Skip the ':'.
		mToken = GetToken();
		ShaderSemantic semantic = SHADER_SEMANTIC_COUNTER;

		// Now we parse the semantic:

		// Attributes:
		if (mToken == "POSITION")
			semantic = SHADER_SEMANTIC_POSITION;
		else if (mToken == "NORMAL")
			semantic = SHADER_SEMANTIC_NORMAL;
		else if (mToken == "COLOR")
			semantic = SHADER_SEMANTIC_COLOR;
		else if (mToken == "TANGENT")
			semantic = SHADER_SEMANTIC_TANGENT;
		else if (mToken == "TEX_COORDS0")
			semantic = SHADER_SEMANTIC_TEX_COORDS0;
		else if (mToken == "TEX_COORDS1")
			semantic = SHADER_SEMANTIC_TEX_COORDS1;

		// Uniforms:
		else if (mToken == "PROJECTION_MATRIX")
			semantic = SHADER_SEMANTIC_PROJECTION_MATRIX;
		else if (mToken == "VIEW_MATRIX")
			semantic = SHADER_SEMANTIC_VIEW_MATRIX;
		else if (mToken == "WORLD_MATRIX")
			semantic = SHADER_SEMANTIC_WORLD_MATRIX;
		else if (mToken == "NORMAL_MATRIX")
			semantic = SHADER_SEMANTIC_NORMAL_MATRIX;
		else if (mToken == "TEXTURE_MATRIX")
			semantic = SHADER_SEMANTIC_TEXTURE_MATRIX;

		// Otherwise we print out an error:
		else
		{
			DebugPrintF("[EffectFileParser] %i: Invalid semantic: %s.\n", mLine, mToken.c_str());
			return false;
		}

		// Finally, all we still have to check for is the endinge semicolon (';'):
		EatWhitespace();
		if (mBuffer[mIdx] != ';')
		{
			DebugPrintF("[EffectFileParser] %i: Invalid character '%c' at the end of semantic declaration. Expected a semicolon ';'.\n", mLine, mBuffer[mIdx]);
			return false;
		}
		mIdx++; // Skip the ';'.

		// Finally save the semantic and we're done: :)
		std::pair<ShaderSemantic, vxString> pair = std::pair<ShaderSemantic, vxString>(semantic, name);
		mSemantics.Push(pair);
		return true;
	}

	vxBool EffectFileParser::ParseMapping()
	{
		mToken = GetToken();
		vxString name = mToken;
		if(mToken.length() == 0)
			return false;

		mToken = GetToken();
		if(mToken != "to")
		{
			DebugPrintF("[EffectFileParser] %i: Invalid mapping declaration: Expected 'to'!\n", mLine);
			return false;
		}

		mToken = GetToken();
		vxString param = mToken;

		EatWhitespace();
		if(mBuffer[mIdx] != '(')
		{
			DebugPrintF("[EffectFileParser] %i: Invalid mapping declaration: Expected '('!\n", mLine);
			return false;
		}
		mIdx++; // Skip the '('
		mToken = GetToken(); // Find the 'type' keyword
		if(mToken != "type")
		{
			DebugPrintF("[EffectFileParser] %i: Invalid mapping declaration: Expected type definition!\n", mLine);
			return false;
		}

		EatWhitespace();
		if(mBuffer[mIdx] != '=')
		{
			DebugPrintF("[EffectFileParser] %i: Invalid mapping declaration: Expected '=' in type definition!\n", mLine);
			return false;
		}
		mIdx++; // Skip the '='

		// Get the type:
		mToken = GetToken();
		vxString type = mToken;

		EatWhitespace();
		if(mBuffer[mIdx] != ')')
		{
			DebugPrintF("[EffectFileParser] %i: Invalid mapping declaration: Expected ')' after type definition!\n", mLine);
			return false;
		}
		mIdx++; // Skip the ')'

		EatWhitespace();
		if(mBuffer[mIdx] != ';')
		{
			DebugPrintF("[EffectFileParser] %i: Invalid mapping declaration: Expected ';'!\n", mLine);
			return false;
		}
		mIdx++; // Skip the ';'

		ShaderMappingType mappingType = SHADER_MAPPING_TYPE_INVALID;
		if(type == "texture")
			mappingType = SHADER_MAPPING_TYPE_TEXTURE;
		else if(type == "float")
			mappingType = SHADER_MAPPING_TYPE_FLOAT;
		else if(type == "int")
			mappingType = SHADER_MAPPING_TYPE_INT;
		else if(type == "uint")
			mappingType = SHADER_MAPPING_TYPE_UINT;
		else if(type == "bool")
			mappingType = SHADER_MAPPING_TYPE_BOOL;

		if(mappingType == SHADER_MAPPING_TYPE_INVALID)
		{
			DebugPrintF("[EffectFileParser] %i: Invalid mapping declaration: Invalid type!\n", mLine);
			return false;
		}

		//DebugPrintF("[EffectFileParser] %i: Found shader mapping: <%s, %s, %s>!\n", line, name.c_str(), param.c_str(), type.c_str());
		ShaderMapping mapping = { name, param, mappingType };
		mMappings.Push(mapping);
		return true;
	}

	vxBool EffectFileParser::ParseShaderBlock()
	{
		// Check, which shader was selected:
		ShaderType type = SHADER_TYPE_INVALID;
		if (mToken == "VERTEX_SHADER")
			type = SHADER_TYPE_VERTEX;
		else if (mToken == "FRAGMENT_SHADER")
			type = SHADER_TYPE_FRAGMENT;
		else if (mToken == "GEOMETRY_SHADER")
			type = SHADER_TYPE_GEOMETRY;
		else if (mToken == "TESS_EVALUATION_SHADER")
			type = SHADER_TYPE_TESS_EVALUATION;
		else if (mToken == "TESS_CONTROL_SHADER")
			type = SHADER_TYPE_TESS_CONTROL;

		// If the shader type could not be determined
		// we print out an error and return false of course:
		else
		{
			DebugPrintF("[EFfectFileParser] %i: Invalid shader block name: %s.\n", mLine, mToken.c_str());
			return false;
		}

		// Next we definitely expect the next character to be a '{':
		EatWhitespace();
		if (mBuffer[mIdx] != '{')
			return false;
		mIdx++; // Skip the '{'.

		EatWhitespace();
		vxU32 level = 0;
		vxString source = "";
		// Now we load the whole shader source code:
		while (!(mBuffer[mIdx] == '}' && level == 0))
		{
			if (mBuffer[mIdx] == '{')
				level++;
			else if (mBuffer[mIdx] == '}')
				level--;
			else if (mBuffer[mIdx] == '\n')
				mLine++;

			source += mBuffer[mIdx++];
		}

		mIdx++; // Skip the '}'.

		// Now we can actually parse the shader: :)

		Shader shader;
		if (!shader.Create(type, source.c_str(), source.size(), &kLogBuffer, kMaxLogSize))
		{
			kLogBuffer[kMaxLogSize] = '\0';
			if (kLogBuffer[0] == '\0')
				DebugPrintF("[EffectFileParser] %i: Failed to compile shader (%u). No log information available!\n", mLine, type);
			else
				DebugPrintF("[EffectFileParser] %i: Failed to compile shader (%u). Info log: %s\n", mLine, type, kLogBuffer);

			return false;
		}

		mProgram->AddShader(shader);
		return true;
	}

	vxBool EffectFileParser::ReturnFailure()
	{
		PrintDebugMsg();
		SafeDestroy();
		return false;
	}

	void EffectFileParser::PrintDebugMsg()
	{
		if (mToken.size() == 0)
			DebugPrintF("[EffectFileParser] %i: Unexpected character: '%c'.\n", mLine, mBuffer[mIdx]);
		else
			DebugPrintF("[EffectFileParser] %i: Unexpected token: %s.\n", mLine, mToken.c_str());
	}

	void EffectFileParser::SafeDestroy()
	{
		if (mProgram != nullptr)
			delete mProgram;
	}
}
