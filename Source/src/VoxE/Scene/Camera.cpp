/*
 * Camera.cpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/Camera.hpp>

namespace VX
{
	Camera::Camera()
	{
		Orthographic(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
	}

	Camera::~Camera()
	{
	}

	void Camera::Orthographic(const vxF32 left, const vxF32 right, const vxF32 bottom, const vxF32 top, const vxF32 near, const vxF32 far)
	{
		mProjection = VX::Ortho(left, right, bottom, top, near, far);
	}

	void Camera::Perspective(const vxF32 fovy, const vxF32 aspect, const vxF32 near, const vxF32 far)
	{
		mProjection = VX::Perspective(fovy, aspect, near, far);
	}

	const Matrix4& Camera::GetProjection() const
	{
		return mProjection;
	}
}
