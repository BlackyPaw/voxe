/*
 * SceneNode.cpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Renderer/Renderer.hpp>
#include <VoxE/Scene/Scene.hpp>
#include <VoxE/Scene/SceneNode.hpp>

#include <algorithm>

namespace VX
{
	SceneNode::SceneNode(Scene* scene) :
		mScene(scene),
		mParent(nullptr)
	{
	}

	SceneNode::~SceneNode()
	{
		for(auto it = mChildren.begin(); it != mChildren.end(); ++it)
		{
			delete (*it);
		}
	}

	void SceneNode::AddChild(SceneNode* child)
	{
		auto find = std::find(mChildren.begin(), mChildren.end(), child);
		if(find != mChildren.end())
			return;
		child->mParent = this;
		mChildren.push_back(child);
	}

	void SceneNode::RemoveChild(SceneNode* child)
	{
		auto find = std::find(mChildren.begin(), mChildren.end(), child);
		if(find != mChildren.end())
		{
			(*find)->mParent = nullptr;
			mChildren.erase(find);
		}
	}

	void SceneNode::SetDrawCallback(const DrawCallback& callback)
	{
		mCallback = callback;
	}

	void SceneNode::Draw()
	{
		TransformStack& stack = mScene->GetTransformStack();
		stack.Push();
		stack.Top() *= GetTransform();

		Renderer::GetSharedInstance()->ApplyTransform(stack.Top());

		if(!mCallback.Empty())
		{
			mCallback.Invoke();
		}

		for(auto it = mChildren.begin(); it != mChildren.end(); ++it)
		{
			(*it)->Draw();
		}

		stack.Pop();
	}
}
