/*
 * Scene.cpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Renderer/Renderer.hpp>
#include <VoxE/Scene/Scene.hpp>

namespace VX
{
	Scene::Scene() :
		mRoot(this)
	{
	}

	Scene::~Scene()
	{
	}

	void Scene::AddNode(SceneNode* child)
	{
		mRoot.AddChild(child);
	}

	void Scene::RemoveNode(SceneNode* child)
	{
		mRoot.RemoveChild(child);
	}

	void Scene::Draw()
	{
		Renderer::GetSharedInstance()->ApplyCamera(mCamera);
		mRoot.Draw();
	}

	Camera& Scene::GetCamera()
	{
		return mCamera;
	}

	Camera const& Scene::GetCamera() const
	{
		return mCamera;
	}

	TransformStack& Scene::GetTransformStack()
	{
		return mTransformStack;
	}

	TransformStack const& Scene::GetTransformStack() const
	{
		return mTransformStack;
	}
}
