#include <VoxE/Scene/TransformStack.hpp>

namespace VX
{
	void TransformStack::Push()
	{
		mStack.Push(mTop);
	}

	void TransformStack::Pop()
	{
		mTop = mStack.Back();
		mStack.Pop();
	}

	Matrix4& TransformStack::Top()
	{
		return mTop;
	}

	Matrix4 const& TransformStack::Top() const
	{
		return mTop;
	}
}
