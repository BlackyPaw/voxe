#include <VoxE/Graphics/Texture.hpp>
#include <VoxE/Renderer/TextureManager.hpp>
#include <GL/glew.h>

namespace VX
{
	Texture::Texture() :
		mReferences(nullptr),
		mHandle(0),
		mWidth(0),
		mHeight(0)
	{
	}

	Texture::Texture(const vxU32 width, const vxU32 height, const vxU8* pixels) :
		mReferences(nullptr),
		mHandle(0),
		mWidth(0),
		mHeight(0)
	{
		Create(width, height, pixels);
	}

	Texture::Texture(const Texture& t) :
		mReferences(nullptr)
	{
		Copy(t);
	}

	Texture::~Texture()
	{
		Destroy();
	}

	void Texture::Create(const vxU32 width, const vxU32 height, const vxU8* pixels)
	{
		Destroy();

		vxU32 handle = 0;
		glGenTextures(1, &handle);
		if (handle == 0)
			return;

		TextureManager* pTextureManager = TextureManager::GetSharedInstance();
		vxU32 oldTexture = pTextureManager->GetTextureHandle(pTextureManager->GetActiveUnit());
		glBindTexture(GL_TEXTURE_2D, handle);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

		// Re-bind old texture after initialization in order to avoid NASTY texture leaks:
		if(oldTexture != 0)
			glBindTexture(GL_TEXTURE_2D, oldTexture);

		mReferences = new vxU32(1);
		mHandle = handle;
		mWidth = width;
		mHeight = height;
	}

	void Texture::SetMinFilter(const TextureFilter& f)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (f == TEX_FILTER_NEAREST ? GL_NEAREST : GL_LINEAR));
	}

	void Texture::SetMagFilter(const TextureFilter& f)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (f == TEX_FILTER_NEAREST ? GL_NEAREST : GL_LINEAR));
	}

	void Texture::SetWrapS(const TextureWrap& w)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (w == TEX_WRAP_REPEAT ? GL_REPEAT : GL_CLAMP));
	}

	void Texture::SetWrapT(const TextureWrap& w)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (w == TEX_WRAP_REPEAT ? GL_REPEAT : GL_CLAMP));
	}

	const vxU32 Texture::GetWidth() const
	{
		return mWidth;
	}

	const vxU32 Texture::GetHeight() const
	{
		return mHeight;
	}

	void Texture::Bind()
	{
		glBindTexture(GL_TEXTURE_2D, mHandle);
	}

	vxBool Texture::operator== (const Texture& t)
	{
		return (mHandle == t.mHandle);
	}

	vxBool Texture::operator!= (const Texture& t)
	{
		return !((*this) == t);
	}

	Texture& Texture::operator= (const Texture& t)
	{
		Copy(t);
		return *this;
	}

	vxU32 Texture::GetHandle() const
	{
		return mHandle;
	}

	void Texture::Copy(const Texture& t)
	{
		Destroy();

		mReferences = t.mReferences;
		mHandle = t.mHandle;
		mWidth = t.mWidth;
		mHeight = t.mHeight;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void Texture::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				glDeleteTextures(1, &mHandle);
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mWidth = 0;
		mHeight = 0;
	}
}
