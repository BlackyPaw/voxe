#include <VoxE/Graphics/Buffer.hpp>
#include <GL/glew.h>

namespace VX
{
	Buffer::Buffer() :
		mReferences(nullptr),
		mHandle(0),
		mType(BUFFER_TYPE_COUNTER),
		mTypeEnum(0)
	{
	}

	Buffer::Buffer(const Buffer& b) :
		mReferences(nullptr)
	{
		Copy(b);
	}

	Buffer::~Buffer()
	{
		Destroy();
	}

	Buffer& Buffer::operator= (const Buffer& b)
	{
		Copy(b);
		return *this;
	}

	vxBool Buffer::CreateImmutable(const BufferType& type, const vxI32 size, const void* data, const vxU32 flags)
	{
		Destroy();

		vxU32 handle = 0;
		vxU32 typeEnum = TranslateBufferType(type);
		glGenBuffers(1, &handle);
		if (handle == 0)
			return false;

		glBindBuffer(typeEnum, handle);
		if (glBufferStorage != nullptr)
			glBufferStorage(typeEnum, size, data, flags);

		mReferences = new vxU32(1);
		mHandle = handle;
		mType = type;
		mTypeEnum = typeEnum;

		return true;
	}

	vxBool Buffer::CreateMutable(const BufferType& type, const vxI32 size, const void* data, const vxU32 usage)
	{
		Destroy();

		vxU32 handle = 0;
		vxU32 typeEnum = TranslateBufferType(type);
		glGenBuffers(1, &handle);
		if (handle == 0)
			return false;

		glBindBuffer(typeEnum, handle);
		glBufferData(typeEnum, size, data, usage);

		mReferences = new vxU32(1);
		mHandle = handle;
		mType = type;
		mTypeEnum = typeEnum;

		return true;
	}

	void Buffer::SubData(const vxI32 offset, const vxI32 size, const void* data)
	{
		glBufferSubData(mTypeEnum, offset, size, data);
	}

	void* Buffer::Map(const vxU32 access)
	{
		return glMapBuffer(mTypeEnum, access);
	}

	void* Buffer::MapRange(const vxI32 offset, const vxI32 length, const vxU32 access)
	{
		return glMapBufferRange(mTypeEnum, offset, length, access);
	}

	void Buffer::UnMap()
	{
		glUnmapBuffer(mTypeEnum);
	}

	const BufferType& Buffer::GetBufferType() const
	{
		return mType;
	}

	void Buffer::Bind()
	{
		glBindBuffer(mTypeEnum, mHandle);
	}

	void Buffer::UnBind()
	{
		glBindBuffer(mTypeEnum, 0);
	}

	void Buffer::Copy(const Buffer& b)
	{
		Destroy();

		mReferences = b.mReferences;
		mHandle = b.mHandle;
		mType = b.mType;
		mTypeEnum = b.mTypeEnum;
		
		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void Buffer::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				if (mHandle != 0)
					glDeleteBuffers(1, &mHandle);
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mType = BUFFER_TYPE_COUNTER;
		mTypeEnum = 0;
	}
}