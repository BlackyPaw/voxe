#include <VoxE/Graphics/ShaderProgram.hpp>
#include <GL/glew.h>

namespace VX
{
	ShaderProgram::ShaderProgram() :
		mReferences(nullptr),
		mHandle(0)
	{
	}

	ShaderProgram::ShaderProgram(const ShaderProgram& p) :
		mReferences(nullptr)
	{
		Copy(p);
	}

	ShaderProgram::~ShaderProgram()
	{
		Destroy();
	}

	void ShaderProgram::AddShader(const Shader& s)
	{
		if (mHandle == 0)
			mShaders.Push(s);
	}

	void ShaderProgram::RemoveShaders()
	{
		mShaders.Clear();
	}

	vxBool ShaderProgram::Link(vxChar** pLog, const vxU32 maxLen)
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				if (mHandle != 0)
					glDeleteProgram(mHandle);
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mDeclaredVars.clear();
		mSemantics.clear();
		mMappings.clear();

		// Create a program:
		vxU32 handle = glCreateProgram();
		// Attach all shaders:
		for (auto it = mShaders.Begin(); it != mShaders.End(); ++it)
			glAttachShader(handle, it->GetHandle());

		// Now link the program:
		glLinkProgram(handle);

		// Now query the link status:
		GLint res = 0;
		glGetProgramiv(handle, GL_LINK_STATUS, &res);

		// Check if the linkage succeeded or not:
		if (res == GL_FALSE)
		{
			// If VOXE_DEBUG is defined and we're given a pLog pointer
			// we query the info log:
#if defined(VOXE_DEBUG)

			if (pLog != nullptr)
				glGetProgramInfoLog(handle, maxLen, nullptr, (*pLog));

#endif // VOXE_DEBUG

			// Delete the program in order not to create any memory leaks:
			glDeleteProgram(handle);
			return false;
		}

		// If we get here, we know everything went okay, so we
		// can now assign our handle:
		mReferences = new vxU32(1);
		mHandle = handle;
		mShaders.Clear(); // <- Clear the shaders as stated in the documentation of AddShader(...).

		return true;
	}

	void ShaderProgram::AssignSemantic(const vxString& var, const ShaderSemantic& sem)
	{
		if (mHandle == 0)
			return;

		// Query the shader argument index:
		vxU32 loc = ResolveName(var);

		// loc will not contain the shader argument index of the variable.

		// Check if the semantic was already assigned and erase its first
		// assignment if necessary:
		auto find = mSemantics.find(sem);
		if (find != mSemantics.end())
			mSemantics.erase(find);

		// Now actually assign the semantic:
		mSemantics.insert(std::make_pair(sem, loc));
	}

	vxU32 ShaderProgram::ResolveSemantic(const ShaderSemantic& sem)
	{
		auto find = mSemantics.find(sem);
		if (find == mSemantics.end())
			return kInvalidShaderArgIndex;
		return find->second;
	}

	vxU32 ShaderProgram::ResolveName(const vxString& name)
	{
		auto find = mDeclaredVars.find(name);
		if (find == mDeclaredVars.end())
		{
			// Query the shader argument index:
			GLint loc = -1;
			if ((loc = glGetAttribLocation(mHandle, name.c_str())) == -1)
			{
				if ((loc = glGetUniformLocation(mHandle, name.c_str())) == -1)
					// If we can actually not find the variable in the list
					// of active attribute neither in the list of active uniforms
					// return:
					return kInvalidShaderArgIndex;
			}

			mDeclaredVars.insert(std::make_pair(name, static_cast<vxU32>(loc)));
			return static_cast<vxU32>(loc);
		}
		return find->second;
	}

	vxU32 ShaderProgram::ResolveMapping(const vxString& name)
	{
		auto find = mMappings.find(name);
		if(find != mMappings.end())
		{
			return ResolveName(find->second.mParam);
		}
		return kInvalidShaderArgIndex;
	}

	void ShaderProgram::AddMapping(const ShaderMapping& mapping)
	{
		auto find = mMappings.find(mapping.mName);
		if(find == mMappings.end())
			mMappings.insert(std::make_pair(mapping.mName, mapping));
	}

	void ShaderProgram::EnableAttribute(const vxU32 argIdx)
	{
		glEnableVertexAttribArray(argIdx);
	}

	void ShaderProgram::AttributeData(const vxU32 argIdx, const vxU32 size, const DataType& type, const vxBool normalized, const vxU32 stride, const void* pointer)
	{
		glVertexAttribPointer(argIdx, size, TranslateDataType(type), normalized, stride, pointer);
	}

	void ShaderProgram::DisableAttribute(const vxU32 argIdx)
	{
		glDisableVertexAttribArray(argIdx);
	}

	void ShaderProgram::Uniform1(const vxU32 argIdx, const vxU32 count, const vxF32* value)
	{
		glUniform1fv(argIdx, count, value);
	}

	void ShaderProgram::Uniform1(const vxU32 argIdx, const vxU32 count, const vxI32* value)
	{
		glUniform1iv(argIdx, count, value);
	}

	void ShaderProgram::Uniform1(const vxU32 argIdx, const vxU32 count, const vxU32* value)
	{
		glUniform1uiv(argIdx, count, value);
	}

	void ShaderProgram::Uniform2(const vxU32 argIdx, const vxU32 count, const vxF32* value)
	{
		glUniform2fv(argIdx, count, value);
	}

	void ShaderProgram::Uniform2(const vxU32 argIdx, const vxU32 count, const vxI32* value)
	{
		glUniform2iv(argIdx, count, value);
	}

	void ShaderProgram::Uniform2(const vxU32 argIdx, const vxU32 count, const vxU32* value)
	{
		glUniform2uiv(argIdx, count, value);
	}

	void ShaderProgram::Uniform3(const vxU32 argIdx, const vxU32 count, const vxF32* value)
	{
		glUniform3fv(argIdx, count, value);
	}

	void ShaderProgram::Uniform3(const vxU32 argIdx, const vxU32 count, const vxI32* value)
	{
		glUniform3iv(argIdx, count, value);
	}

	void ShaderProgram::Uniform3(const vxU32 argIdx, const vxU32 count, const vxU32* value)
	{
		glUniform3uiv(argIdx, count, value);
	}

	void ShaderProgram::Uniform4(const vxU32 argIdx, const vxU32 count, const vxF32* value)
	{
		glUniform4fv(argIdx, count, value);
	}

	void ShaderProgram::Uniform4(const vxU32 argIdx, const vxU32 count, const vxI32* value)
	{
		glUniform4iv(argIdx, count, value);
	}

	void ShaderProgram::Uniform4(const vxU32 argIdx, const vxU32 count, const vxU32* value)
	{
		glUniform4uiv(argIdx, count, value);
	}

	void ShaderProgram::UniformMatrix4x4(const vxU32 argIdx, const vxU32 count, const vxBool transpose, const vxF32* value)
	{
		glUniformMatrix4fv(argIdx, count, transpose, value);
	}

	void ShaderProgram::Bind()
	{
		glUseProgram(mHandle);
	}

	ShaderProgram& ShaderProgram::operator= (const ShaderProgram& p)
	{
		Copy(p);
		return *this;
	}

	void ShaderProgram::Copy(const ShaderProgram& p)
	{
		Destroy();

		mReferences = p.mReferences;
		mHandle = p.mHandle;
		mShaders = p.mShaders;
		mDeclaredVars = p.mDeclaredVars;
		mSemantics = p.mSemantics;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void ShaderProgram::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				if (mHandle != 0)
					glDeleteProgram(mHandle);
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mShaders.Clear();
		mDeclaredVars.clear();
		mSemantics.clear();
	}
}
