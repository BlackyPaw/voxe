#include <VoxE/Graphics/DataType.hpp>
#include <GL/glew.h>

namespace VX
{
	static const GLenum kDataTypeTranslations[DATA_TYPE_COUNTER] = { GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, GL_UNSIGNED_INT, GL_FLOAT, GL_DOUBLE };

	vxU32 TranslateDataType(const DataType& type)
	{
		if (type >= DATA_TYPE_COUNTER)
			return 0;
		return kDataTypeTranslations[type];
	}
}