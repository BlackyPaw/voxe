#include <VoxE/Graphics/Shader.hpp>
#include <GL/glew.h>

namespace VX
{
	Shader::Shader() :
		mReferences(nullptr),
		mHandle(0),
		mType(SHADER_TYPE_INVALID)
	{
	}

	Shader::Shader(const Shader& s) :
		mReferences(nullptr)
	{
		Copy(s);
	}

	Shader::~Shader()
	{
		Destroy();
	}

	vxBool Shader::Create(const ShaderType& type, const vxChar* pSource, const vxU32 sourceLen, vxChar** pLog, const vxU32 maxLen)
	{
		Destroy();

		// Determine the shader's shader type:
		vxU32 shaderEnum = 0;
		switch (type)
		{
			case SHADER_TYPE_VERTEX:			shaderEnum = GL_VERTEX_SHADER; break;
			case SHADER_TYPE_FRAGMENT:			shaderEnum = GL_FRAGMENT_SHADER; break;
			case SHADER_TYPE_GEOMETRY:			shaderEnum = GL_GEOMETRY_SHADER; break;
			case SHADER_TYPE_TESS_EVALUATION:	shaderEnum = GL_TESS_EVALUATION_SHADER; break;
			case SHADER_TYPE_TESS_CONTROL:		shaderEnum = GL_TESS_CONTROL_SHADER; break;
			case SHADER_TYPE_INVALID: 			return false;
		}
		// If no type was found, return:
		if (shaderEnum == 0)
			return false;

		vxU32 handle = glCreateShader(shaderEnum);
		// If no shader could be created, return:
		if (handle == 0)
			return false;

		glShaderSource(handle, 1, &pSource, reinterpret_cast<const GLint*>(&sourceLen));
		glCompileShader(handle);

		GLint res = 0;
		glGetShaderiv(handle, GL_COMPILE_STATUS, &res);
		// Check, if the compilation failed:
		if (res == GL_FALSE)
		{
			// If VOXE_DEBUG is defined and pLog is given we want to return not only
			// false, but also the info log of the shader:
#if defined(VOXE_DEBUG)

			if (pLog != nullptr)
				glGetShaderInfoLog(handle, maxLen, nullptr, (*pLog));

#endif // VOXE_DEBUG

			// Destroy the shader object, in order to to procude any memory leaks:
			glDeleteShader(handle);
			return false;
		}

		// If we actually get here, we know that the shader has been compiled
		// successfully, therefore we can now assign the newly created handle:
		mReferences = new vxU32(1);
		mHandle = handle;
		mType = type;

		return true;
	}

	const vxU32 Shader::GetHandle() const
	{
		return mHandle;
	}

	const ShaderType Shader::GetType() const
	{
		return mType;
	}

	Shader& Shader::operator= (const Shader& s)
	{
		Copy(s);
		return *this;
	}

	void Shader::Copy(const Shader& s)
	{
		Destroy();

		mReferences = s.mReferences;
		mHandle = s.mHandle;
		mType = s.mType;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void Shader::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				if (mHandle != 0)
					glDeleteShader(mHandle);
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mType = SHADER_TYPE_INVALID;
	}
}
