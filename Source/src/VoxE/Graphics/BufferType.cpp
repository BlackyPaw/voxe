#include <VoxE/Graphics/BufferType.hpp>
#include <GL/glew.h>

namespace VX
{
	static const GLenum kBufferTypeTranslations[BUFFER_TYPE_COUNTER] = { GL_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER };

	vxU32 TranslateBufferType(const BufferType& type)
	{
		if (type >= BUFFER_TYPE_COUNTER)
			return 0;
		return kBufferTypeTranslations[type];
	}
}