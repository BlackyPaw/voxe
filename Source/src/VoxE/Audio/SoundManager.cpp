#include <VoxE/Audio/SoundManager.hpp>
#include <AL/al.h>

namespace VX
{
	void SoundManager::Initialize()
	{
		alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
		alListener3f(AL_VELOCITY, 0.0f, 0.0f, 0.0f);

		for (vxU32 i = 0; i < kNumSources; ++i)
		{
			mSources[i].SetVolume(0.05f);
		}
	}

	void SoundManager::PlayOneShot(const ResourceHandle& sound)
	{
		for (vxU32 i = 0; i < kNumSources; ++i)
		{
			if (!mSources[i].IsPlaying())
			{
				mSources[i].SetSound(sound);
				mSources[i].Play();
			}
		}
	}
}
