#include <VoxE/Audio/ALContext.hpp>
#include <AL/alc.h>

namespace VX
{
	ALContext::ALContext() :
		mReferences(nullptr),
		mDevice(nullptr),
		mContext(nullptr)
	{
	}

	ALContext::ALContext(const ALContext& c) :
		mReferences(nullptr)
	{
		Copy(c);
	}

	ALContext::~ALContext()
	{
		Destroy();
	}

	vxBool ALContext::Create()
	{
		Destroy();

		ALCdevice* pDevice = nullptr;
		ALCcontext* pContext = nullptr;

		// Open the default device:
		pDevice = alcOpenDevice(nullptr);
		if (pDevice == nullptr)
			return false;

		// Now create the default context:
		pContext = alcCreateContext(pDevice, nullptr);
		if (pContext == nullptr)
		{
			alcCloseDevice(pDevice);
			return false;
		}

		alcMakeContextCurrent(pContext);

		mReferences = new vxU32(1);
		mDevice = pDevice;
		mContext = pContext;

		return true;
	}

	void ALContext::MakeCurrent()
	{
		if (mContext != nullptr)
			alcMakeContextCurrent(static_cast<ALCcontext*>(mContext));
	}

	vxBool ALContext::IsValid() const
	{
		return (mContext != nullptr && mDevice != nullptr);
	}

	ALContext& ALContext::operator= (const ALContext& c)
	{
		Copy(c);
		return *this;
	}

	void ALContext::Copy(const ALContext& c)
	{
		Destroy();

		mReferences = c.mReferences;
		mDevice = c.mDevice;
		mContext = c.mContext;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void ALContext::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				// Delete the OpenAL context here:
				if (mContext != nullptr)
					alcDestroyContext(static_cast<ALCcontext*>(mContext));
				if (mDevice != nullptr)
					alcCloseDevice(static_cast<ALCdevice*>(mDevice));
			}
		}

		mReferences = nullptr;
		mDevice = nullptr;
		mContext = nullptr;
	}
}
