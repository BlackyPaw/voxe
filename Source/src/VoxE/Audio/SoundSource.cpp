#include <VoxE/Audio/SoundSource.hpp>
#include <AL/al.h>

namespace VX
{
	SoundSource::SoundSource() :
		mReferences(nullptr),
		mHandle(0)
	{
		Create();
	}

	SoundSource::SoundSource(const SoundSource& s) :
		mReferences(nullptr)
	{
		Copy(s);
	}

	SoundSource::~SoundSource()
	{
		Destroy();
	}

	void SoundSource::SetSound(const ResourceHandle& sound)
	{
		ResourceHandle handle = sound;
		SoundBuffer* pBuffer = handle.Get<SoundBuffer>();
		if (pBuffer == nullptr)
			return;

		alSourcei(mHandle, AL_BUFFER, pBuffer->GetHandle());
	}

	void SoundSource::SetLooping(const vxBool l)
	{
		alSourcei(mHandle, AL_LOOPING, (l ? AL_TRUE : AL_FALSE));
	}

	void SoundSource::SetVolume(const vxF32 v)
	{
		alSourcef(mHandle, AL_MIN_GAIN, v);
		alSourcef(mHandle, AL_MAX_GAIN, v);
	}
	
	void SoundSource::Play()
	{
		alSourcePlay(mHandle);
	}

	void SoundSource::Pause()
	{
		alSourcePause(mHandle);
	}

	void SoundSource::Stop()
	{
		alSourceStop(mHandle);
	}

	vxBool SoundSource::IsPlaying() const
	{
		vxI32 state = 0;
		alGetSourcei(mHandle, AL_SOURCE_STATE, &state);
		return (state == AL_PLAYING);
	}

	SoundSource& SoundSource::operator= (const SoundSource& s)
	{
		Copy(s);
		return *this;
	}

	void SoundSource::Create()
	{
		Destroy();

		alGenSources(1, &mHandle);
	}

	void SoundSource::Copy(const SoundSource& s)
	{
		Destroy();

		mReferences = s.mReferences;
		mHandle = s.mHandle;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void SoundSource::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				// Delete the source if one exists:
				if (mHandle != 0)
					alDeleteSources(1, &mHandle);
			}
		}

		mReferences = nullptr;
		mHandle = 0;
	}
}
