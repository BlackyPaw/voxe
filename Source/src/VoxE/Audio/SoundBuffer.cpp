#include <VoxE/Audio/SoundBuffer.hpp>
#include <AL/al.h>

namespace VX
{
	SoundBuffer::SoundBuffer() :
		mReferences(nullptr),
		mHandle(0)
	{
	}

	SoundBuffer::SoundBuffer(const SoundBuffer& b) :
		mReferences(nullptr)
	{
		Copy(b);
	}

	SoundBuffer::~SoundBuffer()
	{
		Destroy();
	}

	vxBool SoundBuffer::Create(const vxU32 channels, const vxU32 bitdepth, const vxU8* pcm, const vxU32 size, const vxU32 frequency)
	{
		Destroy();

		vxU32 handle = 0;
		alGenBuffers(1, &handle);
		if (handle == 0)
			return false;

		vxU32 format = 0;
		if (channels == 1)
		{
			if (bitdepth == 8)
				format = AL_FORMAT_MONO8;
			else if (bitdepth == 16)
				format = AL_FORMAT_MONO16;
		}
		else if (channels == 2)
		{
			if (bitdepth == 8)
				format = AL_FORMAT_STEREO8;
			else if (bitdepth == 16)
				format = AL_FORMAT_STEREO16;
		}
		else
			format = AL_FORMAT_STEREO16;

		alBufferData(handle, format, pcm, size, frequency);

		mReferences = new vxU32(1);
		mHandle = handle;

		return true;
	}

	vxBool SoundBuffer::IsValid() const
	{
		return (mHandle != 0);
	}

	vxU32 SoundBuffer::GetHandle() const
	{
		return mHandle;
	}

	SoundBuffer& SoundBuffer::operator= (const SoundBuffer& b)
	{
		Copy(b);
		return *this;
	}

	void SoundBuffer::Copy(const SoundBuffer& b)
	{
		Destroy();

		mReferences = b.mReferences;
		mHandle = b.mHandle;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void SoundBuffer::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				// Free the buffer if it exists:
				if (mHandle != 0)
					alDeleteBuffers(1, &mHandle);
			}
		}

		mReferences = nullptr;
		mHandle = 0;
	}
}
