/*
#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Renderer/SpriteTesselator.hpp>

namespace VX
{
	SpriteTesselator::SpriteTesselator(const vxU32 max) :
		mMaxSprites(max),
		mNumSprites(0)
	{
		mVertexBuffer.CreateMutable(BUFFER_TYPE_VERTEX, max * 4 * 20 * sizeof(vxF32), nullptr, GL_STREAM_DRAW);
		mIndexBuffer.CreateMutable(BUFFER_TYPE_INDEX, max * 6 * sizeof(vxU32), nullptr, GL_STREAM_DRAW);

		mVertexMap	= mVertexBuffer.Map(GL_WRITE_ONLY);
		mIndexMap	= mIndexBuffer.Map(GL_WRITE_ONLY);

		mVertexBuffer.UnBind();
		mIndexBuffer.UnBind();
	}

	SpriteTesselator::~SpriteTesselator()
	{
	}

	void SpriteTesselator::Draw(Sprite& s, RenderContext& c, Renderer& r)
	{
		// Early out:
		if (mVertexMap == nullptr || mIndexMap == nullptr)
			return;

		// Check for a need to flush the tesselator:
		if (mNumSprites + 1 > mMaxSprites)
			Flush(c, r);

		// Pre-transform all positions, so that we don't have
		// to pass a world matrix which allows us to store less
		// data. Later we simple set the world matrix to the
		// identity matrix.
		const vxF32* vertexData = s.GetVertexData();
		Vector3f mPositions[4];
		const Matrix4& t = s.GetTransform();
		mPositions[0] = t * Vector3f(vertexData[0], vertexData[1], vertexData[2]);
		mPositions[1] = t * Vector3f(vertexData[5], vertexData[6], vertexData[7]);
		mPositions[2] = t * Vector3f(vertexData[10], vertexData[11], vertexData[12]);
		mPositions[3] = t * Vector3f(vertexData[15], vertexData[16], vertexData[17]);

		const vxU32 kPositionLen = 3 * sizeof(vxF32);
		const vxU32 kTexCoordLen = 2 * sizeof(vxF32);
		for (vxU32 i = 0; i < 4; ++i)
		{
			memcpy(mVertexMap, &mPositions[i], kPositionLen);
			mVertexMap = Advance(mVertexMap, kPositionLen);
			memcpy(mVertexMap, &vertexData[i*5 + 3], kTexCoordLen);
			mVertexMap = Advance(mVertexMap, kTexCoordLen);
		}

		vxU32 baseIndex = mNumSprites * 4;
		vxU32 indices[6] = { baseIndex, baseIndex + 1, baseIndex + 2, baseIndex, baseIndex + 2, baseIndex + 3 };

		memcpy(mIndexMap, indices, 6 * sizeof(vxU32));
		mIndexMap = Advance(mIndexMap, 6 * sizeof(vxU32));

		mNumSprites++;

		// We're done, phooh... :)
		// return;
	}

	void SpriteTesselator::Flush(RenderContext& c, Renderer& r)
	{
		c.mTransformStack.Push();
		c.mTransformStack.Top() = Matrix4(); // <- The identity matrix. All positions (no more data is given) are pre-transformed.

		// Bind our buffers:
		mVertexBuffer.Bind();
		mIndexBuffer.Bind();

		// We have to unmap the buffers before use:
		mVertexBuffer.UnMap();
		mIndexBuffer.UnMap();

		ShaderProgram* pShader = c.mShader.Get<ShaderProgram>();
		if (pShader != nullptr)
		{
			pShader->EnableAttribute(pShader->ResolveSemantic(SHADER_SEMANTIC_POSITION));
			pShader->AttributeData(pShader->ResolveSemantic(SHADER_SEMANTIC_POSITION), 3, DATA_TYPE_FLOAT, false, 5 * sizeof(vxF32), (void*)(0));
			pShader->EnableAttribute(pShader->ResolveSemantic(SHADER_SEMANTIC_TEX_COORDS0));
			pShader->AttributeData(pShader->ResolveSemantic(SHADER_SEMANTIC_TEX_COORDS0), 2, DATA_TYPE_FLOAT, false, 5 * sizeof(vxF32), (void*)(3 * sizeof(vxF32)));
		}

		r.SetRenderContext(c);
		// And this is the line of code we're doing all this for:
		// We have to perform only one single driver call (glDrawElements):
		r.DrawPrimitivesI(POLYGON_MODE_TRIANGLES, mNumSprites * 6, DATA_TYPE_UINT, (void*)0);

		if (pShader != nullptr)
		{
			pShader->DisableAttribute(pShader->ResolveSemantic(SHADER_SEMANTIC_POSITION));
			pShader->DisableAttribute(pShader->ResolveSemantic(SHADER_SEMANTIC_TEX_COORDS0));
		}
		
		// Re-map the buffers:
		mVertexMap	= mVertexBuffer.Map(GL_WRITE_ONLY);
		mIndexMap	= mVertexBuffer.Map(GL_WRITE_ONLY);

		mVertexBuffer.UnBind();
		mIndexBuffer.UnBind();

		c.mTransformStack.Pop();
	}

	void* SpriteTesselator::Advance(void* p, const vxU32 n)
	{
		return (reinterpret_cast<vxU8*>(p)+n);
	}
}
*/
