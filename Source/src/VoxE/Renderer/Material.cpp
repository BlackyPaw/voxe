/*
 * Material.cpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Renderer/Material.hpp>

namespace VX
{
	MaterialVariant::MaterialVariant() :
		mReferences(nullptr),
		mType(INT),
		mAsInt(0)
	{
	}

	MaterialVariant::MaterialVariant(const MaterialVariant& v) :
		mReferences(nullptr),
		mType(INT)
	{
		Copy(v);
	}

	MaterialVariant::MaterialVariant(const vxString& t) :
		mReferences(nullptr),
		mType(INT)
	{
		SetTexture(t);
	}

	MaterialVariant::MaterialVariant(const vxF32 f) :
		mReferences(nullptr),
		mType(FLOAT),
		mAsFloat(f)
	{
	}

	MaterialVariant::MaterialVariant(const vxI32 i) :
		mReferences(nullptr),
		mType(INT),
		mAsInt(i)
	{
	}

	MaterialVariant::MaterialVariant(const vxU32 u) :
		mReferences(nullptr),
		mType(UINT),
		mAsUInt(u)
	{
	}

	MaterialVariant::MaterialVariant(const vxBool b) :
		mReferences(nullptr),
		mType(BOOL),
		mAsBool(b)
	{
	}

	MaterialVariant::~MaterialVariant()
	{
		Destroy();
	}

	MaterialVariant::Type MaterialVariant::GetType() const { return mType; }
	vxBool MaterialVariant::IsTexture() const { return mType == TEXTURE; }
	vxBool MaterialVariant::IsFloat() const { return mType == FLOAT; }
	vxBool MaterialVariant::IsInt() const { return mType == INT; }
	vxBool MaterialVariant::IsUInt() const { return mType == UINT; }
	vxBool MaterialVariant::IsBool() const { return mType == BOOL; }

	vxString MaterialVariant::ToTexture() const { return vxString(mAsTexture); }
	vxF32 MaterialVariant::ToFloat() const { return mAsFloat; }
	vxI32 MaterialVariant::ToInt() const { return mAsInt; }
	vxU32 MaterialVariant::ToUInt() const { return mAsUInt; }
	vxBool MaterialVariant::ToBool() const { return mAsBool; }

	void MaterialVariant::SetTexture(const vxString& t)
	{
		Destroy();
		mReferences = new vxU32(1);
		mType = TEXTURE;
		std::size_t len = t.size();
		vxCharPtr buffer = new vxChar[len];
		std::memcpy(buffer, t.c_str(), len);
		mAsTexture = buffer;
	}
	void MaterialVariant::SetFloat(const vxF32 f) { Destroy(); mType = FLOAT; mAsFloat = f; }
	void MaterialVariant::SetInt(const vxI32 i) { Destroy(); mType = INT; mAsInt = i; }
	void MaterialVariant::SetUInt(const vxU32 u) { Destroy(); mType = UINT; mAsUInt = u; }
	void MaterialVariant::SetBool(const vxBool b) { Destroy(); mType = BOOL; mAsBool = b; }

	MaterialVariant& MaterialVariant::operator= (const MaterialVariant& v)
	{
		Copy(v);
		return *this;
	}

	void MaterialVariant::Copy(const MaterialVariant& v)
	{
		Destroy();

		mReferences = v.mReferences;
		mType = v.mType;
		switch(mType)
		{
			case TEXTURE:
				mAsTexture = v.mAsTexture;
				break;
			case FLOAT:
				mAsFloat = v.mAsFloat;
				break;
			case INT:
				mAsInt = v.mAsInt;
				break;
			case UINT:
				mAsUInt = v.mAsUInt;
				break;
			case BOOL:
				mAsBool = v.mAsBool;
				break;
		}

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void MaterialVariant::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				delete mReferences;
				if(mType == TEXTURE && mAsTexture != nullptr)
					delete[] mAsTexture;
			}
		}
		mReferences = nullptr;
		mType = INT;
		mAsTexture = nullptr;
	}
}
