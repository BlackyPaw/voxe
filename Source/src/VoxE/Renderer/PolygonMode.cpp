#include <VoxE/Renderer/PolygonMode.hpp>
#include <GL/glew.h>

namespace VX
{
	static const GLenum kPolygonModeTranslations[POLYGON_MODE_COUNTER] = { GL_POINTS, GL_LINES, GL_LINE_LOOP, GL_LINE_STRIP, GL_TRIANGLES, GL_TRIANGLE_FAN, GL_TRIANGLE_STRIP };

	vxU32 TranslatePolygonMode(const PolygonMode& mode)
	{
		if (mode >= POLYGON_MODE_COUNTER)
			return 0;
		return kPolygonModeTranslations[mode];
	}
}