/*
 * TextureManager.cpp
 *
 *  Created on: 29.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Renderer/TextureManager.hpp>
#include <GL/glew.h>

namespace VX
{
	TextureManager::TextureManager() :
		mNextUnit(0)
	{
	}

	vxU32 TextureManager::BindTexture(Texture* texture)
	{
		auto find = mBoundTextures.find(texture->GetHandle());
		if(find == mBoundTextures.end())
		{
			glActiveTexture(GL_TEXTURE0 + mNextUnit);
			texture->Bind();
			// Erase any texture bound to this unit earlier:
			for(auto it = mBoundTextures.begin(); it != mBoundTextures.end(); ++it)
			{
				if(it->second == mNextUnit)
				{
					mBoundTextures.erase(it);
					break;
				}
			}
			mBoundTextures.insert(std::make_pair(texture->GetHandle(), mNextUnit));
			vxU32 result = mNextUnit;
			mNextUnit++;
			return result;
		}
		else
			return find->second;
	}

	vxU32 TextureManager::GetActiveUnit() const
	{
		if(mNextUnit == 0)
		{
			glActiveTexture(GL_TEXTURE0);
			return 0;
		}
		else
			return mNextUnit - 1;
	}

	vxU32 TextureManager::GetTextureHandle(const vxU32 unit) const
	{
		for(auto it = mBoundTextures.begin(); it != mBoundTextures.end(); ++it)
		{
			if(it->second == unit)
				return it->first;
		}
		return 0;
	}

	void TextureManager::Reset()
	{
		mNextUnit = 0;
		// We do not clear the list of bound textures in order to
		// prevent even more possible texture changes.
	}
}
