#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Graphics/Texture.hpp>
#include <VoxE/Renderer/Renderer.hpp>
#include <VoxE/Resources/ResourceCache.hpp>
#include <VoxE/Scene/Scene.hpp>
#include <GL/glew.h>

#include <VoxE/Native/DebugPrint.hpp>

namespace VX
{
	Renderer::Renderer() :
		mLockedScene(nullptr)
	{
	}

	Renderer::~Renderer()
	{
	}

	void Renderer::ApplyMaterial(const Material* mat, const vxBool force)
	{
		// Check if materials are the same:
		if(mLastMaterial.mName != mat->mName || force)
		{
			TextureManager::GetSharedInstance()->Reset();

			ShaderProgram* oldShader = mLastMaterial.mShader.Get<ShaderProgram>();
			ShaderProgram* shader = const_cast<Material*>(mat)->mShader.Get<ShaderProgram>();
			if(shader == nullptr)
				return;

			if(oldShader != shader)
				shader->Bind();


			mLastMaterial = *mat;

			// Pass all parameters:
			for(auto it = mLastMaterial.mParameters.begin(); it != mLastMaterial.mParameters.end(); ++it)
			{
				vxU32 argIdx = shader->ResolveMapping(it->mName);
				if(argIdx == kInvalidShaderArgIndex)
					continue;

				switch(it->mValue.GetType())
				{
					case MaterialVariant::TEXTURE:
					{
						ResourceHandle hTexture = ResourceCache::GetSharedInstance()->GetHandle(ResourceKey(it->mValue.ToTexture()));
						Texture* pTexture = hTexture.Get<Texture>();
						if(pTexture == nullptr)
							break;

						vxU32 unit = TextureManager::GetSharedInstance()->BindTexture(pTexture);
						shader->Uniform1(argIdx, 1, &unit);
					}
					break;
					case MaterialVariant::FLOAT:
					{
						vxF32 f = it->mValue.ToFloat();
						shader->Uniform1(argIdx, 1, &f);
					}
					break;
					case MaterialVariant::INT:
					{
						vxI32 i = it->mValue.ToInt();
						shader->Uniform1(argIdx, 1, &i);
					}
					break;
					case MaterialVariant::UINT:
					{
						vxU32 u = it->mValue.ToUInt();
						shader->Uniform1(argIdx, 1, &u);
					}
					break;
					case MaterialVariant::BOOL:
					{
						vxF32 b = (it->mValue.ToBool() ? 1.0f : 0.0f);
						shader->Uniform1(argIdx, 1, &b);
					}
					break;
				}
			}
		}
	}

	void Renderer::ApplyTransform(const Matrix4& t)
	{
		ShaderProgram* shader = mLastMaterial.mShader.Get<ShaderProgram>();
		if(shader == nullptr)
			return;

		vxU32 worldIdx = shader->ResolveSemantic(SHADER_SEMANTIC_WORLD_MATRIX);
		shader->UniformMatrix4x4(worldIdx, 1, false, t.RawPtr());
	}

	void Renderer::ApplyCamera(const Camera& c)
	{
		ShaderProgram* shader = mLastMaterial.mShader.Get<ShaderProgram>();
		if(shader == nullptr)
			return;

		vxU32 viewIdx = shader->ResolveSemantic(SHADER_SEMANTIC_VIEW_MATRIX);
		vxU32 projectionIdx = shader->ResolveSemantic(SHADER_SEMANTIC_PROJECTION_MATRIX);

		shader->UniformMatrix4x4(viewIdx, 1, false, c.GetTransform().RawPtr());
		shader->UniformMatrix4x4(projectionIdx, 1, false, c.GetProjection().RawPtr());
	}

	void Renderer::SetViewport(const vxI32 x, const vxI32 y, const vxI32 width, const vxI32 height)
	{
		glViewport(x, y, width, height);
	}

	void Renderer::DrawPrimitives(const PolygonMode& mode, const vxI32 first, const vxI32 count)
	{
		glDrawArrays(TranslatePolygonMode(mode), first, count);
	}

	void Renderer::DrawPrimitivesI(const PolygonMode& mode, const vxI32 count, const DataType& type, const void* indices)
	{
		glDrawElements(TranslatePolygonMode(mode), count, TranslateDataType(type), indices);
	}

	/*
	void Renderer::UpdateShader()
	{
		ShaderProgram* pProgram = mPrevContext.mShader.Get<ShaderProgram>();

		UpdateShaderProjection(pProgram);
		//UpdateShaderView(pProgram);
		UpdateShaderWorld(pProgram);
		//UpdateShaderNormal(pProgram);
		//UpdateShaderTexture(pProgram);
	}

	void Renderer::UpdateShaderProjection(ShaderProgram* pProgram)
	{
		if (pProgram == nullptr)
			pProgram = mPrevContext.mShader.Get<ShaderProgram>();
		if (pProgram == nullptr)
			return;

		pProgram->UniformMatrix4x4(pProgram->ResolveSemantic(SHADER_SEMANTIC_PROJECTION_MATRIX), 1, false, mPrevContext.mProjection.RawPtr());
	}

	void Renderer::UpdateShaderWorld(ShaderProgram* pProgram)
	{
		if (pProgram == nullptr)
			pProgram = mPrevContext.mShader.Get<ShaderProgram>();
		if (pProgram == nullptr)
			return;

		pProgram->UniformMatrix4x4(pProgram->ResolveSemantic(SHADER_SEMANTIC_WORLD_MATRIX), 1, false, mPrevWorldMatrix.RawPtr());
	}
	*/
}
