#include <VoxE/Graphics/Texture.hpp>
#include <VoxE/Renderer/Sprite.hpp>
#include <VoxE/Renderer/SpriteTesselator.hpp>

namespace VX
{
	ResourceHandle Sprite::kDefaultShader;

	Sprite::Sprite()
	{
		memset(mVertexData, 0, 20 * sizeof(vxF32));
	}

	Sprite::Sprite(const ResourceHandle& t, const IntRect& r) :
		mTexture(t),
		mSubset(r)
	{
		UpdatePositions();
		UpdateTexCoords();
	}

	Sprite::~Sprite()
	{
	}

	void Sprite::SetDefaultShader(const ResourceHandle& s)
	{
		kDefaultShader = s;
	}

	void Sprite::SetTexture(const ResourceHandle& t)
	{
		Texture* pTexture = const_cast<ResourceHandle&>(t).Get<Texture>();
		IntRect r = IntRect(0, 0, pTexture->GetWidth(), pTexture->GetHeight());
		SetTexture(t, r);
	}

	void Sprite::SetTexture(const ResourceHandle& t, const IntRect& r)
	{
		mTexture = t;
		mSubset = r;
		UpdatePositions(); // <- Update the positions as well, since they depend on the size of the selected texture rectangle.
		UpdateTexCoords();
	}

	void Sprite::SetShader(const ResourceHandle& s)
	{
		mShader = s;
	}

	void Sprite::SetSubset(const IntRect& r)
	{
		mSubset = r;
		UpdatePositions();
		UpdateTexCoords();
	}

	FloatRect Sprite::GetLocalBounds() const
	{
		FloatRect rect;
		rect.mLeft = 0.0f;
		rect.mTop = 0.0f;
		rect.mWidth = static_cast<vxF32>(mSubset.mWidth);
		rect.mHeight = static_cast<vxF32>(mSubset.mHeight);
		return rect;
	}

	const ResourceHandle& Sprite::GetTexture() const
	{
		return mTexture;
	}

	const ResourceHandle& Sprite::GetShader() const
	{
		return mShader;
	}

	const IntRect& Sprite::GetSubset() const
	{
		return mSubset;
	}

	const vxF32* Sprite::GetVertexData() const
	{
		return mVertexData;
	}

	void Sprite::Draw()
	{
		Material mat;
		mat.mShader = (mShader.IsValid() ? mShader : kDefaultShader);
		mat.mParameters.push_back({vxString("Texture"), MaterialVariant(mTexture.GetKey().GetName())});
		Renderer::GetSharedInstance()->ApplyMaterial(&mat, true);

		ShaderProgram* shader = mat.mShader.Get<ShaderProgram>();
		if(shader == nullptr)
			return;

		shader->EnableAttribute(shader->ResolveSemantic(SHADER_SEMANTIC_POSITION));
		shader->AttributeData(shader->ResolveSemantic(SHADER_SEMANTIC_POSITION), 3, DATA_TYPE_FLOAT, false, 5 * sizeof(vxF32), (void*)(mVertexData));
		shader->EnableAttribute(shader->ResolveSemantic(SHADER_SEMANTIC_TEX_COORDS0));
		shader->AttributeData(shader->ResolveSemantic(SHADER_SEMANTIC_TEX_COORDS0), 2, DATA_TYPE_FLOAT, false, 5 * sizeof(vxF32), (void*)(mVertexData + 3));

		static vxU32 kSpriteIndices[6] = { 0, 1, 2, 0, 2, 3 };

		Renderer::GetSharedInstance()->DrawPrimitivesI(POLYGON_MODE_TRIANGLES, 6, DATA_TYPE_UINT, kSpriteIndices);

		shader->DisableAttribute(shader->ResolveSemantic(SHADER_SEMANTIC_POSITION));
		shader->DisableAttribute(shader->ResolveSemantic(SHADER_SEMANTIC_TEX_COORDS0));
	}

	void Sprite::UpdatePositions()
	{
		mVertexData[0]  = 0.0f;
		mVertexData[1]  = 0.0f;
		mVertexData[2]  = 0.0f;
		mVertexData[5]  = mSubset.mWidth;
		mVertexData[6]  = 0.0f;
		mVertexData[7]  = 0.0f;
		mVertexData[10] = mSubset.mWidth;
		mVertexData[11] = mSubset.mHeight;
		mVertexData[12] = 0.0f;
		mVertexData[15] = 0.0f;
		mVertexData[16] = mSubset.mHeight;
		mVertexData[17] = 0.0f;
	}

	void Sprite::UpdateTexCoords()
	{
		if (!mTexture)
			return;
		Texture* pTexture = mTexture.Get<Texture>();
		vxF32 xscl = (1.0f / static_cast<vxF32>(pTexture->GetWidth()));
		vxF32 yscl = (1.0f / static_cast<vxF32>(pTexture->GetHeight()));

		vxF32 left = xscl * mSubset.mLeft;
		vxF32 right = xscl * (mSubset.mLeft + mSubset.mWidth);
		vxF32 top = yscl * mSubset.mTop;
		vxF32 bottom = yscl * (mSubset.mTop + mSubset.mHeight);

		mVertexData[3]	= left;
		mVertexData[4]	= top;
		mVertexData[8]	= right;
		mVertexData[9]	= top;
		mVertexData[13] = right;
		mVertexData[14] = bottom;
		mVertexData[18] = left;
		mVertexData[19] = bottom;
	}
}
