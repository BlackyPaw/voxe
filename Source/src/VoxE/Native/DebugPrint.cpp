#include <VoxE/Native/DebugPrint.hpp>
#include <cstdio>

namespace VX
{
	void DebugPrintF(const char* format, va_list argList)
	{
#if defined(VOXE_DEBUG)
		static const vxU32 kMaxCharacters = 5119;
		static vxChar kBuffer[kMaxCharacters + 1];

		vsnprintf(kBuffer, kMaxCharacters, format, argList);
		kBuffer[kMaxCharacters] = '\0';

		NativeDebugOutput(kBuffer);
#endif // VOXE_DEBUG
	}

	void DebugPrintF(const char* format, ...)
	{
#if defined(VOXE_DEBUG)
		va_list argList;
		va_start(argList, format);
		DebugPrintF(format, argList);
		va_end(argList);
#endif // VOXE_DEBUG
	}
}