#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_UNIX)

#include <VoxE/Native/GLContext.hpp>

// X11
#include <X11/Xlib.h>
// GLX
#include <GL/glew.h>
#include <GL/glx.h>

// STL
#include <cstdio>
#include <string>

namespace VX
{
	vxBool glXFBAttribMatch(DisplayHandle dpy, GLXFBConfig config, const vxI32 attrib, const vxI32 value)
	{
		vxI32 _value;
		if(glXGetFBConfigAttrib(dpy, config, attrib, &_value) == 0)
		{
			return (_value == value);
		}
		else
			return false;
	}

	vxBool glXVisualAttribMatch(DisplayHandle dpy, XVisualInfo* visual, vxI32 attrib, const vxI32 value)
	{
		vxI32 _value;
		if(glXGetConfig(dpy, visual, attrib, &_value) == 0)
		{
			return (value == _value);
		}
		else
			return false;
	}

	GLXFBConfig FindBestFBConfigMatch(DisplayHandle dpy, GLXFBConfig* configs, const vxI32 nelements, const GLContextAttributes& attribs)
	{
		vxI32 bestScore = -1;
		GLXFBConfig bestConfig = nullptr;
		for(vxI32 n = 0; n < nelements; ++n)
		{
			if(attribs.mDoublebuffer)
			{
				if(!glXFBAttribMatch(dpy, configs[n], GLX_DOUBLEBUFFER, True))
					continue;
			}
			// For each matching attribute we will give 1 point.
			// The config with the most points will then be the chosen config:
			vxI32 score = 0;
			GLXFBConfig config = configs[n];
			if(glXFBAttribMatch(dpy, config, GLX_BUFFER_SIZE, static_cast<vxI32>(attribs.mColorBits))) score++;
			if(glXFBAttribMatch(dpy, config, GLX_DEPTH_SIZE, static_cast<vxI32>(attribs.mDepthBits))) score++;
			if(glXFBAttribMatch(dpy, config, GLX_STENCIL_SIZE, static_cast<vxI32>(attribs.mStencilBits))) score++;

			if(score > bestScore)
			{
				bestScore = score;
				bestConfig = config;
			}
		}

		return bestConfig;
	}

	XVisualInfo FindBestVisualMatch(DisplayHandle dpy, XVisualInfo* visuals, const vxI32 nelements, const GLContextAttributes& attribs)
	{
		vxI32 bestScore = -1;
		XVisualInfo bestVisual;
		for(vxI32 n = 0; n < nelements; ++n)
		{
			if(attribs.mDoublebuffer)
			{
				if(!glXVisualAttribMatch(dpy, &visuals[n], GLX_DOUBLEBUFFER, True))
					continue;
			}
			vxI32 score = 0;
			XVisualInfo visual = visuals[n];
			if(glXVisualAttribMatch(dpy, &visual, GLX_BUFFER_SIZE, static_cast<vxI32>(attribs.mColorBits))) score++;
			if(glXVisualAttribMatch(dpy, &visual, GLX_DEPTH_SIZE, static_cast<vxI32>(attribs.mDepthBits))) score++;
			if(glXVisualAttribMatch(dpy, &visual, GLX_STENCIL_SIZE, static_cast<vxI32>(attribs.mStencilBits))) score++;

			if(score > bestScore)
			{
				bestScore = score;
				bestVisual = visual;
			}
		}
		return bestVisual;
	}

	GLContext::GLContext() :
		mDisplay(nullptr),
		mScreen(0),
		mReferences(nullptr),
		mDevice(0),
		mContext(nullptr),
		mSwapInterval(nullptr)
	{
	}

	GLContext::GLContext(const GLContext& c) :
		mReferences(nullptr)
	{
		Copy(c);
	}

	GLContext::~GLContext()
	{
		Destroy();
	}

	void* GLContext::FindBestVisual(DisplayHandle dpy, const GLContextAttributes& attribs)
	{
		// Get all visuals:
		vxI32 nelements;
		XVisualInfo* visuals = XGetVisualInfo(dpy, 0, nullptr, &nelements);
		if(visuals != nullptr && nelements > 0)
		{
			XVisualInfo bestVisual = FindBestVisualMatch(dpy, visuals, nelements, attribs);
			XFree(visuals);

			return new XVisualInfo(bestVisual);
		}
		return nullptr;
	}

	vxBool GLContext::Create(const DeviceHandle& dev, const GLContextAttributes& attribs)
	{
		Destroy();

		// Create an internal copy of the settings:
		GLContextAttributes _attribs = attribs;
		DisplayHandle dpy = XOpenDisplay(0);
		vxI32 screen = DefaultScreen(dpy);
		ContextHandle context = nullptr;

		int glxMajor, glxMinor;
		glXQueryVersion(dpy, &glxMajor, &glxMinor);

		// If FB functionality is available:
		if(glxMajor >= 1 && glxMinor >= 3)
		{
			printf("[Native] > GLX 1.3 supported (%i.%i)!\n", glxMajor, glxMinor);
			vxI32 nelements;
			GLXFBConfig* configs = glXGetFBConfigs(dpy, screen, &nelements);
			if(configs != nullptr && nelements > 0)
			{
				printf("[Native] > Found FBConfigs!\n");
				GLXFBConfig bestConfig = FindBestFBConfigMatch(dpy, configs, nelements, _attribs);
				if(bestConfig != nullptr)
				{
					// Try to create a context:
					const GLubyte* name = reinterpret_cast<const GLubyte*>("glXCreateContextAttribsARB");
					PFNGLXCREATECONTEXTATTRIBSARBPROC _glXCreateContextAttribsARB = reinterpret_cast<PFNGLXCREATECONTEXTATTRIBSARBPROC>(glXGetProcAddress(name));
					if(_glXCreateContextAttribsARB != nullptr)
					{
						printf("[Native] > glXCreateContextAttribsARB supported!\n");
						while(_attribs.mMajorVersion >= 3)
						{
							printf("[Native] > Trying version %i.%i...\n", _attribs.mMajorVersion, _attribs.mMinorVersion);
							// We can use the more "modern" method:
							vxI32 contextAttribs[] =
							{
								GLX_CONTEXT_MAJOR_VERSION_ARB, _attribs.mMajorVersion,
								GLX_CONTEXT_MINOR_VERSION_ARB, _attribs.mMinorVersion,
								GLX_CONTEXT_FLAGS_ARB, (_attribs.mDebugContext ? GLX_CONTEXT_DEBUG_BIT_ARB : 0),
								GLX_CONTEXT_PROFILE_MASK_ARB , (_attribs.mCompatibility ? GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB : GLX_CONTEXT_CORE_PROFILE_BIT_ARB),
								None
							};

							context = _glXCreateContextAttribsARB(dpy, bestConfig, 0, True, contextAttribs);

							if(context != nullptr)
							{
								printf("[Native] > Success on glXCreateContextAttribsARB()!\n");
								glXMakeContextCurrent(dpy, dev, dev, context);
								break;
							}
							else
							{
								if(_attribs.mMinorVersion == 0)
								{
									_attribs.mMajorVersion--;
									_attribs.mMinorVersion = 9;
								}
								else
									_attribs.mMinorVersion--;
							}
						}
					}
					else
					{
						// The extension is not supported, but at least we can use the FBConfig functionality:
						printf("[Native] > Using glXCreateNewContext!\n");
						context = glXCreateNewContext(dpy, bestConfig, GLX_RGBA_TYPE, 0, True);
					}
				}
				XFree(configs);
			}
		}
		else
		{
			printf("[Native] > Falling back to <= OpenGL 2.1!\n");
			XWindowAttributes wa;
			if(XGetWindowAttributes(dpy, dev, &wa) == 0)
				return false;

			XVisualInfo tmp, *visuals;
			tmp.screen = screen;
			tmp.visualid = XVisualIDFromVisual(wa.visual);
			int nelements = 0;
			visuals = XGetVisualInfo(dpy, VisualScreenMask | VisualIDMask, &tmp, &nelements);

			context = glXCreateContext(dpy, visuals, 0, True);

			XFree(visuals);
		}

		// If we were still not able to create a context
		// we have to return :(
		if(context == nullptr)
		{
			printf("[Native] > Failed to create a context!\n");
			return false;
		}

		mReferences = new vxU32(1);
		mDisplay = dpy;
		mDevice = dev;
		mContext = context;
		mScreen = screen;

		const GLubyte* name = reinterpret_cast<const GLubyte*>("glXSwapIntervalEXT");
		mSwapInterval = reinterpret_cast<SwapIntervalExtFunc>(glXGetProcAddress(name));

		glewInit();

		return true;
	}

	void GLContext::FullClear()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT | GL_ACCUM_BUFFER_BIT);
	}

	void GLContext::SwapBuffers()
	{
		glXSwapBuffers(mDisplay, mDevice);
	}

	void GLContext::MakeCurrent()
	{
		glXMakeCurrent(mDisplay, mDevice, mContext);
	}

	void GLContext::EnableVSync(const vxBool vsync)
	{
		if(mSwapInterval != nullptr)
		{
			printf("[Native] > GLX_EXT_swap_interval is supported -> Setting swap interval...\n");
			if(vsync)
				mSwapInterval(mDisplay, mDevice, 1);
			else
				mSwapInterval(mDisplay, mDevice, 0);
		}
	}

	vxBool GLContext::IsValid() const
	{
		return (mReferences != nullptr && mContext != nullptr);
	}

	GLContext& GLContext::operator= (const GLContext& c)
	{
		Copy(c);
		return *this;
	}

	void GLContext::Copy(const GLContext& c)
	{
		Destroy();

		mReferences = c.mReferences;
		mDisplay = c.mDisplay;
		mScreen = c.mScreen;
		mDevice = c.mDevice;
		mContext = c.mContext;
		mSwapInterval = c.mSwapInterval;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void GLContext::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				delete mReferences;
				if(mContext != nullptr)
				{
					if(glXGetCurrentContext() == mContext)
					{
						glXMakeCurrent(mDisplay, mDevice, 0);
					}
					glXDestroyContext(mDisplay, mContext);
				}
				if(mDisplay != nullptr)
					XCloseDisplay(mDisplay);
			}
		}

		mReferences = nullptr;
		mDisplay = nullptr;
		mScreen = 0;
		mDevice = 0;
		mContext = nullptr;
		mSwapInterval = nullptr;
	}
}

#endif
