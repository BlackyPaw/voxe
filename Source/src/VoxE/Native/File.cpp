#include <VoxE/Native/File.hpp>
#include <sys/stat.h>

namespace VX
{
	File::File(const vxString& path) :
		mPath(path)
	{
		DetectMetadata();
	}

	File::File(const Path& path) :
		mPath(path)
	{
		DetectMetadata();
	}

	File::~File()
	{
	}

	const Path& File::GetPath() const
	{
		return mPath;
	}

	vxU32 File::GetSize() const
	{
		return mSize;
	}

	void File::DetectMetadata()
	{
		mPath = mPath.Canonicalize();
		if (mPath.Exists())
		{
			struct stat file_stats;
			vxI32 res = stat(mPath.ToString().c_str(), &file_stats);
			mSize = (res == 0 ? file_stats.st_size : 0);
		}
		else
		{
			mSize = 0;
		}
	}
}