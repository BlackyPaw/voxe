#include <VoxE/Native/FileStream.hpp>
#include <VoxE/Native/WindowIcon.hpp>
#include <SOIL/SOIL.h>

namespace VX
{
	WindowIcon::WindowIcon() :
		mReferences(nullptr),
		mWidth(0),
		mHeight(0),
		mPixels(nullptr)
	{
	}

	WindowIcon::WindowIcon(const File& f) :
		mReferences(nullptr),
		mWidth(0),
		mHeight(0),
		mPixels(nullptr)
	{
		LoadFile(f);
	}

	WindowIcon::WindowIcon(const WindowIcon& ico) :
		mReferences(nullptr)
	{
		Copy(ico);
	}

	WindowIcon::~WindowIcon()
	{
		Destroy();
	}

	vxBool WindowIcon::LoadFile(const File& f)
	{
		FileInputStream in;
		in.Open(f);

		in.Seek(STREAM_SEEK_END);
		vxU32 size = in.GetCur();
		in.Seek(STREAM_SEEK_BEGIN);
		
		if (size == 0)
			return false;

		vxU8* buffer = new vxU8[size];
		in.Read(size, buffer);

		in.Close();

		vxU32 width = 0, height = 0;
		vxU8* pixels = SOIL_load_image_from_memory(buffer, size, reinterpret_cast<int*>(&width), reinterpret_cast<int*>(&height), nullptr, SOIL_LOAD_RGBA);

		if (pixels == nullptr)
			return false;

		mReferences = new vxU32(1);
		mWidth = width;
		mHeight = height;
		mPixels = pixels;

		return true;
	}

	WindowIcon& WindowIcon::operator= (const WindowIcon& ico)
	{
		Copy(ico);
		return *this;
	}

	void WindowIcon::Copy(const WindowIcon& ico)
	{
		Destroy();

		mReferences = ico.mReferences;
		mWidth = ico.mWidth;
		mHeight = ico.mHeight;
		mPixels = ico.mPixels;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void WindowIcon::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				SOIL_free_image_data(mPixels);
			}
		}

		mReferences = nullptr;
		mWidth = 0;
		mHeight = 0;
		mPixels = nullptr;
	}
}