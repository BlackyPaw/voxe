#include <VoxE/Native/WindowSizeEvent.hpp>

namespace VX
{
	WindowSizeEvent::WindowSizeEvent(const Vector2ui& size) :
		Event(GetEventID()),
		mSize(size)
	{
	}

	const Vector2ui& WindowSizeEvent::GetSize() const
	{
		return mSize;
	}
}