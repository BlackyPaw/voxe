/*
 * Time.cpp
 *
 *  Created on: 11.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/Assert.hpp>
#include <VoxE/Native/Time.hpp>

namespace VX
{
	Time::Time(const vxU64 microseconds) :
		mMicroseconds(microseconds)
	{
	}

	vxU64 Time::Microseconds() const
	{
		return mMicroseconds;
	}

	vxU32 Time::Milliseconds() const
	{
		return static_cast<vxU32>(mMicroseconds * 0.001);
	}

	vxF64 Time::Secondsd() const
	{
		return (static_cast<vxF64>(mMicroseconds) * 0.000001);
	}

	vxF32 Time::Seconds() const
	{
		return (static_cast<vxF32>(mMicroseconds) * 0.000001f);
	}

	Time& Time::operator+= (const Time& t)
	{
		mMicroseconds += t.mMicroseconds;
		return *this;
	}

	Time& Time::operator-= (const Time& t)
	{
		mMicroseconds -= t.mMicroseconds;
		return *this;
	}

	Time& Time::operator*= (const Time& t)
	{
		mMicroseconds *= t.mMicroseconds;
		return *this;
	}

	Time& Time::operator/= (const Time& t)
	{
		ASSERT(t.mMicroseconds != 0);
		mMicroseconds /= t.mMicroseconds;
		return *this;
	}

	Time operator+ (const Time& a, const Time& b)
	{
		Time tmp(a);
		tmp += b;
		return tmp;
	}

	Time operator- (const Time& a, const Time& b)
	{
		Time tmp(a);
		tmp -= b;
		return tmp;
	}

	Time operator* (const Time& a, const Time& b)
	{
		Time tmp(a);
		tmp *= b;
		return tmp;
	}

	Time operator/ (const Time& a, const Time& b)
	{
		Time tmp(a);
		tmp /= b;
		return tmp;
	}
}


