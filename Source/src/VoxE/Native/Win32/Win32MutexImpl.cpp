#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)

#include <VoxE/Native/Mutex.hpp>
#include <windows.h>

namespace VX
{
	Mutex::Mutex()
	{
		mHandle = CreateMutex(0, false, nullptr);
	}

	Mutex::~Mutex()
	{
		CloseHandle(mHandle);
	}

	void Mutex::Lock()
	{
		WaitForSingleObject(mHandle, INFINITE);
	}

	vxBool Mutex::TryLock()
	{
		return (WaitForSingleObject(mHandle, 0) != WAIT_TIMEOUT);
	}

	void Mutex::Release()
	{
		ReleaseMutex(mHandle);
	}
}

#endif