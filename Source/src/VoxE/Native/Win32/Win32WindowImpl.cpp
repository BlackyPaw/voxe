#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)

#include <VoxE/HID/KeyDownEvent.hpp>
#include <VoxE/HID/KeyUpEvent.hpp>
#include <VoxE/HID/MouseDownEvent.hpp>
#include <VoxE/HID/MouseUpEvent.hpp>
#include <VoxE/HID/MouseMoveEvent.hpp>

#include <VoxE/Core/EventManager.hpp>
#include <VoxE/Native/GLContext.hpp>
#include <VoxE/Native/Window.hpp>
#include <VoxE/Native/WindowSizeEvent.hpp>
#include <windows.h>
#include <windowsx.h>

namespace VX
{
	static const vxU32 kNumWindows = 0;
	static const vxCharPtr kClassName = "VoxE-WndClass";
	static const vxCharPtr kDefaultTitle = "VoxE";

	KeyboardKeys MapVirtualKeyToEnum(WPARAM wParam, LPARAM lParam);

	Window::Window() :
		mHandle(0),
		mIcon(0),
		mIsOpen(false),
		mReferences(nullptr),
		mIsSizing(false)
	{
	}

	Window::Window(const Window& window)
	{
		Copy(window);
	}

	Window::~Window()
	{
		Destroy();
	}

	void Window::Create(const GLContextAttributes& attribs)
	{
		if (kNumWindows == 0)
			RegisterWindowClass();

		mReferences = new vxU32(1);
		mHandle = CreateWindow(kClassName, kDefaultTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 0, 0, GetModuleHandle(0), nullptr);
		SetWindowLongPtr(mHandle, GWLP_USERDATA, reinterpret_cast<LONG>(this));
		mIsOpen = true;
		mDevice = GetDC(mHandle);

		ShowWindow(mHandle, SW_HIDE);
		UpdateWindow(mHandle);

		mLastSize = GetSize();
		memset(mKeys, 0, 0xFF * sizeof(vxBool));

		mContext.Create(mDevice, attribs);
	}

	void Window::Update()
	{
		MSG msg;
		while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE) > 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	
	void Window::Show(const vxBool show)
	{
		ShowWindow(mHandle, (show ? SW_SHOW : SW_HIDE));
	}

	Vector2i Window::GetPosition() const
	{
		RECT rect;
		GetWindowRect(mHandle, &rect);
		return Vector2i(rect.left, rect.top);
	}

	Vector2ui Window::GetSize() const
	{
		RECT rect;
		GetClientRect(mHandle, &rect);
		return Vector2ui(static_cast<vxU32>(rect.right - rect.left), static_cast<vxU32>(rect.bottom - rect.top));
	}

	void Window::SetPosition(const Vector2i& pos)
	{
		SetWindowPos(mHandle, 0, pos.x, pos.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
	}

	void Window::SetSize(const Vector2ui& size)
	{
		RECT rect;
		rect.left = 0;
		rect.top = 0;
		rect.right = static_cast<int>(size.x);
		rect.bottom = static_cast<int>(size.y);
		AdjustWindowRect(&rect, GetWindowLong(mHandle, GWL_STYLE), false);
		SetWindowPos(mHandle, 0, 0, 0, (rect.right - rect.left), (rect.bottom - rect.top), SWP_NOZORDER | SWP_NOMOVE);
	}

	void Window::SetIcon(const WindowIcon& ico)
	{
		SetIcon(ico.mWidth, ico.mHeight, ico.mPixels);
	}

	void Window::SetIcon(const vxU32 width, const vxU32 height, const vxU8* pixels)
	{
		if (mIcon != 0)
			DestroyIcon(mIcon);

		if (width == 0 || height == 0 || pixels == nullptr)
			return;

		// Convert to BGRA:
		vxU8* conv = new vxU8[width * height * 4];
		// - Swap the RGB channels:
		for (vxU32 i = 0; i < width * height; ++i)
		{
			conv[i * 4 + 0] = pixels[i * 4 + 2];
			conv[i * 4 + 1] = pixels[i * 4 + 1];
			conv[i * 4 + 2] = pixels[i * 4 + 0];
			conv[i * 4 + 3] = pixels[i * 4 + 3];
		}

		// Create the icon:
		mIcon = CreateIcon(GetModuleHandle(0), width, height, 1, 32, nullptr, conv);

		delete[] conv;

		// Update the icon:
		if (mIcon != 0)
		{
			SendMessage(mHandle, WM_SETICON, ICON_BIG, reinterpret_cast<LPARAM>(mIcon));
			SendMessage(mHandle, WM_SETICON, ICON_SMALL, reinterpret_cast<LPARAM>(mIcon));
		}
	}

	void Window::ShowCursor(const vxBool show)
	{
		::ShowCursor(show);
	}

	void Window::SetTitle(const std::string& title)
	{
		SetWindowText(mHandle, title.c_str());
	}

	vxBool Window::IsOpen() const
	{
		return mIsOpen;
	}

	const GLContext& Window::GetGLContext() const
	{
		return mContext;
	}

	/*
	 * DEPRECATED
	 * This functionality is considered deprecated as it's not currently supported on all target platforms and always looks a bit buggs :s
	 */
	/*
	vxBool Window::SetFullscreen(const FullscreenDesc& desc)
	{
		if (desc.mFullscreen)
		{
			DEVMODE dmScreenSettings;
			ZeroMemory(&dmScreenSettings, sizeof(dmScreenSettings));
			dmScreenSettings.dmSize = sizeof(dmScreenSettings);
			dmScreenSettings.dmPelsWidth = desc.mResolution.x;
			dmScreenSettings.dmPelsHeight = desc.mResolution.y;
			dmScreenSettings.dmBitsPerPel = desc.mBitsPerPixel;
			dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
			if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
				return false;

			SetWindowLong(mHandle, GWL_STYLE, WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
			SetWindowLong(mHandle, GWL_EXSTYLE, WS_EX_APPWINDOW);

			SetWindowPos(mHandle, HWND_TOP, 0, 0, desc.mResolution.x, desc.mResolution.y, SWP_FRAMECHANGED);
			ShowWindow(mHandle, SW_SHOW);
			return true;
		}
		else
		{
			if (ChangeDisplaySettings(0, 0) != DISP_CHANGE_SUCCESSFUL)
				return false;

			SetWindowLong(mHandle, GWL_STYLE, WS_OVERLAPPEDWINDOW);
			SetWindowLong(mHandle, GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_WINDOWEDGE);
			ShowWindow(mHandle, SW_SHOW);
			return true;
		}
	}

	void Window::SetWindowed()
	{
		FullscreenDesc desc;
		desc.mFullscreen = false;
		SetFullscreen(desc);
	}
	*/

	Window& Window::operator= (const Window& window)
	{
		Copy(window);
		return *this;
	}

	void Window::Destroy()
	{
		if (mReferences)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				DestroyWindow(mHandle);
				if (mIcon)
					DestroyIcon(mIcon);
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mIcon = 0;
		mIsOpen = false;
#if defined(VOXE_PLATFORM_WINDOWS)
		mLastSize = Vector2ui(0, 0);
		mIsSizing = false;
#endif
	}

	void Window::Copy(const Window& window)
	{
		Destroy();

		mReferences = window.mReferences;
		mHandle = window.mHandle;
		mIcon = window.mIcon;
		mIsOpen = window.mIsOpen;
#if defined(VOXE_PLATFORM_WINDOWS)
		mLastSize = window.mLastSize;
		mIsSizing = window.mIsSizing;
#endif

		if (mReferences)
			(*mReferences)++;
	}

	LRESULT CALLBACK Window::WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		Window* pWin = reinterpret_cast<Window*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
		if (pWin != nullptr)
		{
			switch (msg)
			{
				//////////////////////////////////////////////////////////////
				/// Internals
				//////////////////////////////////////////////////////////////
				case WM_CLOSE:
				{
					pWin->mIsOpen = false;
				}
				break;
				
				//////////////////////////////////////////////////////////////
				// HID
				//////////////////////////////////////////////////////////////
				case WM_KEYDOWN:
				{
					if (!pWin->mKeys[wParam])
					{
						KeyDownEvent* pEvent = new KeyDownEvent(MapVirtualKeyToEnum(wParam, lParam));
						EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
						pWin->mKeys[wParam] = true;
					}
				}
				break;
				case WM_KEYUP:
				{
					KeyUpEvent* pEvent = new KeyUpEvent(MapVirtualKeyToEnum(wParam, lParam));
					EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
					pWin->mKeys[wParam] = false;
				}
				break;
				case WM_LBUTTONDOWN:
				{
					MouseDownEvent* pEvent = new MouseDownEvent(MOUSE_BUTTON_LEFT, Vector2i(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
					EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
				}
				break;
				case WM_RBUTTONDOWN:
				{
					MouseDownEvent* pEvent = new MouseDownEvent(MOUSE_BUTTON_RIGHT, Vector2i(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
					EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
				}
				break;
				case WM_MBUTTONDOWN:
				{
					MouseDownEvent* pEvent = new MouseDownEvent(MOUSE_BUTTON_MIDDLE, Vector2i(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
					EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
				}
				break;
				case WM_LBUTTONUP:
				{
					MouseUpEvent* pEvent = new MouseUpEvent(MOUSE_BUTTON_LEFT, Vector2i(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
					EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
				}
				break;
				case WM_RBUTTONUP:
				{
					MouseUpEvent* pEvent = new MouseUpEvent(MOUSE_BUTTON_RIGHT, Vector2i(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
					EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
				}
				break;
				case WM_MBUTTONUP:
				{
					MouseUpEvent* pEvent = new MouseUpEvent(MOUSE_BUTTON_MIDDLE, Vector2i(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
					EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
				}
				break;
				case WM_MOUSEMOVE:
				{
					Vector2i coords;
					coords.x = static_cast<vxI32>(GET_X_LPARAM(lParam));
					coords.y = static_cast<vxI32>(GET_Y_LPARAM(lParam));
					MouseMoveEvent* pEvent = new MouseMoveEvent(coords);
					EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
				}

				//////////////////////////////////////////////////////////////
				/// Sizing
				//////////////////////////////////////////////////////////////
				case WM_SIZE:
				{
					if (!pWin->mIsSizing && wParam != SIZE_MINIMIZED && pWin->mLastSize != pWin->GetSize())
					{
						pWin->mLastSize = pWin->GetSize();
						WindowSizeEvent* pEvent = new WindowSizeEvent(pWin->mLastSize);
						EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
					}
				}
				break;
				case WM_ENTERSIZEMOVE:
				{
					pWin->mIsSizing = true;
				}
				break;
				case WM_EXITSIZEMOVE:
				{
					if (pWin->mLastSize != pWin->GetSize())
					{
						pWin->mLastSize = pWin->GetSize();
						WindowSizeEvent* pEvent = new WindowSizeEvent(pWin->mLastSize);
						EventManager::GetSharedInstance()->QueueEvent(EventPtr(pEvent));
					}
					pWin->mIsSizing = false;
				}
				break;
			}
		}
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	void Window::RegisterWindowClass()
	{
		WNDCLASSEX wc;
		wc.cbClsExtra = 0;
		wc.cbSize = sizeof(wc);
		wc.cbWndExtra = 0;
		wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wc.hCursor = LoadCursor(0, IDC_ARROW);
		wc.hIcon = LoadIcon(0, IDI_APPLICATION);
		wc.hIconSm = LoadIcon(0, IDI_APPLICATION);
		wc.hInstance = GetModuleHandle(0);
		wc.lpfnWndProc = Window::WindowProc;
		wc.lpszClassName = kClassName;
		wc.lpszMenuName = 0;
		wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

		RegisterClassEx(&wc);
	}

	KeyboardKeys MapVirtualKeyToEnum(WPARAM wParam, LPARAM lParam)
	{
		// A-Z
		if (wParam > 0x40 && wParam < 0x5B)
			return static_cast<KeyboardKeys>(KEYBOARD_KEY_A + (wParam - 0x41));

		// 0-9
		if (wParam > 0x2F && wParam < 0x3A)
			return static_cast<KeyboardKeys>(KEYBOARD_KEY_0 + (wParam - 0x30));

		// Num0-Num9
		if (wParam > 0x5F && wParam < 0x6A)
			return static_cast<KeyboardKeys>(KEYBOARD_KEY_NUM_0 + (wParam - 0x60));

		// F1-F12
		if (wParam > 0x6F && wParam < 0x7C)
			return static_cast<KeyboardKeys>(KEYBOARD_KEY_F1 + (wParam - 0x70));

		// Special keys:
		switch (wParam)
		{
			case VK_ESCAPE:		return KEYBOARD_KEY_ESC;
			case VK_SPACE:		return KEYBOARD_KEY_SPACE;
			case VK_BACK:		return KEYBOARD_KEY_BACKSPACE;
			case VK_RETURN:		return KEYBOARD_KEY_ENTER;
			case VK_TAB:		return KEYBOARD_KEY_TAB;
			case VK_LEFT:		return KEYBOARD_KEY_LEFT;
			case VK_UP:			return KEYBOARD_KEY_UP;
			case VK_RIGHT:		return KEYBOARD_KEY_RIGHT;
			case VK_DOWN:		return KEYBOARD_KEY_DOWN;
			case VK_CLEAR:		return KEYBOARD_KEY_CLEAR;
			case VK_PAUSE:		return KEYBOARD_KEY_PAUSE;
			case VK_CAPITAL:	return KEYBOARD_KEY_CAPS_LOCK;
			case VK_PRIOR:		return KEYBOARD_KEY_PAGE_UP;
			case VK_NEXT:		return KEYBOARD_KEY_PAGE_DOWN;
			case VK_END:		return KEYBOARD_KEY_END;
			case VK_HOME:		return KEYBOARD_KEY_HOME;
			case VK_SELECT:		return KEYBOARD_KEY_SELECT;
			case VK_PRINT:		return KEYBOARD_KEY_PRINT;
			case VK_EXECUTE:	return KEYBOARD_KEY_EXECUTE;
			case VK_SNAPSHOT:	return KEYBOARD_KEY_PRINT_SCREEN;
			case VK_INSERT:		return KEYBOARD_KEY_INSERT;
			case VK_DELETE:		return KEYBOARD_KEY_DELETE;
			case VK_HELP:		return KEYBOARD_KEY_HELP;
			case VK_MULTIPLY:	return KEYBOARD_KEY_MULTIPLY;
			case VK_ADD:		return KEYBOARD_KEY_ADD;
			case VK_SUBTRACT:	return KEYBOARD_KEY_SUBTRACT;
			case VK_DIVIDE:		return KEYBOARD_KEY_DIVIDE;
			case VK_SEPARATOR:	return KEYBOARD_KEY_SEPARATOR;
			case VK_DECIMAL:	return KEYBOARD_KEY_DECIMAL;
			case VK_NUMLOCK:	return KEYBOARD_KEY_NUM_LOCK;
			case VK_SCROLL:		return KEYBOARD_KEY_SCROLL_LOCK;
			case VK_OEM_PLUS:	return KEYBOARD_KEY_PLUS;
			case VK_OEM_COMMA:	return KEYBOARD_KEY_COMMA;
			case VK_OEM_MINUS:	return KEYBOARD_KEY_MINUS;
			case VK_OEM_PERIOD:	return KEYBOARD_KEY_PERIOD;
			case VK_OEM_2:		return KEYBOARD_KEY_SLASH;
			case VK_OEM_3:		return KEYBOARD_KEY_TILDE;
			case VK_OEM_4:		return KEYBOARD_KEY_OPEN_BRACKET;
			case VK_OEM_6:		return KEYBOARD_KEY_CLOSE_BRACKET;
			case VK_OEM_5:		return KEYBOARD_KEY_BACKSLASH;
			case VK_OEM_7:		return KEYBOARD_KEY_QUOTES;
		}

		return KEYBOARD_NUM_KEYS;
	}
}

#endif