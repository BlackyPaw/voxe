#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)

#include <VoxE/Native/Thread.hpp>
#include <windows.h>

namespace VX
{
	DWORD WINAPI BaseThreadEntry(LPVOID lpParameter);

	Thread::Thread()
	{
		mHandle = CreateThread(0, 0, BaseThreadEntry, this, CREATE_SUSPENDED, 0);
	}

	Thread::~Thread()
	{
		CloseHandle(mHandle);
	}

	void Thread::Start()
	{
		ResumeThread(mHandle);
	}

	void Thread::Wait()
	{
		WaitForSingleObject(mHandle, INFINITE);
	}

	vxU32 Thread::Run()
	{
		return 0;
	}

	void Thread::Terminate()
	{
		TerminateThread(mHandle, kInvalidReturnCode);
	}

	vxBool Thread::IsRunning() const
	{
		vxU32 exitCode = kInvalidReturnCode;
		GetExitCodeThread(mHandle, reinterpret_cast<LPDWORD>(&exitCode));
		return (exitCode == STILL_ACTIVE);
	}

	vxU32 Thread::GetExitCode() const
	{
		vxU32 exitCode = kInvalidReturnCode;
		GetExitCodeThread(mHandle, reinterpret_cast<LPDWORD>(&exitCode));
		return (exitCode == STILL_ACTIVE ? kInvalidReturnCode : exitCode);
	}

	DWORD WINAPI BaseThreadEntry(LPVOID lpParameter)
	{
		Thread* pThread = reinterpret_cast<Thread*>(lpParameter);
		if (pThread == nullptr)
			return kInvalidReturnCode;

		// Work around the STILL_ACTIVE problem:
		vxU32 code = pThread->Run();
		if (code == 259)
			code = kInvalidReturnCode;

		return code;
	}
}

#endif