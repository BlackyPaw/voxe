#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)

#include <VoxE/Native/DebugPrint.hpp>
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif // WIN32_LEAN_AND_MEAN
#include <windows.h>

namespace VX
{
	void NativeDebugOutput(const vxChar* msg)
	{
#if defined(VOXE_DEBUG)
		OutputDebugString(msg);
#endif // VOXE_DEBUG
	}
}

#endif // VOXE_PLATFORM_WINDOWS