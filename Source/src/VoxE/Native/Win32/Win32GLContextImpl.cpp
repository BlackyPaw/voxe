#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)

#include <VoxE/Native/DebugPrint.hpp>
#include <VoxE/Native/GLContext.hpp>
#include <string>
#include <windows.h>
#include <GL/glew.h>

#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
#define WGL_CONTEXT_FLAGS_ARB 0x2094
#define WGL_CONTEXT_PROFILE_MASK_ARB 0x9126

#define WGL_CONTEXT_DEBUG_BIT_ARB 0x0001

#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
#define WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB 0x00000002

namespace VX
{
	GLContext::GLContext() :
		mReferences(nullptr),
		mDevice(0),
		mContext(0),
		mSwapInterval(nullptr)
	{
	}

	GLContext::GLContext(const GLContext& context) :
		mReferences(nullptr),
		mDevice(0),
		mContext(0),
		mSwapInterval(nullptr)
	{
		Copy(context);
	}

	GLContext::~GLContext()
	{
		Destroy();
	}

	vxBool GLContext::Create(const DeviceHandle& dev, const GLContextAttributes& attribs)
	{
		Destroy();

		PIXELFORMATDESCRIPTOR pfd;
		pfd.nSize = sizeof(pfd);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | (attribs.mDoublebuffer ? PFD_DOUBLEBUFFER : 0);
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = attribs.mColorBits; // <- Default number of color bits.
		pfd.cRedBits = pfd.cRedShift = 0;
		pfd.cGreenBits = pfd.cGreenShift = 0;
		pfd.cBlueBits = pfd.cBlueShift = 0;
		pfd.cAlphaBits = pfd.cAlphaShift = 0;
		pfd.cAccumBits = pfd.cAccumRedBits = pfd.cAccumGreenBits = pfd.cAccumBlueBits = pfd.cAccumAlphaBits = 0;
		pfd.cDepthBits = attribs.mDepthBits; // <- Default number of depth bits.
		pfd.cStencilBits = attribs.mStencilBits; // <- Default number of stencil bits.
		pfd.cAuxBuffers = 0;
		pfd.iLayerType = PFD_MAIN_PLANE;
		pfd.bReserved = 0;
		pfd.dwLayerMask = 0;
		pfd.dwVisibleMask = 0;
		pfd.dwDamageMask = 0;

		vxI32 iFormat = ChoosePixelFormat(dev, &pfd);
		if (iFormat == 0)
			return false;

		SetPixelFormat(dev, iFormat, &pfd);

		DebugPrintF("[Native] > Creating dummy context...\n");
		ContextHandle dummy = wglCreateContext(dev);
		if (dummy == nullptr)
			return false;

		wglMakeCurrent(dev, dummy);

		GetExtensionsStringFunc wglGetExtensionString = (GetExtensionsStringFunc)wglGetProcAddress("wglGetExtensionsStringARB");
		SwapIntervalExtFunc wglSwapIntervalEXT = nullptr;
		CreateContextAttribsArbFunc wglCreateContextAttribsARB = nullptr;
		if (wglGetExtensionString != nullptr)
		{
			std::string extensionsString = wglGetExtensionString(mDevice);
			for (vxU32 i = 0; i < extensionsString.length(); ++i)
			{
				std::string extensionName = "";
				while (i < extensionsString.length() && !isspace(extensionsString[i]))
					extensionName += extensionsString[i++];

				if (extensionName == "WGL_EXT_swap_control")
				{
					DebugPrintF("[Native] > Found WGL extension WGL_EXT_swap_control!\n");
					wglSwapIntervalEXT = (SwapIntervalExtFunc)wglGetProcAddress("wglSwapIntervalEXT");
				}
				else if (extensionName == "WGL_ARB_create_context" || extensionName == "WGL_ARB_create_context_profile")
				{
					wglCreateContextAttribsARB = (CreateContextAttribsArbFunc)wglGetProcAddress("wglCreateContextAttribsARB");
					DebugPrintF("[Native] > Found WGL extension WGL_ARB_create_context (_profile)!\n");
				}
			}
		}

		// Check if we can use a newer OpenGL context creation scheme:
		if (wglCreateContextAttribsARB != nullptr)
		{
			GLContextAttributes _attribs = attribs;
			ContextHandle context = nullptr;
			while (_attribs.mMajorVersion >= 3)
			{
				DebugPrintF("[Native] > Trying OpenGL version %i.%i!\n", _attribs.mMajorVersion, _attribs.mMinorVersion);
				int contextAttribs[] =
				{
					WGL_CONTEXT_MAJOR_VERSION_ARB, _attribs.mMajorVersion,
					WGL_CONTEXT_MINOR_VERSION_ARB, _attribs.mMinorVersion,
					WGL_CONTEXT_FLAGS_ARB, (_attribs.mDebugContext ? WGL_CONTEXT_DEBUG_BIT_ARB : 0),
					WGL_CONTEXT_PROFILE_MASK_ARB, (_attribs.mCompatibility ? WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB : WGL_CONTEXT_CORE_PROFILE_BIT_ARB),
					0
				};

				context = wglCreateContextAttribsARB(dev, nullptr, contextAttribs);

				if (context != nullptr)
				{
					DebugPrintF("[Native] > Successfully created OpenGL context using wglCreateCreateContextAttribsARB (version=%i.%i)!\n", _attribs.mMajorVersion, _attribs.mMinorVersion);
					mReferences = new vxU32(1);
					mDevice = dev;
					mContext = context;
					mSwapInterval = wglSwapIntervalEXT;

					DebugPrintF("[Native] > Deleting dummy context...\n");
					// Don't forget to delete the dummy context:
					// (Actually i've nearly forgotten it... :s )
					wglMakeCurrent(dev, 0);
					wglDeleteContext(dummy);

					// And just for doing the same as with dummy contexts:
					// Make our real context current, too:
					wglMakeCurrent(dev, context);
					break;
				}
				else
				{
					if (_attribs.mMinorVersion == 0)
					{
						_attribs.mMajorVersion--;
						_attribs.mMinorVersion = 9;
					}
					else
						_attribs.mMinorVersion--;
				}
			}
		}

		// If we've still not got a valid context, simply use our dummy:
		if (mContext == nullptr)
		{
			DebugPrintF("[Native] > Using fallback technique: Now using dummy context created on the legacy way!\n");
			mReferences = new vxU32(1);
			mDevice = dev;
			mContext = dummy;
			mSwapInterval = wglSwapIntervalEXT;
		}

		// We should now have a valid context :) (dummy contexts are checked for validity right after their creation!)

		if (glewInit() != GLEW_OK)
			return false;

		// Now initialize the default OpenGL states:
		glEnable(GL_TEXTURE_2D);
		glDepthFunc(GL_LEQUAL);
		glClearDepth(1.0f);

		return true;
	}

	void GLContext::FullClear()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT | GL_ACCUM_BUFFER_BIT);
	}

	void GLContext::SwapBuffers()
	{
		::SwapBuffers(mDevice);
	}

	void GLContext::MakeCurrent()
	{
		wglMakeCurrent(mDevice, mContext);
	}

	void GLContext::EnableVSync(const vxBool vsync)
	{
		if (mSwapInterval != nullptr)
			mSwapInterval((vsync ? 1 : 0));
	}

	vxBool GLContext::IsValid() const
	{
		return (mDevice != 0 && mContext != 0);
	}

	GLContext& GLContext::operator= (const GLContext& context)
	{
		Copy(context);
		return *this;
	}

	void GLContext::Copy(const GLContext& context)
	{
		Destroy();

		mReferences = context.mReferences;
		mDevice = context.mDevice;
		mContext = context.mContext;
		mSwapInterval = context.mSwapInterval;

		if (mReferences)
			(*mReferences)++;
	}

	void GLContext::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				wglMakeCurrent(mDevice, 0);
				wglDeleteContext(mContext);
			}
		}

		mReferences = nullptr;
		mDevice = 0;
		mContext = 0;
		mSwapInterval = nullptr;
	}
}

#endif