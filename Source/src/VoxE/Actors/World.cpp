/*
 * World.cpp
 *
 *  Created on: 30.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Actors/World.hpp>
#include <VoxE/Renderer/Renderer.hpp>

#include <algorithm>

namespace VX
{
	World::World()
	{
		mSceneNodePool.Create(sizeof(SceneNode), 2048);
		mActorPool.Create(sizeof(Actor), 2048);

		mScene.GetCamera().Orthographic(0.0f, 500.0f, 500.0f, 0.0f, -1.0f, 1.0f);
	}

	World::~World()
	{
		for(auto it = mActors.begin(); it != mActors.end(); ++it)
		{
			DestroyActor((*it));
		}
	}

	void World::Update(const vxF32 delta)
	{
		for(auto it = mActors.begin(); it != mActors.end(); ++it)
		{
			(*it)->Update(delta);
		}
	}

	void World::Draw()
	{
		mScene.Draw();
	}

	Actor* World::NewActor()
	{
		// TODO:
		// 	- Create a sophisticated memory allocation strategy here!!!!!
		void* nodemem = mSceneNodePool.Allocate();
		void* actormem = mActorPool.Allocate();
		if(nodemem == nullptr || actormem == nullptr)
			return nullptr;

		SceneNode* node = new(nodemem) SceneNode(&mScene);
		mScene.AddNode(node);
		Actor* actor = new(actormem) Actor(this, node);
		mActors.push_back(actor);
		return actor;
	}

	void World::DestroyActor(Actor* actor)
	{
		SceneNode* node = actor->mSceneNode;
		auto find = std::find(mActors.begin(), mActors.end(), actor);
		if(find != mActors.end())
			mActors.erase(find);
		mActorPool.Free(actor);
		mScene.RemoveNode(node);
		mSceneNodePool.Free(node);
	}
}
