/*
 * SpriteActorComponent.cpp
 *
 *  Created on: 31.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Actors/SpriteRenderComponent.hpp>

#include <VoxE/Native/DebugPrint.hpp>

namespace VX
{
	SpriteRenderComponent::SpriteRenderComponent() :
		ActorComponent(GetComponentID())
	{
	}

	void SpriteRenderComponent::SetTexture(const ResourceHandle& t)
	{
		mSprite.SetTexture(t);
	}

	void SpriteRenderComponent::SetShader(const ResourceHandle& s)
	{
		mSprite.SetShader(s);
	}

	void SpriteRenderComponent::SetSubset(const IntRect& r)
	{
		mSprite.SetSubset(r);
	}

	const ResourceHandle& SpriteRenderComponent::GetTexture() const
	{
		return mSprite.GetTexture();
	}

	const ResourceHandle& SpriteRenderComponent::GetShader() const
	{
		return mSprite.GetShader();
	}

	const IntRect& SpriteRenderComponent::GetSubset() const
	{
		return mSprite.GetSubset();
	}

	void SpriteRenderComponent::Update(const vxF32 delta)
	{
	}

	void SpriteRenderComponent::Draw()
	{
		mSprite.Draw();
	}
}
