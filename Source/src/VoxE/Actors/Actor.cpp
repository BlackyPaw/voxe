/*
 * Actor.cpp
 *
 *  Created on: 30.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Actors/Actor.hpp>
#include <VoxE/Actors/World.hpp>

#include <VoxE/Native/DebugPrint.hpp>
#include <algorithm>

namespace VX
{
	Actor::Actor(World* world, SceneNode* node) :
		mWorld(world),
		mSceneNode(node)
	{
		mSceneNode->SetDrawCallback(DrawCallback(this, &Actor::Draw));
	}

	Actor::~Actor()
	{
		// Avoid calls into freed memory:
		// (create empty callback which will not be called):
		mSceneNode->SetDrawCallback(DrawCallback());
		for(auto it = mComponents.begin(); it != mComponents.end(); ++it)
		{
			delete (*it);
		}
	}

	void Actor::AddComponent(ActorComponent* com)
	{
		mComponents.push_back(com);
	}

	void Actor::RemoveComponent(ActorComponent* com)
	{
		auto find = std::find(mComponents.begin(), mComponents.end(), com);
		if(find != mComponents.end())
			mComponents.erase(find);
	}

	void Actor::Update(const vxF32 delta)
	{
		for(auto it = mComponents.begin(); it != mComponents.end(); ++it)
		{
			(*it)->Update(delta);
		}
	}

	void Actor::Draw()
	{
		for(auto it = mComponents.begin(); it != mComponents.end(); ++it)
		{
			(*it)->Draw();
		}
	}

	//////////////////////////////////////////////////////////////
	// ITransformable
	//////////////////////////////////////////////////////////////
	void Actor::Translate(const float x, const float y, const float z)
	{
		mSceneNode->Translate(Vector3f(x, y, z));
	}
	void Actor::Translate(const Vector3f& trans)
	{
		mSceneNode->Translate(trans);
	}
	void Actor::Rotate(const float pitch, const float yaw, const float roll)
	{
		mSceneNode->Rotate(Vector3f(pitch, yaw, roll));
	}
	void Actor::Rotate(const Vector3f& rot)
	{
		mSceneNode->Rotate(rot);
	}
	void Actor::Scale(const float x, const float y, const float z)
	{
		mSceneNode->Scale(Vector3f(x, y, z));
	}
	void Actor::Scale(const Vector3f& scale)
	{
		mSceneNode->Scale(scale);
	}

	void Actor::SetPosition(const float x, const float y, const float z)
	{
		mSceneNode->SetPosition(Vector3f(x, y, z));
	}
	void Actor::SetPosition(const Vector3f& pos)
	{
		mSceneNode->SetPosition(pos);
	}
	void Actor::SetRotation(const float pitch, const float yaw, const float roll)
	{
		mSceneNode->SetRotation(Vector3f(pitch, yaw, roll));
	}
	void Actor::SetRotation(const Vector3f& rot)
	{
		mSceneNode->SetRotation(rot);
	}
	void Actor::SetRotation(const Quaternion& rot)
	{
		mSceneNode->SetRotation(rot);
	}
	void Actor::SetScale(const float x, const float y, const float z)
	{
		mSceneNode->SetScale(Vector3f(x, y, z));
	}
	void Actor::SetScale(const Vector3f& scale)
	{
		mSceneNode->SetScale(scale);
	}

	const Vector3f& Actor::GetPosition() const
	{
		return mSceneNode->GetPosition();
	}
	const Quaternion& Actor::GetRotation() const
	{
		return mSceneNode->GetRotation();
	}
	const Vector3f Actor::GetEulerAngles() const
	{
		return mSceneNode->GetEulerAngles();
	}
	const Vector3f& Actor::GetScale() const
	{
		return mSceneNode->GetScale();
	}

	FloatRect Actor::TransformRect(const FloatRect& rect) const
	{
		return mSceneNode->TransformRect(rect);
	}

	const Matrix4& Actor::GetTransform() const
	{
		return mSceneNode->GetTransform();
	}
}
