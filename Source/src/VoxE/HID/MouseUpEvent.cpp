#include <VoxE/HID/MouseUpEvent.hpp>

namespace VX
{
	MouseUpEvent::MouseUpEvent(const MouseButtons& button, const Vector2i& coords) :
		Event(GetEventID()),
		mButton(button),
		mCoords(coords)
	{
	}

	const MouseButtons& MouseUpEvent::GetButton() const
	{
		return mButton;
	}

	const Vector2i& MouseUpEvent::GetCoords() const
	{
		return mCoords;
	}
}