#include <VoxE/Core/EventManager.hpp>
#include <VoxE/HID/Keyboard.hpp>
#include <VoxE/HID/KeyDownEvent.hpp>
#include <VoxE/HID/KeyUpEvent.hpp>
#include <VoxE/Native/Assert.hpp>

namespace VX
{
	Keyboard::Keyboard()
	{
		ResetKeys();

		EventManager::GetSharedInstance()->RegisterListener(KeyDownEvent::GetEventID(), EventListener(this, &Keyboard::OnKeyDown));
		EventManager::GetSharedInstance()->RegisterListener(KeyUpEvent::GetEventID(), EventListener(this, &Keyboard::OnKeyUp));
	}

	Keyboard::~Keyboard()
	{
		EventManager::GetSharedInstance()->UnRegisterListener(KeyDownEvent::GetEventID(), EventListener(this, &Keyboard::OnKeyDown));
		EventManager::GetSharedInstance()->UnRegisterListener(KeyUpEvent::GetEventID(), EventListener(this, &Keyboard::OnKeyUp));
	}

	void Keyboard::ResetKeys()
	{
		memset(mStates, 0, KEYBOARD_NUM_KEYS * sizeof(vxBool));
	}

	vxBool Keyboard::IsPressed(const KeyboardKeys& key) const
	{
		ASSERT(key < KEYBOARD_NUM_KEYS);
		return mStates[key];
	}

	vxBool Keyboard::IsReleased(const KeyboardKeys& key) const
	{
		ASSERT(key < KEYBOARD_NUM_KEYS);
		return !mStates[key];
	}

	void Keyboard::OnKeyDown(EventPtr& e)
	{
		KeyDownEvent* pEvent = reinterpret_cast<KeyDownEvent*>(e.get());
		ASSERT(pEvent->GetKey() < KEYBOARD_NUM_KEYS);
		mStates[pEvent->GetKey()] = true;
	}

	void Keyboard::OnKeyUp(EventPtr& e)
	{
		KeyUpEvent* pEvent = reinterpret_cast<KeyUpEvent*>(e.get());
		ASSERT(pEvent->GetKey() < KEYBOARD_NUM_KEYS);
		mStates[pEvent->GetKey()] = false;
	}
}