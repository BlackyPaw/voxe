#include <VoxE/HID/KeyDownEvent.hpp>

namespace VX
{
	KeyDownEvent::KeyDownEvent(const KeyboardKeys& key) :
		Event(GetEventID()),
		mKey(key)
	{
	}

	const KeyboardKeys& KeyDownEvent::GetKey() const
	{
		return mKey;
	}
}