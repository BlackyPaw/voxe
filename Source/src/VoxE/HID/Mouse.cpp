#include <VoxE/Core/EventManager.hpp>
#include <VoxE/HID/Mouse.hpp>
#include <VoxE/HID/MouseDownEvent.hpp>
#include <VoxE/HID/MouseUpEvent.hpp>
#include <VoxE/HID/MouseMoveEvent.hpp>
#include <VoxE/Native/Assert.hpp>

namespace VX
{
	Mouse::Mouse()
	{
		ResetState();

		EventManager::GetSharedInstance()->RegisterListener(MouseDownEvent::GetEventID(), EventListener(this, &Mouse::OnMouseDown));
		EventManager::GetSharedInstance()->RegisterListener(MouseUpEvent::GetEventID(), EventListener(this, &Mouse::OnMouseUp));
		EventManager::GetSharedInstance()->RegisterListener(MouseMoveEvent::GetEventID(), EventListener(this, &Mouse::OnMouseMove));
	}

	Mouse::~Mouse()
	{
		EventManager::GetSharedInstance()->UnRegisterListener(MouseDownEvent::GetEventID(), EventListener(this, &Mouse::OnMouseDown));
		EventManager::GetSharedInstance()->UnRegisterListener(MouseUpEvent::GetEventID(), EventListener(this, &Mouse::OnMouseUp));
		EventManager::GetSharedInstance()->UnRegisterListener(MouseMoveEvent::GetEventID(), EventListener(this, &Mouse::OnMouseMove));
	}

	const Vector2i& Mouse::GetCoords() const
	{
		return mCoords;
	}

	vxBool Mouse::IsButtonDown(const MouseButtons& button) const
	{
		ASSERT(button < MOUSE_NUM_BUTTONS);
		return mButtons[button];
	}

	vxBool Mouse::IsButtonUp(const MouseButtons& button) const
	{
		ASSERT(button < MOUSE_NUM_BUTTONS);
		return !mButtons[button];
	}

	void Mouse::ResetState()
	{
		memset(mButtons, 0, MOUSE_NUM_BUTTONS * sizeof(vxBool));
	}

	void Mouse::OnMouseDown(EventPtr& e)
	{
		MouseDownEvent* pEvent = reinterpret_cast<MouseDownEvent*>(e.get());
		mButtons[pEvent->GetButton()] = true;
	}

	void Mouse::OnMouseUp(EventPtr& e)
	{
		MouseUpEvent* pEvent = reinterpret_cast<MouseUpEvent*>(e.get());
		mButtons[pEvent->GetButton()] = false;
	}

	void Mouse::OnMouseMove(EventPtr& e)
	{
		MouseMoveEvent* pEvent = reinterpret_cast<MouseMoveEvent*>(e.get());
		mCoords = pEvent->GetCoords();
	}
}
