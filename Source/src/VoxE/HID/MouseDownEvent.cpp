#include <VoxE/HID/MouseDownEvent.hpp>

namespace VX
{
	MouseDownEvent::MouseDownEvent(const MouseButtons& button, const Vector2i& coords) :
		Event(GetEventID()),
		mButton(button),
		mCoords(coords)
	{
	}

	const MouseButtons& MouseDownEvent::GetButton() const
	{
		return mButton;
	}

	const Vector2i& MouseDownEvent::GetCoords() const
	{
		return mCoords;
	}
}