#include <VoxE/Animation/CelAnimationPlayer.hpp>

namespace VX
{
	CelAnimationPlayer::CelAnimationPlayer(const ResourceHandle& anim) :
		mAnimation(anim),
		mIsPlaying(false),
		mTime(0.0f),
		mIndex(0)
	{
		CelAnimation* pAnim = mAnimation.Get<CelAnimation>();
		if (pAnim != nullptr && pAnim->GetNumStates() > 0)
			mCurState = pAnim->GetState(0);
	}

	void CelAnimationPlayer::Update(const vxF32 delta)
	{
		CelAnimation* pAnim = mAnimation.Get<CelAnimation>();
		if (pAnim == nullptr || pAnim->GetNumStates() == 0 || !mIsPlaying)
			return;

		mTime += delta;
		if (mTime > pAnim->GetSpeed())
		{
			mIndex++;
			mIndex = mIndex % pAnim->GetNumStates();
			mTime = fmodf(mTime, pAnim->GetSpeed());

			mCurState = pAnim->GetState(mIndex);
		}
	}

	void CelAnimationPlayer::Play()
	{
		mIsPlaying = true;
	}

	void CelAnimationPlayer::Stop()
	{
		mIsPlaying = false;
		mTime = 0.0f;
		mIndex = 0;
		CelAnimation* pAnim = mAnimation.Get<CelAnimation>();
		if (pAnim != nullptr && pAnim->GetNumStates() > 0)
			mCurState = pAnim->GetState(0);
	}

	const IntRect& CelAnimationPlayer::GetCurrentState() const
	{
		return mCurState;
	}
}
