#include <VoxE/Animation/CelAnimation.hpp>

namespace VX
{
	CelAnimation::CelAnimation() :
		mSpeed(0.0f)
	{
	}

	CelAnimation::CelAnimation(const vxF32 speed, const std::vector<IntRect>& states) :
		mSpeed(speed),
		mStates(states)
	{
	}

	const vxF32 CelAnimation::GetSpeed() const
	{
		return mSpeed;
	}

	const IntRect& CelAnimation::GetState(const vxU32 i) const
	{
		return mStates[i];
	}

	const vxU32 CelAnimation::GetNumStates() const
	{
		return mStates.size();
	}
}