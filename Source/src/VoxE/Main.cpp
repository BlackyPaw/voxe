#include <VoxE/Core/EventManager.hpp>
#include <VoxE/HID/KeyDownEvent.hpp>
#include <VoxE/HID/KeyUpEvent.hpp>
#include <VoxE/Native/DebugPrint.hpp>
#include <VoxE/Native/GLContext.hpp>
#include <VoxE/Native/GLContextAttributes.hpp>
#include <VoxE/Native/Window.hpp>
#include <VoxE/Native/WindowSizeEvent.hpp>
#include <VoxE/Resources/ResourceCache.hpp>
#include <VoxE/Native/Time.hpp>

// DEBUG-INCLUDES:
#include <VoxE/Actors/SpriteRenderComponent.hpp>
#include <VoxE/Actors/World.hpp>

#include <GL/glew.h>

using namespace VX;

void OnWindowSize(EventPtr& e);
void OnKey(EventPtr& e);

#if defined(VOXE_DEBUG)
int main(int argc, char* argv[])
#else
#include <Windows.h>
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
#endif
{
	Window window;
	GLContextAttributes settings;
	settings.mMajorVersion = 3;
	settings.mMinorVersion = 0;
	settings.mColorBits = 32;
	settings.mDepthBits = 24;
	settings.mStencilBits = 8;
	settings.mCompatibility = false;
	settings.mDebugContext = true;
	window.Create(settings);
	window.Show();
	window.SetTitle("VoxE");
	GLContext glContext = window.GetGLContext();
	if (!glContext.IsValid())
	{
		DebugPrintF("[Native] > Failed to create valid OpenGL context!\n");
		return -1;
	}
	glContext.EnableVSync(true);

	Vector2ui size = window.GetSize();
	glViewport(0, 0, size.x, size.y);

	EventManager::GetSharedInstance()->RegisterListener(WindowSizeEvent::GetEventID(), EventListener(OnWindowSize));
	EventManager::GetSharedInstance()->RegisterListener(KeyDownEvent::GetEventID(), EventListener(OnKey));
	EventManager::GetSharedInstance()->RegisterListener(KeyUpEvent::GetEventID(), EventListener(OnKey));

	ResourceCache::GetSharedInstance()->Initialize();
	ResourceHandle shader = ResourceCache::GetSharedInstance()->GetHandle(ResourceKey("test.efx"));
	Sprite::SetDefaultShader(shader);

	Time lastFrame = Time::Now();

	World world;
	Actor* actor = world.NewActor();
	SpriteRenderComponent* com = new SpriteRenderComponent();
	com->SetTexture(ResourceCache::GetSharedInstance()->GetHandle(ResourceKey("sample.png")));
	actor->AddComponent(com);

	while (window.IsOpen())
	{
		Time now = Time::Now();
		vxF32 delta = (now - lastFrame).Seconds();
		window.Update();
		glContext.FullClear();
		EventManager::GetSharedInstance()->Flush();

		actor->Translate(25.0f * delta, 7.5f * delta, 0.0f);

		world.Update(delta);
		world.Draw();

		glContext.SwapBuffers();
		lastFrame = now;
	}

	EventManager::GetSharedInstance()->UnRegisterListener(WindowSizeEvent::GetEventID(), EventListener(OnWindowSize));
	EventManager::GetSharedInstance()->UnRegisterListener(KeyDownEvent::GetEventID(), EventListener(OnKey));
	EventManager::GetSharedInstance()->UnRegisterListener(KeyUpEvent::GetEventID(), EventListener(OnKey));

	return 0;
}

void OnWindowSize(EventPtr& e)
{
	WindowSizeEvent* pEvent = reinterpret_cast<WindowSizeEvent*>(e.get());
	glViewport(0, 0, pEvent->GetSize().x, pEvent->GetSize().y);
	DebugPrintF("[Native] > Window has been resized to <%u, %u>!\n", pEvent->GetSize().x, pEvent->GetSize().y);
}

void OnKey(EventPtr& e)
{
	KeyboardKeys key;
	vxBool pressed;
	if(e->GetEventID() == KeyDownEvent::GetEventID())
	{
		key = reinterpret_cast<KeyDownEvent*>(e.get())->GetKey();
		pressed = true;
	}
	else
	{
		key = reinterpret_cast<KeyUpEvent*>(e.get())->GetKey();
		pressed = false;
	}

	if(pressed)
	{
		printf("[HID] > Key was pressed: %u!\n", key);
	}
	else
	{
		printf("[HID] > Key was released: %u!\n", key);
	}
}
