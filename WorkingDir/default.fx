EFFECT
{
	// Attributes
	in Position : POSITION;
	
	// Uniforms
	in ProjectionMatrix : PROJECTION_MATRIX;
	in ViewMatrix : VIEW_MATRIX;
	in WorldMatrix : WORLD_MATRIX;
	
	VERTEX_SHADER
	{
		in vec3 Position;
		
		uniform mat4 ProjectionMatrix;
		uniform mat4 ViewMatrix;
		uniform mat4 WorldMatrix;
		
		void main(void)
		{
			gl_Position = ProjectionMatrix * ViewMatrix * WorldMatrix * vec4(Position.xyz, 1.0);
		}
	}
}